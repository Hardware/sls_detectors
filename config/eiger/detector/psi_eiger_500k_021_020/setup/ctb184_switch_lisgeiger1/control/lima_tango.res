#
# Resource backup , created Tue Sep 08 17:20:32 CEST 2020
#

#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/BackgroundSubstractionDeviceServer: "id00/backgroundsubstraction/eiger500k"


# --- id00/backgroundsubstraction/eiger500k attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtAccumulation device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtAccumulation: "id00/ctaccumulation/eiger500k"


# --- id00/ctaccumulation/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtAccumulation properties
#---------------------------------------------------------


# CLASS CtAccumulation attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtAcquisition device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtAcquisition: "id00/ctacquisition/eiger500k"


# --- id00/ctacquisition/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtAcquisition properties
#---------------------------------------------------------


# CLASS CtAcquisition attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtBuffer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtBuffer: "id00/ctbuffer/eiger500k"


# --- id00/ctbuffer/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtBuffer properties
#---------------------------------------------------------


# CLASS CtBuffer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtConfig device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtConfig: "id00/ctconfig/eiger500k"


# --- id00/ctconfig/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtConfig properties
#---------------------------------------------------------


# CLASS CtConfig attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtControl device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtControl: "id00/ctcontrol/eiger500k"


# --- id00/ctcontrol/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtControl properties
#---------------------------------------------------------


# CLASS CtControl attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtEvent device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtEvent: "id00/ctevent/eiger500k"


# --- id00/ctevent/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtEvent properties
#---------------------------------------------------------


# CLASS CtEvent attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtImage device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtImage: "id00/ctimage/eiger500k"


# --- id00/ctimage/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtImage properties
#---------------------------------------------------------


# CLASS CtImage attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtSaving device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtSaving: "id00/ctsaving/eiger500k"


# --- id00/ctsaving/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtSaving properties
#---------------------------------------------------------


# CLASS CtSaving attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtShutter device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtShutter: "id00/ctshutter/eiger500k"


# --- id00/ctshutter/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtShutter properties
#---------------------------------------------------------


# CLASS CtShutter attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtVideo device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtVideo: "id00/ctvideo/eiger500k"


# --- id00/ctvideo/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtVideo properties
#---------------------------------------------------------


# CLASS CtVideo attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/FlatfieldDeviceServer: "id00/flatfield/eiger500k"


# --- id00/flatfield/eiger500k attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LimaCCDs: "id00/limaccds/eiger500k"


# --- id00/limaccds/eiger500k properties

id00/limaccds/eiger500k->BufferMaxMemory: 20
id00/limaccds/eiger500k->LimaCameraType: SlsDetectorEiger
id00/limaccds/eiger500k->NbProcessingThread: 12

# --- id00/limaccds/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LimaTacoCCDs: "id00/limatacoccds/eiger500k"


# --- id00/limatacoccds/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LiveViewer: "id00/liveviewer/eiger500k"


# --- id00/liveviewer/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/MaskDeviceServer: "id00/mask/eiger500k"


# --- id00/mask/eiger500k attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/PeakFinderDeviceServer: "id00/peakfinder/eiger500k"


# --- id00/peakfinder/eiger500k attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/Roi2spectrumDeviceServer: "id00/roi2spectrum/eiger500k"


# --- id00/roi2spectrum/eiger500k attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/RoiCounterDeviceServer: "id00/roicounter/eiger500k"


# --- id00/roicounter/eiger500k attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, SlsDetectorEiger device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/SlsDetectorEiger: "id00/slsdetectoreiger/eiger500k"


# --- id00/slsdetectoreiger/eiger500k properties

id00/slsdetectoreiger/eiger500k->apply_corrections: 0
id00/slsdetectoreiger/eiger500k->config_fname: "/users/blissadm/local/sls_detectors/config/eiger/detector/psi_eiger_500k_021_020/setup/ctb184_switch_lisgeiger1/sdk/slsdetector.config"
id00/slsdetectoreiger/eiger500k->high_voltage: 150
id00/slsdetectoreiger/eiger500k->pixel_depth_cpu_affinity_map: "{ 4: (((CPU( 6), CPU( 7)), (CPU( 9), CPU(10))),",\ 
                                                          "      CPU(18, 19, 21, 22),",\ 
                                                          "      CPU(*chain(range(0, 6), range(12, 18))),",\ 
                                                          "      CPU(0),",\ 
                                                          "      (('eth0,eth1,eth2,eth4,eth6,eth7,eth8,eth9', {-1: (CPU(0), CPU(0))}),",\ 
                                                          "       ('eth3', {-1: (CPU( 8), CPU(20))}),",\ 
                                                          "       ('eth5', {-1: (CPU(11), CPU(23))}))),",\ 
                                                          "  8: '@4',",\ 
                                                          " 16: '@4',",\ 
                                                          " 32: '@4'}"

# --- id00/slsdetectoreiger/eiger500k attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorEiger properties
#---------------------------------------------------------


# CLASS SlsDetectorEiger attribute properties


