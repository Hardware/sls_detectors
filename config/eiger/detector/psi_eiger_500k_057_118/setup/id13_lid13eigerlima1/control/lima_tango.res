#
# Resource backup , created Tue Sep 08 17:20:32 CEST 2020
#

#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/BackgroundSubstractionDeviceServer: "id13/backgroundsubstraction/psi_eiger_500k"


# --- id13/backgroundsubstraction/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtAccumulation device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtAccumulation: "id13/ctaccumulation/psi_eiger_500k"


# --- id13/ctaccumulation/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtAccumulation properties
#---------------------------------------------------------


# CLASS CtAccumulation attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtAcquisition device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtAcquisition: "id13/ctacquisition/psi_eiger_500k"


# --- id13/ctacquisition/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtAcquisition properties
#---------------------------------------------------------


# CLASS CtAcquisition attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtBuffer device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtBuffer: "id13/ctbuffer/psi_eiger_500k"


# --- id13/ctbuffer/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtBuffer properties
#---------------------------------------------------------


# CLASS CtBuffer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtConfig device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtConfig: "id13/ctconfig/psi_eiger_500k"


# --- id13/ctconfig/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtConfig properties
#---------------------------------------------------------


# CLASS CtConfig attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtControl device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtControl: "id13/ctcontrol/psi_eiger_500k"


# --- id13/ctcontrol/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtControl properties
#---------------------------------------------------------


# CLASS CtControl attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtEvent device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtEvent: "id13/ctevent/psi_eiger_500k"


# --- id13/ctevent/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtEvent properties
#---------------------------------------------------------


# CLASS CtEvent attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtImage device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtImage: "id13/ctimage/psi_eiger_500k"


# --- id13/ctimage/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtImage properties
#---------------------------------------------------------


# CLASS CtImage attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtSaving device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtSaving: "id13/ctsaving/psi_eiger_500k"


# --- id13/ctsaving/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtSaving properties
#---------------------------------------------------------


# CLASS CtSaving attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtShutter device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtShutter: "id13/ctshutter/psi_eiger_500k"


# --- id13/ctshutter/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtShutter properties
#---------------------------------------------------------


# CLASS CtShutter attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, CtVideo device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/CtVideo: "id13/ctvideo/psi_eiger_500k"


# --- id13/ctvideo/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS CtVideo properties
#---------------------------------------------------------


# CLASS CtVideo attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/FlatfieldDeviceServer: "id13/flatfield/psi_eiger_500k"


# --- id13/flatfield/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/LimaCCDs: "id13/limaccds/psi_eiger_500k"


# --- id13/limaccds/psi_eiger_500k properties

id13/limaccds/psi_eiger_500k->AccBufferParameters: "<initMem=1, durationPolicy=Persistent, sizePolicy=Fixed, reqMemSizePercent=8>"
id13/limaccds/psi_eiger_500k->BufferAllocParameters: "<initMem=1, durationPolicy=Persistent, sizePolicy=Fixed, reqMemSizePercent=8>"
id13/limaccds/psi_eiger_500k->LimaCameraType: SlsDetectorEiger
id13/limaccds/psi_eiger_500k->NbProcessingThread: 16
id13/limaccds/psi_eiger_500k->SavingZBufferParameters: "<initMem=1, durationPolicy=Persistent, sizePolicy=Fixed, reqMemSizePercent=9>"

# --- id13/limaccds/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/LimaTacoCCDs: "id13/limatacoccds/psi_eiger_500k"


# --- id13/limatacoccds/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/LiveViewer: "id13/liveviewer/psi_eiger_500k"


# --- id13/liveviewer/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/MaskDeviceServer: "id13/mask/psi_eiger_500k"


# --- id13/mask/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/PeakFinderDeviceServer: "id13/peakfinder/psi_eiger_500k"


# --- id13/peakfinder/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/Roi2spectrumDeviceServer: "id13/roi2spectrum/psi_eiger_500k"


# --- id13/roi2spectrum/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/RoiCounterDeviceServer: "id13/roicounter/psi_eiger_500k"


# --- id13/roicounter/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/psi_eiger_500k, SlsDetectorEiger device declaration
#---------------------------------------------------------

LimaCCDs/psi_eiger_500k/DEVICE/SlsDetectorEiger: "id13/slsdetectoreiger/psi_eiger_500k"


# --- id13/slsdetectoreiger/psi_eiger_500k properties

id13/slsdetectoreiger/psi_eiger_500k->apply_corrections: 0
id13/slsdetectoreiger/psi_eiger_500k->buffer_max_memory: 30
id13/slsdetectoreiger/psi_eiger_500k->config_fname: "/users/blissadm/local/sls_detectors/config/eiger/detector/psi_eiger_500k_057_118/setup/id13_lid13eigerlima1/sdk/slsdetector.config"
id13/slsdetectoreiger/psi_eiger_500k->high_voltage: 150
id13/slsdetectoreiger/psi_eiger_500k->pixel_depth_cpu_affinity_map: "{ 4: (((CPU( 1), CPU( 3)), (CPU( 9), CPU(11))),",\ 
                                                          "      CPU(15),",\ 
                                                          "      CPU(*chain(range(16, 32, 2), range(48, 64, 2))),",\ 
                                                          "      CPU(*chain(range( 4, 16, 2), range(36, 48, 2))),",\ 
                                                          "      (('i10p0,i10p1', {-1: (CPU(31), CPU(63))}),",\ 
                                                          "       ('i10p2', {-1: (CPU( 5), CPU(37))}),",\ 
                                                          "       ('i10p3', {-1: (CPU(13), CPU(45))}))),",\ 
                                                          "  8: '@4',",\ 
                                                          " 16: '@4',",\ 
                                                          " 32: '@4'}"

# --- id13/slsdetectoreiger/psi_eiger_500k attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorEiger properties
#---------------------------------------------------------


# CLASS SlsDetectorEiger attribute properties


