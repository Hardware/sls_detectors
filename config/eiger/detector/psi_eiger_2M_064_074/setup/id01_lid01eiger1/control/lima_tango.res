#
# Resource backup , created Mon Jan 27 10:01:55 CET 2022
#

#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/BackgroundSubstractionDeviceServer: "id01/backgroundsubstraction/eiger2m"


# --- id01/backgroundsubstraction/eiger2m attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, BpmDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/BpmDeviceServer: "id01/bpm/eiger2m"


# --- id01/bpm/eiger2m attribute properties


#---------------------------------------------------------
# CLASS BpmDeviceServer properties
#---------------------------------------------------------


# CLASS BpmDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtAccumulation device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtAccumulation: "id01/ctaccumulation/eiger2m"


# --- id01/ctaccumulation/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtAccumulation properties
#---------------------------------------------------------


# CLASS CtAccumulation attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtAcquisition device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtAcquisition: "id01/ctacquisition/eiger2m"


# --- id01/ctacquisition/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtAcquisition properties
#---------------------------------------------------------


# CLASS CtAcquisition attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtBuffer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtBuffer: "id01/ctbuffer/eiger2m"


# --- id01/ctbuffer/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtBuffer properties
#---------------------------------------------------------


# CLASS CtBuffer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtConfig device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtConfig: "id01/ctconfig/eiger2m"


# --- id01/ctconfig/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtConfig properties
#---------------------------------------------------------


# CLASS CtConfig attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtControl device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtControl: "id01/ctcontrol/eiger2m"


# --- id01/ctcontrol/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtControl properties
#---------------------------------------------------------


# CLASS CtControl attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtEvent device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtEvent: "id01/ctevent/eiger2m"


# --- id01/ctevent/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtEvent properties
#---------------------------------------------------------


# CLASS CtEvent attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtImage device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtImage: "id01/ctimage/eiger2m"


# --- id01/ctimage/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtImage properties
#---------------------------------------------------------


# CLASS CtImage attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtSaving device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtSaving: "id01/ctsaving/eiger2m"


# --- id01/ctsaving/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtSaving properties
#---------------------------------------------------------


# CLASS CtSaving attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtShutter device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtShutter: "id01/ctshutter/eiger2m"


# --- id01/ctshutter/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtShutter properties
#---------------------------------------------------------


# CLASS CtShutter attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, CtVideo device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/CtVideo: "id01/ctvideo/eiger2m"


# --- id01/ctvideo/eiger2m attribute properties


#---------------------------------------------------------
# CLASS CtVideo properties
#---------------------------------------------------------


# CLASS CtVideo attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/FlatfieldDeviceServer: "id01/flatfield/eiger2m"


# --- id01/flatfield/eiger2m attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/LimaCCDs: "id01/limaccds/eiger2M"


# --- id01/limaccds/eiger2M properties

id01/limaccds/eiger2M->BufferMaxMemory: 10
id01/limaccds/eiger2M->LimaCameraType: SlsDetectorEiger
id01/limaccds/eiger2M->NbProcessingThread: 8

# --- id01/limaccds/eiger2M attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/LimaTacoCCDs: "id01/limatacoccds/eiger2m"


# --- id01/limatacoccds/eiger2m attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/LiveViewer: "id01/liveviewer/eiger2m"


# --- id01/liveviewer/eiger2m attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/MaskDeviceServer: "id01/mask/eiger2m"


# --- id01/mask/eiger2m attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/PeakFinderDeviceServer: "id01/peakfinder/eiger2m"


# --- id01/peakfinder/eiger2m attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/Roi2spectrumDeviceServer: "id01/roi2spectrum/eiger2m"


# --- id01/roi2spectrum/eiger2m attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, RoiCollectionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/RoiCollectionDeviceServer: "id01/roicollection/eiger2m"


# --- id01/roicollection/eiger2m attribute properties


#---------------------------------------------------------
# CLASS RoiCollectionDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCollectionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/RoiCounterDeviceServer: "id01/roicounter/eiger2m"


# --- id01/roicounter/eiger2m attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger2M, SlsDetectorEiger device declaration
#---------------------------------------------------------

LimaCCDs/eiger2M/DEVICE/SlsDetectorEiger: "id01/slsdetectoreiger/eiger2M"


# --- id01/slsdetectoreiger/eiger2M properties

id01/slsdetectoreiger/eiger2M->buffer_max_memory: 40
id01/slsdetectoreiger/eiger2M->config_fname: "/users/blissadm/local/sls_detectors/config/eiger/detector/psi_eiger_2M_064_074/setup/id01_lid01eiger1/sdk/slsdetector.config"
id01/slsdetectoreiger/eiger2M->high_voltage: 190
id01/slsdetectoreiger/eiger2M->pixel_depth_cpu_affinity_map: "{ 4: (((CPU( 6), CPU( 7)), (CPU( 9), CPU(10)),",\ 
                                                             "       (CPU( 6), CPU( 7)), (CPU( 9), CPU(10)),",\ 
                                                             "       (CPU( 6), CPU( 7)), (CPU( 9), CPU(10)),",\ 
                                                             "       (CPU( 6), CPU( 7)), (CPU( 9), CPU(10))),",\ 
                                                             "       CPU(18, 19, 21, 22),",\ 
                                                             "       CPU(*chain(range( 2,  6), range(14, 18))),",\ 
                                                             "       CPU(*chain(range( 0,  2), range(12, 14))),",\ 
                                                             "       (('eth0,eth1,xth0', {-1: (CPU( 0), CPU( 0))}),",\ 
                                                             "        ('xth1,xth1.196,xth1.3080', {-1: (CPU( 0, 12), CPU( 1, 13))}),",\ 
                                                             "        ('mth0,mth1', {-1: (CPU( 8), CPU(20))}),",\ 
                                                             "        ('mth2,mth3', {-1: (CPU(11), CPU(23))}))),",\ 
                                                             "  8: '@4',",\ 
                                                             " 16: '@4',",\ 
                                                             " 32: '@4'}"

# --- id01/slsdetectoreiger/eiger2M attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorEiger properties
#---------------------------------------------------------


# CLASS SlsDetectorEiger attribute properties


