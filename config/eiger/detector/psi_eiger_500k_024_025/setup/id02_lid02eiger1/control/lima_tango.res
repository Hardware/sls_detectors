#
# Resource backup , created Mon Apr 11 13:58:57 CEST 2022
#

#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/BackgroundSubstractionDeviceServer: "id02/backgroundsubstraction/eiger500k"


# --- id02/backgroundsubstraction/eiger500k attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, BpmDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/BpmDeviceServer: "id02/bpm/eiger500k"


# --- id02/bpm/eiger500k attribute properties


#---------------------------------------------------------
# CLASS BpmDeviceServer properties
#---------------------------------------------------------


# CLASS BpmDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtAccumulation device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtAccumulation: "id02/ctaccumulation/eiger500k"


# --- id02/ctaccumulation/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtAccumulation properties
#---------------------------------------------------------


# CLASS CtAccumulation attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtAcquisition device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtAcquisition: "id02/ctacquisition/eiger500k"


# --- id02/ctacquisition/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtAcquisition properties
#---------------------------------------------------------


# CLASS CtAcquisition attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtBuffer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtBuffer: "id02/ctbuffer/eiger500k"


# --- id02/ctbuffer/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtBuffer properties
#---------------------------------------------------------


# CLASS CtBuffer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtConfig device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtConfig: "id02/ctconfig/eiger500k"


# --- id02/ctconfig/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtConfig properties
#---------------------------------------------------------


# CLASS CtConfig attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtControl device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtControl: "id02/ctcontrol/eiger500k"


# --- id02/ctcontrol/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtControl properties
#---------------------------------------------------------


# CLASS CtControl attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtEvent device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtEvent: "id02/ctevent/eiger500k"


# --- id02/ctevent/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtEvent properties
#---------------------------------------------------------


# CLASS CtEvent attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtImage device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtImage: "id02/ctimage/eiger500k"


# --- id02/ctimage/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtImage properties
#---------------------------------------------------------


# CLASS CtImage attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtSaving device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtSaving: "id02/ctsaving/eiger500k"


# --- id02/ctsaving/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtSaving properties
#---------------------------------------------------------


# CLASS CtSaving attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtShutter device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtShutter: "id02/ctshutter/eiger500k"


# --- id02/ctshutter/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtShutter properties
#---------------------------------------------------------


# CLASS CtShutter attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtVideo device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtVideo: "id02/ctvideo/eiger500k"


# --- id02/ctvideo/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtVideo properties
#---------------------------------------------------------


# CLASS CtVideo attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/FlatfieldDeviceServer: "id02/flatfield/eiger500k"


# --- id02/flatfield/eiger500k attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LimaCCDs: "id02/limaccds/eiger500k"


# --- id02/limaccds/eiger500k properties

id02/limaccds/eiger500k->BufferMaxMemory: 10
id02/limaccds/eiger500k->InstrumentName: ESRF-ID02
id02/limaccds/eiger500k->LimaCameraType: SlsDetectorEiger
id02/limaccds/eiger500k->NbProcessingThread: 12
id02/limaccds/eiger500k->UserDetectorName: id02-eiger500k-saxs

# --- id02/limaccds/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LimaTacoCCDs: "id02/limatacoccds/eiger500k"


# --- id02/limatacoccds/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LiveViewer: "id02/liveviewer/eiger500k"


# --- id02/liveviewer/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/MaskDeviceServer: "id02/mask/eiger500k"


# --- id02/mask/eiger500k attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/PeakFinderDeviceServer: "id02/peakfinder/eiger500k"


# --- id02/peakfinder/eiger500k attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/Roi2spectrumDeviceServer: "id02/roi2spectrum/eiger500k"


# --- id02/roi2spectrum/eiger500k attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/RoiCounterDeviceServer: "id02/roicounter/eiger500k"


# --- id02/roicounter/eiger500k attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, SlsDetectorEiger device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/SlsDetectorEiger: "id02/slsdetectoreiger/eiger500k"


# --- id02/slsdetectoreiger/eiger500k properties

id02/slsdetectoreiger/eiger500k->apply_corrections: 0
id02/slsdetectoreiger/eiger500k->buffer_max_memory: 40
id02/slsdetectoreiger/eiger500k->config_fname: "/users/blissadm/local/sls_detectors/config/eiger/detector/psi_eiger_500k_024_025/setup/id02_lid02eiger1/sdk/slsdetector.config"
id02/slsdetectoreiger/eiger500k->high_voltage: 150
id02/slsdetectoreiger/eiger500k->pixel_depth_cpu_affinity_map: "{ 4: (((CPU( 6), CPU( 7)), (CPU( 9), CPU(10))),",\ 
                                                               "      CPU(18, 19, 21, 22),",\ 
                                                               "      CPU(*chain(range( 0,  6), range(12, 18))),",\ 
                                                               "      CPU(0),",\ 
                                                               "      (('eth0,eth1,eth2,eth4,eth6, eth7', {-1: (CPU( 0), CPU( 0))}),",\ 
                                                               "       ('eth3', {-1: (CPU( 8), CPU(20))}),",\ 
                                                               "       ('eth5', {-1: (CPU(11), CPU(23))}))),",\ 
                                                               "  8: '@4',",\ 
                                                               " 16: '@4',",\ 
                                                               " 32: '@4'}"

# --- id02/slsdetectoreiger/eiger500k attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorEiger properties
#---------------------------------------------------------


# CLASS SlsDetectorEiger attribute properties


