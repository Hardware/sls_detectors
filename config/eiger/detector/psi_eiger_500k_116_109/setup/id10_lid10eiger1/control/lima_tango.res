#
# Resource backup , created Mon Apr 11 13:58:57 CEST 2022
#

#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/BackgroundSubstractionDeviceServer: "id10/backgroundsubstraction/eiger500k"


# --- id10/backgroundsubstraction/eiger500k attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, BpmDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/BpmDeviceServer: "id10/bpm/eiger500k"


# --- id10/bpm/eiger500k attribute properties


#---------------------------------------------------------
# CLASS BpmDeviceServer properties
#---------------------------------------------------------


# CLASS BpmDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtAccumulation device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtAccumulation: "id10/ctaccumulation/eiger500k"


# --- id10/ctaccumulation/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtAccumulation properties
#---------------------------------------------------------


# CLASS CtAccumulation attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtAcquisition device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtAcquisition: "id10/ctacquisition/eiger500k"


# --- id10/ctacquisition/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtAcquisition properties
#---------------------------------------------------------


# CLASS CtAcquisition attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtBuffer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtBuffer: "id10/ctbuffer/eiger500k"


# --- id10/ctbuffer/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtBuffer properties
#---------------------------------------------------------


# CLASS CtBuffer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtConfig device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtConfig: "id10/ctconfig/eiger500k"


# --- id10/ctconfig/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtConfig properties
#---------------------------------------------------------


# CLASS CtConfig attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtControl device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtControl: "id10/ctcontrol/eiger500k"


# --- id10/ctcontrol/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtControl properties
#---------------------------------------------------------


# CLASS CtControl attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtEvent device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtEvent: "id10/ctevent/eiger500k"


# --- id10/ctevent/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtEvent properties
#---------------------------------------------------------


# CLASS CtEvent attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtImage device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtImage: "id10/ctimage/eiger500k"


# --- id10/ctimage/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtImage properties
#---------------------------------------------------------


# CLASS CtImage attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtSaving device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtSaving: "id10/ctsaving/eiger500k"


# --- id10/ctsaving/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtSaving properties
#---------------------------------------------------------


# CLASS CtSaving attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtShutter device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtShutter: "id10/ctshutter/eiger500k"


# --- id10/ctshutter/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtShutter properties
#---------------------------------------------------------


# CLASS CtShutter attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, CtVideo device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/CtVideo: "id10/ctvideo/eiger500k"


# --- id10/ctvideo/eiger500k attribute properties


#---------------------------------------------------------
# CLASS CtVideo properties
#---------------------------------------------------------


# CLASS CtVideo attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/FlatfieldDeviceServer: "id10/flatfield/eiger500k"


# --- id10/flatfield/eiger500k attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LimaCCDs: "id10/limaccds/eiger500k"


# --- id10/limaccds/eiger500k properties

id10/limaccds/eiger500k->BufferMaxMemory: 10
id10/limaccds/eiger500k->LimaCameraType: SlsDetectorEiger
id10/limaccds/eiger500k->NbProcessingThread: 8
id10/limaccds/eiger500k->UserDetectorName: Eiger1

# --- id10/limaccds/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LimaTacoCCDs: "id10/limatacoccds/eiger500k"


# --- id10/limatacoccds/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/LiveViewer: "id10/liveviewer/eiger500k"


# --- id10/liveviewer/eiger500k attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/MaskDeviceServer: "id10/mask/eiger500k"


# --- id10/mask/eiger500k attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/PeakFinderDeviceServer: "id10/peakfinder/eiger500k"


# --- id10/peakfinder/eiger500k attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/Roi2spectrumDeviceServer: "id10/roi2spectrum/eiger500k"


# --- id10/roi2spectrum/eiger500k attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/RoiCounterDeviceServer: "id10/roicounter/eiger500k"


# --- id10/roicounter/eiger500k attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/eiger500k, SlsDetectorEiger device declaration
#---------------------------------------------------------

LimaCCDs/eiger500k/DEVICE/SlsDetectorEiger: "id10/slsdetectoreiger/eiger500k"


# --- id10/slsdetectoreiger/eiger500k properties

id10/slsdetectoreiger/eiger500k->apply_corrections: 0
id10/slsdetectoreiger/eiger500k->buffer_max_memory: 40
id10/slsdetectoreiger/eiger500k->config_fname: "/users/blissadm/local/sls_detectors/config/eiger/detector/psi_eiger_500k_116_109/setup/id10_lid10eiger1/sdk/slsdetector.config"
id10/slsdetectoreiger/eiger500k->fixed_clock_div: 1
id10/slsdetectoreiger/eiger500k->high_voltage: 150
id10/slsdetectoreiger/eiger500k->pixel_depth_cpu_affinity_map: "{ 4: (((CPU( 6), CPU( 7)), (CPU( 9), CPU(10))),",\ 
                                                               "      CPU(18, 19, 21, 22),",\ 
                                                               "      CPU(*chain(range( 2,  6), range(14, 18))),",\ 
                                                               "      CPU(*chain(range( 0,  2), range(12, 14))),",\ 
                                                               "      (('eth0,eth2,eth4', {-1: (CPU( 0), CPU( 0))}),",\ 
                                                               "       ('eth6,eth7', {-1: (CPU( 0, 12), CPU( 1, 13))}),",\ 
                                                               "       ('eth3', {-1: (CPU( 8), CPU(20))}),",\ 
                                                               "       ('eth5', {-1: (CPU(11), CPU(23))}))),",\ 
                                                               "  8: '@4',",\ 
                                                               " 16: '@4',",\ 
                                                               " 32: '@4'}"

# --- id10/slsdetectoreiger/eiger500k attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorEiger properties
#---------------------------------------------------------


# CLASS SlsDetectorEiger attribute properties


