#
# Resource backup , created Wed Jul 10 16:49:41 CEST 2024
#

#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/BackgroundSubstractionDeviceServer: "id00/backgroundsubstraction/id09_jungfrau1m"


# --- id00/backgroundsubstraction/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, BpmDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/BpmDeviceServer: "id00/bpm/id09_jungfrau1m"


# --- id00/bpm/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS BpmDeviceServer properties
#---------------------------------------------------------


# CLASS BpmDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/FlatfieldDeviceServer: "id00/flatfield/id09_jungfrau1m"


# --- id00/flatfield/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/LimaCCDs: "id00/limaccds/id09_jungfrau1m"


# --- id00/limaccds/id09_jungfrau1m properties

id00/limaccds/id09_jungfrau1m->AccBufferParameters: "<initMem=1, durationPolicy=Persistent, sizePolicy=Fixed, reqMemSizePercent=8>"
id00/limaccds/id09_jungfrau1m->BufferAllocParameters: "<initMem=1, durationPolicy=Persistent, sizePolicy=Fixed, reqMemSizePercent=8>"
id00/limaccds/id09_jungfrau1m->LimaCameraType: SlsDetectorJungfrau
id00/limaccds/id09_jungfrau1m->NbProcessingThread: 56
id00/limaccds/id09_jungfrau1m->SavingZBufferParameters: "<initMem=1, durationPolicy=Persistent, sizePolicy=Fixed, reqMemSizePercent=9>"

# --- id00/limaccds/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/LimaTacoCCDs: "id00/limatacoccds/id09_jungfrau1m"


# --- id00/limatacoccds/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/LiveViewer: "id00/liveviewer/id09_jungfrau1m"


# --- id00/liveviewer/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/MaskDeviceServer: "id00/mask/id09_jungfrau1m"


# --- id00/mask/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/PeakFinderDeviceServer: "id00/peakfinder/id09_jungfrau1m"


# --- id00/peakfinder/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/Roi2spectrumDeviceServer: "id00/roi2spectrum/id09_jungfrau1m"


# --- id00/roi2spectrum/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, RoiCollectionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/RoiCollectionDeviceServer: "id00/roicollection/id09_jungfrau1m"


# --- id00/roicollection/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS RoiCollectionDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCollectionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/RoiCounterDeviceServer: "id00/roicounter/id09_jungfrau1m"


# --- id00/roicounter/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/id09_jungfrau1m, SlsDetectorJungfrau device declaration
#---------------------------------------------------------

LimaCCDs/id09_jungfrau1m/DEVICE/SlsDetectorJungfrau: "id00/slsdetectorjungfrau/id09_jungfrau1m"


# --- id00/slsdetectorjungfrau/id09_jungfrau1m properties

id00/slsdetectorjungfrau/id09_jungfrau1m->buffer_max_memory: 35
id00/slsdetectorjungfrau/id09_jungfrau1m->config_fname: "/users/blissadm/local/sls_detectors/config/jungfrau/detector/psi_jungfrau1m_01/setup/id22_lid00lima23/sdk/slsdetector_1m_x2.config"
id00/slsdetectorjungfrau/id09_jungfrau1m->det_asm_calib_file: "/users/blissadm/local/id09/jungfrau/calibration/2024-07-08/gains_wg_std.h5"
id00/slsdetectorjungfrau/id09_jungfrau1m->det_asm_calib_file_sc: "/users/blissadm/local/id09/jungfrau/calibration/2024-07-08/gains_wg_sc.h5"
id00/slsdetectorjungfrau/id09_jungfrau1m->pixel_depth_cpu_affinity_map: "{16: {'recv_cpu': ((CPU( 1), CPU( 3)), (CPU( 5), CPU( 7))),",\ 
                                                                        "      'acq_cpu': CPU( 9),",\ 
                                                                        "      'lima_cpu': CPU(*chain(range( 4, 32, 2), range(36, 64, 2), range( 9, 64, 2))),",\ 
                                                                        "      'lima_node': Node(0),",\ 
                                                                        "      'other_cpu': CPU(*chain(range( 0,  4, 2), range(32, 36, 2))),",\ 
                                                                        "      'netdev_cpu': (('i10p0', {-1: (CPU( 9), CPU(41))}),",\ 
                                                                        "           ('i10p1', {-1: (CPU(11), CPU(43))}),",\ 
                                                                        "           ('i10p2', {-1: (CPU(13), CPU(45))}),",\ 
                                                                        "           ('i10p3', {-1: (CPU(15), CPU(47))}),",\ 
                                                                        "           ('eno1', {-1: (CPU( 0,  2), CPU(32, 34))}),",\ 
                                                                        "           ('eno3', {-1: (CPU( 2), CPU(34))})),",\ 
                                                                        "      'rx_netdev': ('i10p0', 'i10p1', 'i10p2', 'i10p3')}}"

# --- id00/slsdetectorjungfrau/id09_jungfrau1m attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorJungfrau properties
#---------------------------------------------------------


# CLASS SlsDetectorJungfrau attribute properties


