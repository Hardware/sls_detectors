#
# Resource backup , created Wed Dec 16 08:59:20 CET 2020
#

#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/BackgroundSubstractionDeviceServer: "id00/backgroundsubstraction/deg_jungfrau500k_084"


# --- id00/backgroundsubstraction/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, BpmDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/BpmDeviceServer: "id00/bpm/deg_jungfrau500k_084"


# --- id00/bpm/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS BpmDeviceServer properties
#---------------------------------------------------------


# CLASS BpmDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtAccumulation device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtAccumulation: "id00/ctaccumulation/deg_jungfrau500k_084"


# --- id00/ctaccumulation/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtAccumulation properties
#---------------------------------------------------------


# CLASS CtAccumulation attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtAcquisition device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtAcquisition: "id00/ctacquisition/deg_jungfrau500k_084"


# --- id00/ctacquisition/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtAcquisition properties
#---------------------------------------------------------


# CLASS CtAcquisition attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtBuffer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtBuffer: "id00/ctbuffer/deg_jungfrau500k_084"


# --- id00/ctbuffer/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtBuffer properties
#---------------------------------------------------------


# CLASS CtBuffer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtConfig device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtConfig: "id00/ctconfig/deg_jungfrau500k_084"


# --- id00/ctconfig/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtConfig properties
#---------------------------------------------------------


# CLASS CtConfig attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtControl device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtControl: "id00/ctcontrol/deg_jungfrau500k_084"


# --- id00/ctcontrol/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtControl properties
#---------------------------------------------------------


# CLASS CtControl attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtEvent device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtEvent: "id00/ctevent/deg_jungfrau500k_084"


# --- id00/ctevent/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtEvent properties
#---------------------------------------------------------


# CLASS CtEvent attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtImage device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtImage: "id00/ctimage/deg_jungfrau500k_084"


# --- id00/ctimage/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtImage properties
#---------------------------------------------------------


# CLASS CtImage attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtSaving device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtSaving: "id00/ctsaving/deg_jungfrau500k_084"


# --- id00/ctsaving/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtSaving properties
#---------------------------------------------------------


# CLASS CtSaving attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtShutter device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtShutter: "id00/ctshutter/deg_jungfrau500k_084"


# --- id00/ctshutter/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtShutter properties
#---------------------------------------------------------


# CLASS CtShutter attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, CtVideo device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/CtVideo: "id00/ctvideo/deg_jungfrau500k_084"


# --- id00/ctvideo/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS CtVideo properties
#---------------------------------------------------------


# CLASS CtVideo attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/FlatfieldDeviceServer: "id00/flatfield/deg_jungfrau500k_084"


# --- id00/flatfield/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/LimaCCDs: "id00/limaccds/deg_jungfrau500k_084"


# --- id00/limaccds/deg_jungfrau500k_084 properties

id00/limaccds/deg_jungfrau500k_084->BufferMaxMemory: 10
id00/limaccds/deg_jungfrau500k_084->LimaCameraType: SlsDetectorJungfrau
id00/limaccds/deg_jungfrau500k_084->NbProcessingThread: 8

# --- id00/limaccds/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/LimaTacoCCDs: "id00/limatacoccds/deg_jungfrau500k_084"


# --- id00/limatacoccds/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/LiveViewer: "id00/liveviewer/deg_jungfrau500k_084"


# --- id00/liveviewer/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/MaskDeviceServer: "id00/mask/deg_jungfrau500k_084"


# --- id00/mask/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/PeakFinderDeviceServer: "id00/peakfinder/deg_jungfrau500k_084"


# --- id00/peakfinder/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/Roi2spectrumDeviceServer: "id00/roi2spectrum/deg_jungfrau500k_084"


# --- id00/roi2spectrum/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/RoiCounterDeviceServer: "id00/roicounter/deg_jungfrau500k_084"


# --- id00/roicounter/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_jungfrau500k_084, SlsDetectorJungfrau device declaration
#---------------------------------------------------------

LimaCCDs/deg_jungfrau500k_084/DEVICE/SlsDetectorJungfrau: "id00/slsdetectorjungfrau/deg_jungfrau500k_084"


# --- id00/slsdetectorjungfrau/deg_jungfrau500k_084 properties

id00/slsdetectorjungfrau/deg_jungfrau500k_084->buffer_max_memory: 40
id00/slsdetectorjungfrau/deg_jungfrau500k_084->config_fname: "/users/blissadm/local/sls_detectors/config/jungfrau/detector/psi_jungfrau500k_084/setup/id22_direct_ldegjfrau1/sdk/slsdetector_x1.config"
id00/slsdetectorjungfrau/deg_jungfrau500k_084->pixel_depth_cpu_affinity_map: "{16: (((CPU( 6),),),",\ 
                                                                             "      CPU(18, 19, 21, 22),",\ 
                                                                             "      CPU(*chain(range( 2,  6), range(14, 18))),",\ 
                                                                             "      CPU(*chain(range( 0,  2), range(12, 14))),",\ 
                                                                             "      (('eth1,eth6,eth7', {-1: (CPU(0), CPU(0))}),",\ 
                                                                             "       ('eth5', {-1: (CPU( 0, 12), CPU( 1, 13))}),",\ 
                                                                             "       ('eth2,eth4', {-1: (CPU( 8), CPU(20))}),",\ 
                                                                             "       ('eth3', {-1: (CPU(11), CPU(23))})))",\ 
                                                                             }
      
# --- id00/slsdetectorjungfrau/deg_jungfrau500k_084 attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorJungfrau properties
#---------------------------------------------------------


# CLASS SlsDetectorJungfrau attribute properties


