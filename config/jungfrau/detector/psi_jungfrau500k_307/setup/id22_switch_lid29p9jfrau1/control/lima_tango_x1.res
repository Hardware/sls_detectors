#
# Resource backup , created Mon Dec 21 17:11:26 CET 2020
#

#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/BackgroundSubstractionDeviceServer: "id00/backgroundsubstraction/deg_psi_jungfrau500k_3"


# --- id00/backgroundsubstraction/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, BpmDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/BpmDeviceServer: "id00/bpm/deg_psi_jungfrau500k_3"


# --- id00/bpm/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS BpmDeviceServer properties
#---------------------------------------------------------


# CLASS BpmDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtAccumulation device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtAccumulation: "id00/ctaccumulation/deg_psi_jungfrau500k_3"


# --- id00/ctaccumulation/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtAccumulation properties
#---------------------------------------------------------


# CLASS CtAccumulation attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtAcquisition device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtAcquisition: "id00/ctacquisition/deg_psi_jungfrau500k_3"


# --- id00/ctacquisition/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtAcquisition properties
#---------------------------------------------------------


# CLASS CtAcquisition attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtBuffer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtBuffer: "id00/ctbuffer/deg_psi_jungfrau500k_3"


# --- id00/ctbuffer/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtBuffer properties
#---------------------------------------------------------


# CLASS CtBuffer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtConfig device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtConfig: "id00/ctconfig/deg_psi_jungfrau500k_3"


# --- id00/ctconfig/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtConfig properties
#---------------------------------------------------------


# CLASS CtConfig attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtControl device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtControl: "id00/ctcontrol/deg_psi_jungfrau500k_3"


# --- id00/ctcontrol/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtControl properties
#---------------------------------------------------------


# CLASS CtControl attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtEvent device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtEvent: "id00/ctevent/deg_psi_jungfrau500k_3"


# --- id00/ctevent/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtEvent properties
#---------------------------------------------------------


# CLASS CtEvent attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtImage device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtImage: "id00/ctimage/deg_psi_jungfrau500k_3"


# --- id00/ctimage/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtImage properties
#---------------------------------------------------------


# CLASS CtImage attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtSaving device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtSaving: "id00/ctsaving/deg_psi_jungfrau500k_3"


# --- id00/ctsaving/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtSaving properties
#---------------------------------------------------------


# CLASS CtSaving attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtShutter device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtShutter: "id00/ctshutter/deg_psi_jungfrau500k_3"


# --- id00/ctshutter/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtShutter properties
#---------------------------------------------------------


# CLASS CtShutter attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, CtVideo device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/CtVideo: "id00/ctvideo/deg_psi_jungfrau500k_3"


# --- id00/ctvideo/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS CtVideo properties
#---------------------------------------------------------


# CLASS CtVideo attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/FlatfieldDeviceServer: "id00/flatfield/deg_psi_jungfrau500k_3"


# --- id00/flatfield/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/LimaCCDs: "id00/limaccds/deg_psi_jungfrau500k_3"


# --- id00/limaccds/deg_psi_jungfrau500k_3 properties

id00/limaccds/deg_psi_jungfrau500k_3->BufferMaxMemory: 25
id00/limaccds/deg_psi_jungfrau500k_3->LimaCameraType: SlsDetectorJungfrau
id00/limaccds/deg_psi_jungfrau500k_3->NbProcessingThread: 128

# --- id00/limaccds/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/LimaTacoCCDs: "id00/limatacoccds/deg_psi_jungfrau500k_3"


# --- id00/limatacoccds/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/LiveViewer: "id00/liveviewer/deg_psi_jungfrau500k_3"


# --- id00/liveviewer/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/MaskDeviceServer: "id00/mask/deg_psi_jungfrau500k_3"


# --- id00/mask/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/PeakFinderDeviceServer: "id00/peakfinder/deg_psi_jungfrau500k_3"


# --- id00/peakfinder/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/Roi2spectrumDeviceServer: "id00/roi2spectrum/deg_psi_jungfrau500k_3"


# --- id00/roi2spectrum/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/RoiCounterDeviceServer: "id00/roicounter/deg_psi_jungfrau500k_3"


# --- id00/roicounter/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/deg_psi_jungfrau500k_3, SlsDetectorJungfrau device declaration
#---------------------------------------------------------

LimaCCDs/deg_psi_jungfrau500k_3/DEVICE/SlsDetectorJungfrau: "id00/slsdetectorjungfrau/deg_psi_jungfrau500k_3"


# --- id00/slsdetectorjungfrau/deg_psi_jungfrau500k_3 properties

id00/slsdetectorjungfrau/deg_psi_jungfrau500k_3->config_fname: "/users/blissadm/local/sls_detectors/config/jungfrau/detector/psi_jungfrau500k_307/setup/id22_switch_lid29p9jfrau1/sdk/slsdetector_x1.config"

# --- id00/slsdetectorjungfrau/deg_psi_jungfrau500k_3 attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorJungfrau properties
#---------------------------------------------------------


# CLASS SlsDetectorJungfrau attribute properties


