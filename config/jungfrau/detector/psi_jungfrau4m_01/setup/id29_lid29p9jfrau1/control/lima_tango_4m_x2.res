#
# Resource backup , created Tue Mar 22 17:34:14 CET 2022
#

#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, BackgroundSubstractionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/BackgroundSubstractionDeviceServer: "id29/backgroundsubstraction/jungfrau4m"


# --- id29/backgroundsubstraction/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS BackgroundSubstractionDeviceServer properties
#---------------------------------------------------------


# CLASS BackgroundSubstractionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, BpmDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/BpmDeviceServer: "id29/bpm/jungfrau4m"


# --- id29/bpm/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS BpmDeviceServer properties
#---------------------------------------------------------


# CLASS BpmDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtAccumulation device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtAccumulation: "id29/ctaccumulation/jungfrau4m"


# --- id29/ctaccumulation/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtAccumulation properties
#---------------------------------------------------------


# CLASS CtAccumulation attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtAcquisition device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtAcquisition: "id29/ctacquisition/jungfrau4m"


# --- id29/ctacquisition/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtAcquisition properties
#---------------------------------------------------------


# CLASS CtAcquisition attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtBuffer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtBuffer: "id29/ctbuffer/jungfrau4m"


# --- id29/ctbuffer/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtBuffer properties
#---------------------------------------------------------


# CLASS CtBuffer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtConfig device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtConfig: "id29/ctconfig/jungfrau4m"


# --- id29/ctconfig/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtConfig properties
#---------------------------------------------------------


# CLASS CtConfig attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtControl device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtControl: "id29/ctcontrol/jungfrau4m"


# --- id29/ctcontrol/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtControl properties
#---------------------------------------------------------


# CLASS CtControl attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtEvent device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtEvent: "id29/ctevent/jungfrau4m"


# --- id29/ctevent/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtEvent properties
#---------------------------------------------------------


# CLASS CtEvent attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtImage device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtImage: "id29/ctimage/jungfrau4m"


# --- id29/ctimage/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtImage properties
#---------------------------------------------------------


# CLASS CtImage attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtSaving device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtSaving: "id29/ctsaving/jungfrau4m"


# --- id29/ctsaving/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtSaving properties
#---------------------------------------------------------


# CLASS CtSaving attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtShutter device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtShutter: "id29/ctshutter/jungfrau4m"


# --- id29/ctshutter/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtShutter properties
#---------------------------------------------------------


# CLASS CtShutter attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, CtVideo device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/CtVideo: "id29/ctvideo/jungfrau4m"


# --- id29/ctvideo/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS CtVideo properties
#---------------------------------------------------------


# CLASS CtVideo attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, FlatfieldDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/FlatfieldDeviceServer: "id29/flatfield/jungfrau4m"


# --- id29/flatfield/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS FlatfieldDeviceServer properties
#---------------------------------------------------------


# CLASS FlatfieldDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, LimaCCDs device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/LimaCCDs: "id29/limaccds/jungfrau4m"


# --- id29/limaccds/jungfrau4m properties

id29/limaccds/jungfrau4m->BufferMaxMemory: 5
id29/limaccds/jungfrau4m->LimaCameraType: SlsDetectorJungfrau
id29/limaccds/jungfrau4m->NbProcessingThread: 120

# --- id29/limaccds/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS LimaCCDs properties
#---------------------------------------------------------


# CLASS LimaCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, LimaTacoCCDs device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/LimaTacoCCDs: "id29/limatacoccds/jungfrau4m"


# --- id29/limatacoccds/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS LimaTacoCCDs properties
#---------------------------------------------------------


# CLASS LimaTacoCCDs attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, LiveViewer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/LiveViewer: "id29/liveviewer/jungfrau4m"


# --- id29/liveviewer/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS LiveViewer properties
#---------------------------------------------------------


# CLASS LiveViewer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, MaskDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/MaskDeviceServer: "id29/mask/jungfrau4m"


# --- id29/mask/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS MaskDeviceServer properties
#---------------------------------------------------------


# CLASS MaskDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, PeakFinderDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/PeakFinderDeviceServer: "id29/peakfinder/jungfrau4m"


# --- id29/peakfinder/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS PeakFinderDeviceServer properties
#---------------------------------------------------------


# CLASS PeakFinderDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, Roi2spectrumDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/Roi2spectrumDeviceServer: "id29/roi2spectrum/jungfrau4m"


# --- id29/roi2spectrum/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS Roi2spectrumDeviceServer properties
#---------------------------------------------------------


# CLASS Roi2spectrumDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, RoiCollectionDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/RoiCollectionDeviceServer: "id29/roicollection/jungfrau4m"


# --- id29/roicollection/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS RoiCollectionDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCollectionDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, RoiCounterDeviceServer device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/RoiCounterDeviceServer: "id29/roicounter/jungfrau4m"


# --- id29/roicounter/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS RoiCounterDeviceServer properties
#---------------------------------------------------------


# CLASS RoiCounterDeviceServer attribute properties


#---------------------------------------------------------
# SERVER LimaCCDs/jungfrau4m, SlsDetectorJungfrau device declaration
#---------------------------------------------------------

LimaCCDs/jungfrau4m/DEVICE/SlsDetectorJungfrau: "id29/slsdetectorjungfrau/jungfrau4m"


# --- id29/slsdetectorjungfrau/jungfrau4m properties

id29/slsdetectorjungfrau/jungfrau4m->buffer_max_memory: 75
id29/slsdetectorjungfrau/jungfrau4m->buffer_packet_fifo_depth: 50000
id29/slsdetectorjungfrau/jungfrau4m->config_fname: "/users/blissadm/local/sls_detectors/config/jungfrau/detector/psi_jungfrau4m_01/setup/id29_lid29p9jfrau1/sdk/slsdetector_4m_x2.config"
id29/slsdetectorjungfrau/jungfrau4m->detector_id: 1
id29/slsdetectorjungfrau/jungfrau4m->det_asm_calib_file: "/data/id29/inhouse/opid291/Jungfrau/Calibration/2022-06-17/gains.h5"
id29/slsdetectorjungfrau/jungfrau4m->pixel_depth_cpu_affinity_map: "{16: (((CPU(  4), CPU(  5)), (CPU(  6), CPU(  7)),",\ 
                                                                   "       (CPU( 68), CPU( 69)), (CPU( 70), CPU( 71)),",\ 
                                                                   "       (CPU( 36), CPU( 37)), (CPU( 38), CPU( 39)),",\ 
                                                                   "       (CPU(100), CPU(101)), (CPU(102), CPU(103))),",\ 
                                                                   "      CPU(  8,  40,  72, 104),",\ 
                                                                   "      CPU(*chain(range(  8,  32), range( 40,  64),",\ 
                                                                   "                 range( 72,  92), range(104, 124))),",\ 
                                                                   "      CPU(*chain(range( 92,  95), range(124, 127))),",\ 
                                                                   "      (('enP5p1s0f0,enP5p1s0f1,enP48p1s0f1,mlx100c0p1,mlx100c1p1',",\ 
                                                                   "         {-1: (CPU( 31), CPU( 63))}),",\ 
                                                                   "       ('enP48p1s0f0,vlan200,vlan233', {-1: (CPU( 95), CPU(127))}),",\ 
                                                                   "       ('mlx100c0p0', {-1: (CPU(  0,  32), CPU(  1,   2,   3,  33,  34,  35))}),",\ 
                                                                   "       ('mlx100c1p0', {-1: (CPU( 64,  96), CPU( 65,  66,  67,  97,  98,  99))})),",\ 
                                                                   "      ('mlx100c0p0', 'mlx100c1p0'))",\ 
}

# --- id29/slsdetectorjungfrau/jungfrau4m attribute properties


#---------------------------------------------------------
# CLASS SlsDetectorJungfrau properties
#---------------------------------------------------------


# CLASS SlsDetectorJungfrau attribute properties


