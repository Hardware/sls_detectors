******************************************************
Instructions on the ID29 Jungfrau-4M calibration files
******************************************************

*Last update: 03/11/2023*

Introduction
============

The *ID29 Jungfrau-4M* detector control software, based on *Lima* and *Lima2*, require a set of configuration files with the geometry and intensity calibration. This document explains the mechanism for generating these files with the *gen_id29_jungfrau4m_calibration* script.

Filesystem directories
======================

Control software
----------------

The control software is installed on the control computers:

* *lid29p9jfrau1*
* *lid29pwr9*

under the directory specified by the *SLS_DETECTORS_DIR* environment variable (set in */etc/profile.d/sls_detectors.sh*):

::

   /users/blissadm/local/sls_detectors

Jungfrau-4M calibration
-----------------------

The *ID29 Jungfrau-4M* calibration files are in:

::

   /data/id29/inhouse/opid291/Jungfrau/Calibration

A basic history of the evolution is kept by creating a new sub-directory on each calibration update, referred to as *calibration tag*, containing the date information:

::

   (base) opid29@lid29pwr9:~$ ls -lart /data/id29/inhouse/opid291/Jungfrau/Calibration
   total 6
   drwxr-xr-x  2 opid29 jsbg 4096 Jun 17  2022 2022-04-15
   drwxr-xr-x  3 opid29 jsbg 4096 Jun 21  2022 2022-06-17
   drwxr-xr-x  2 opid29 jsbg 4096 Nov 15 10:04 2022-09-30
   drwxr-xr-x  2 opid29 jsbg 4096 Jan 18 16:30 2022-11-18
   drwxrwxr-x 14 basus  jsbg 4096 Feb  1 11:54 ..
   drwxr-xr-x  2 opid29 jsbg 4096 Apr 20 10:09 2023-01-19
   lrwxrwxrwx  1 opid29 jsbg   10 Apr 20 11:54 previous -> 2023-04-20
   drwxr-xr-x  2 opid29 jsbg 4096 Apr 20 12:32 2023-04-20
   drwxr-xr-x  2 opid29 jsbg 4096 Apr 24 12:19 2023-04-24
   drwxrwxrwx  9 opid29 jsbg 4096 Apr 24 13:55 .
   lrwxrwxrwx  1 opid29 jsbg   10 Apr 24 13:55 current -> 2023-04-24

The *current* symbolic link is used in order to keep the control software independent of the latest calibration. The *previous* link allows to roll-back after a faulty calibration.

pyFAI calibration
=================

Execute *pyFAI* on one of the above mentioned computers as *opid29* inside the *pyfai* conda environment:

::

   (base) opid29@lid29pwr9:~$ (conda activate pyfai && pyFAI-calib2)

The rings are best seen in the last *acc_peak* file of any receiver, inside the *accumulated* subdirectory. The detector definition file and the mask file can be loaded from the current calibration:

::

   /data/id29/inhouse/opid291/Jungfrau/Calibration/current/
       Jungfrau_4M_ID29.h5
       mask.h5

The resulting *Point Of Normal Incidence* (PONI) information file can be saved on that same directory:

::

   /data/id29/inhouse/opid291/Jungfrau/Calibration/current/
       calibration-2023-04-20-CeO2.poni

Script invocation
=================

The script must be executed as *opid29* on both *lid29p9jfrau1* and *lid29pwr9* control computers inside a *conda* environment:

* On *lid29p9jfrau1* use the *jungfrau_lima2* environment:

::

   (conda activate jungfrau_lima2 && \
      cd ${SLS_DETECTORS_DIR} && \
      python jungfrau/scripts/gen_id29_jungfrau4m_calibration \
        --pyfai_poni /data/id29/inhouse/opid291/Jungfrau/Calibration/current/calibration-2023-04-20-CeO2.poni)

* On *lid29pwr9* use the *lima2* environment:

::

   (conda activate lima2 && \
      cd ${SLS_DETECTORS_DIR} && \
      python jungfrau/scripts/gen_id29_jungfrau4m_calibration \
        --pyfai_poni /data/id29/inhouse/opid291/Jungfrau/Calibration/current/calibration-2023-04-20-CeO2.poni)

Calibration tag
---------------

The *gen_id29_jungfrau4m_calibration* script will attempt to create a new entry in the calibration directory named after the calibration tag. The default tag is the date using the *%Y-%m-%d* time format; it can be changed with the *--cal_tag* option:

::

   (conda activate jungfrau_lima2 && \
      cd ${SLS_DETECTORS_DIR} && \
      python jungfrau/scripts/gen_id29_jungfrau4m_calibration \
        --cal_tag 2023-04-19-2 --sample_distance 150.07 --beam_center 1068.0,1135.0)

When a new entry is created, the script will copy the basic detector data from the *current* entry:

* *gainMaps_JF4MID29*: (symbolic link to) directory with gain factory calibration files: *gainMaps_Mxxx_YY-MM-DD.bin*
* *Jungfrau_4M_ID29.h5*: Spatial distortion
* *hot_cold_mask.h5*: Last hot/cold pixels
* *beamstop_geometry.json*: Beam-stop mask geometry
* *peakfinder_params.json*: *Lima2 SMX peakfinder* parameters

Update on sample-detector geometry
----------------------------------

Part of the calibration must be updated each time that the sample-detector geometry changes. As shown in the examples above, the *--pyfai_poni* option allows specifying a new *pyFAI* detector geometry configuration, which can be created with the *pyFAI-calib2* utility in the *pyfai* conda environment (currently @ *lid29pwr9* only):

::

   (conda activate pyfai && pyFAI-calib2)

The *pyFAI PONI* file can be saved in any directory, including */tmp_14_days*. It will be copied to the new calibration entry directory if the source is located in another directory.

An approximate geometry can also be specified with the *--sample_distance* (in millimeters) and *--beam_center* (in pixels) options:

::

   (conda activate jungfrau_lima2 && \
      cd ${SLS_DETECTORS_DIR} && \
      python jungfrau/scripts/gen_id29_jungfrau4m_calibration \
        --sample_distance 150.07 --beam_center 1068.0,1135.0)

Calibration file stages
=======================

The script systematically builds the calibration files from the basic information copied from the *current* entry. The following stages are performed sequentially, affecting both *Lima* and *Lima2* DAQ control applications.

The first stages are basic verifications on the running context: user ID, host name and conda enviroment, as well as the availability of the necessary scripts that perform individual tasks.

Parse the Lima2 configuration & obtain basic detector geometry
--------------------------------------------------------------

The *Lima2 Tango* configuration is read and the *PSI* plugin configuration in *JSON* format is decoded in order to obtain the basic detector geometry (w/o gaps) and the number of UDP interfaces.

Assembly With Gap (AsmWG) map & raw packet geometry LUT
-------------------------------------------------------

The SDK detector geometry calculation utility *sls_geometry_assembler* is invoked to generate the *Raw -> AsmWG* transformation table *pixel_map_wg.h5*, from which the *Packet -> AsmWG* Look-Up-Table (LUT) *psi_geom_lut.h5* is obtained.

Module index in the SDK config
------------------------------

The effective module order in the SDK configuration is found for the next stage.

Gain calibration maps
---------------------

The detector G0, G1 & G2 pixel calibration map file *gains_wg.h5* is built from the primary factory module calibration files and the *AsmWG* geometry map.

Uncorrected pyFAI detector pixel geometry
-----------------------------------------

A primitive *pyFAI* detector geometry definition, *Jungfrau_4M_ID29_uncorrected.h5*, is also created (although it is currently unused).

Sample-detector geometry
------------------------

The specified geometry is stored in a *calib_params_YYYY-MM-DD.txt* file, which is pointed to by the *calib_params.txt* symbolic link.

::

   (base) opid29@lid29pwr9:~$ cat /data/id29/inhouse/opid291/Jungfrau/Calibration/current/calib_params.txt
   {
     "sample_distance": 150.8918866288234,
     "beam_center": [
       1071.3598528310538,
       1136.4790666333665
     ]
   }

It is used by *mxCube* to populate the *MX metadata* in the *master* files, stored in the *aggregated* data collection sub-directory.

*Lima2 pyFAI-based SMX* processing
----------------------------------

The sample-detector geometry is used to create the files used by *Lima2 SMX* processing pipeline:

* *csr_wg.h5*: pyFAI pixel -> ring integration bin matrix in CSR format
* *bin_centers_wg.h5*: integration bin -> radius table (*radius1d*)
* *r_centers_wg.h5*: pixel -> radius map (*radius2d*)

Update the hot/cold pixel mask
------------------------------

The hot/cold pixel mask can be updated by analyzing dark data (dense) files, which can be specified with the *--dark_data_dir* and the *--dark_data_files* options. The *--dark_data_files* option can specify a single file name or a *glob* pattern:

::

   (conda activate jungfrau_lima2 && \
     cd ${SLS_DETECTORS_DIR} && \
     python jungfrau/scripts/gen_id29_jungfrau4m_calibration \
       --pyfai_poni /data/id29/inhouse/opid291/Jungfrau/Calibration/2023-09-12/calibration-2023-10-25-CeO2.poni \
       --dark_data_dir /data/visitor/mx2545/id29/20231102/RAW_DATA/jungfrau_dark/jungfrau_dark/run_1 \
       --dark_data_files "jungfrau_dark-jungfrau_dark_dense_*.h5")

**Note**: Please mind the quotes (") around the file name pattern passed to the *--dark_data_files* option. If a *glob* pattern is specified instead of a file name the quotes are mandatory in order to avoid its expansion by the shell.

If *--dark_data_files* is provided, *gen_id29_jungfrau4m_calibration* internally calls the *gen_hot_cold_mask* script, and can also pass the information specified through the following options:

* *--cold_thres*: pixel intensity threshold to consider a pixel cold.
* *--hot_thres*: pixel intensity threshold to consider a pixel hot.
* *--min_occurrence*: minimum occurrence in a series of images to consider a hot/cold event in a pixel (in % of total frames).
* *--frames_per_file*: limit the numer of frames to read in each dark file.

The hot/cold pixel mask can also be reset with the *--reset_hot_cold* option. After that images will include all the hot/cold pixels in the detector, so the script must be executed again the new dark files to create a new hot/cold mask.

Beam-stop & holder mask
-----------------------

A mask file with the beam-stop (and its holder) *mask_beamstop_YYYY-MM-DD.h5* is created based on the *beam_center* calibration parameter, together with its corresponding *mask_beamstop.h5* symbolic link.

The automatic script now reads the file beamstop_geometry.json in the calibration directory:

::

   (base) opid29@lid29pwr9:~$ cat /data/id29/inhouse/opid291/Jungfrau/Calibration/current/beamstop_geometry.json
   {
     "size": 55,
     "holder": [[0, -12], [240, -12], [455, -13], [2048, -13],
                [2048, 10], [455, 10], [240, 8], [0, 7]]
   }

where *size* is the diameter of the beam-stop circle, and *holder* is a list of (X, Y) displacements, measured from the beam-center, which generates a polygon. Negative Y disp. means "up" when the image is displayed with growing Y downwards ("down" arrow in *silx* display).

Final masks with and without beam-stop
--------------------------------------

The hot/cold pixel mask and the beam-stop mask are merged into the final *mask_hot_cold_beamstop_YYYY-MM-DD.h5* mask. The symbolic links *mask_beamstop.h5* & *mask_no_beamstop.h5* are created for *mxCube* reference.

Symbolic links used by the *Lima2 SMX* pipeline
-----------------------------------------------

Symbolic links without the *_wg* suffix are created to the different calibration files.

Instruction on manual *current* calibration update
--------------------------------------------------

The *current* calibration tag update is not performed automatically. A message with the instruction to do so is printed:

::

   In order to test the new calibration perform:
     (cd /data/id29/inhouse/opid291/Jungfrau/Calibration && \
         rm -f previous && \
         mv current previous && \
         ln -s 2023-04-24 current)

Revert last calibration update
------------------------------

In case that a calibration is revoked and the previous one must be restored, the following command can be used:

::

   (cd /data/id29/inhouse/opid291/Jungfrau/Calibration && \
       rm -f revoked && \
       mv current revoked && \
       mv previous current)
