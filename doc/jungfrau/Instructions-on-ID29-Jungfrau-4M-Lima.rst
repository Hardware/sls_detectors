*****************************************************
Instructions on the ID29 Jungfrau-4M software control
*****************************************************

*Last update: 15/04/2022*

Introduction
============

The *ID29 Jungfrau-4M* detector can be controlled with a *Lima/Tango/Python* device server through the *SlsDetector* camera plugin, which can be driven from *Bliss*.


Detector power control
======================

The *Jungfrau-4M* power supply can be controlled remotely through a *telnet* connection to `jfrau4mid29ctrl`:

::

   $ telnet jfrau4mid29ctrl
   Trying 160.103.49.88...
   Connected to jfrau4mid29ctrl.esrf.fr.
   Escape character is '^]'.


   BusyBox v1.16.2 (2012-03-09 11:39:48 CET) hush - the humble shell
   Enter 'help' for a list of built-in commands.

   root:/>

The commands `on`, `off` & `monitor` in the `/power_control_user` directory can be used to start, stop & monitor the power supply, respectively:

::

   root:/> ls power_control_user
   monitor  off      on       toggle

   root:/> power_control_user/monitor
   flowmeter temperature [m°C]:cat: can't open '/power_control/hwmon/gfm*/temp_input': No such file or directory
   flowmeter water flow [ml/min]:cat: can't open '/power_control/hwmon/gfm*/flow_input': No such file or directory
   power supply 0 temperature [m°C]:39600
   power supply 0 voltage [mV]:11792
   power supply 0 current [mA]:28851

   root:/> power_control_user/off

   root:/> power_control_user/monitor
   flowmeter temperature [m°C]:cat: can't open '/power_control/hwmon/gfm*/temp_input': No such file or directory
   flowmeter water flow [ml/min]:cat: can't open '/power_control/hwmon/gfm*/flow_input': No such file or directory
   power supply 0 temperature [m°C]:36700
   power supply 0 voltage [mV]:0
   power supply 0 current [mA]:2934

   root:/> power_control_user/on

   root:/> power_control_user/monitor
   flowmeter temperature [m°C]:cat: can't open '/power_control/hwmon/gfm*/temp_input': No such file or directory
   flowmeter water flow [ml/min]:cat: can't open '/power_control/hwmon/gfm*/flow_input': No such file or directory
   power supply 0 temperature [m°C]:36200
   power supply 0 voltage [mV]:11776
   power supply 0 current [mA]:7824


Software installation
=====================

Lima/Tango server
-----------------

The low-level control software runs on `lid29p9jfrau1`, an *IBM Power9 AC-922* server located on the Central Building Data Center. The software is manually installed on the `blissadm` account, under the directory:

::

   /users/blissadm/local/sls_detectors

The base infrastructure uses *Conda* for Python and other dependencies.

The Tango server uses a *Lima-core* development version on top of `v1.9.16`. The *SlsDetector* plugin is based on the *slsDetectorPackage* `v6.1.0` with a *Round-Robin Receiver* (`rr_rxr`) patch. The *Lima* server and its libraries are installed on the `jungfrau_lima1` conda environment.

The `LimaCCDs` *Tango* instance name is `jungfrau4m`.

Memory allocation
^^^^^^^^^^^^^^^^^

The *SlsDetectorJungfrau* device is configured (through the `buffer_max_memory` Tango property) to use up to 75 % of the computer memory (384 GB) for low-level packet data (implying `buffer_max_nb_buffers` ~50,000 frames). This is maximum number of images that *Lima1* can acquire in a single acquisition at the maximum frame rate of 1 kHz. On top of that, *Lima* can use up to 5 % of the computer memory for reconstructed (assembled) frames.
 
The actual amount of memory allocated for packet data is available through the `buffer_packet_fifo_depth` Tango attribute, which is always within the range `[128..buffer_max_nb_frames]`. The `buffer_resize_policy` Tango attribute determines how the Fifo depth is obtained:

* `AUTO`: the `buffer_packet_fifo_depth` is dynamically determined at each `prepareAcq` depending on the requested `acq_nb_frames`. That means that new buffers are allocated each time that a longer acquisition starts. The same applies for shorter acquisitions, where low-level buffer memory is released. This policy can consume a significant amount of time during the *Lima* preparation if the number of frames in the sequence oscillates (for instance, between 1000 and 50,000). `AUTO` is the default policy.

* `MANUAL`: the user can freely set the `buffer_packet_fifo_depth`.

Finally, the `buffer_packet_fifo_depth` Tango property, fixing the Fifo depth at startup with the `MANUAL` policy, is set to 50,000 for the `jungfrau4m`.


`lima_launcher` & `supervisor`
------------------------------

The *Jungfrau-4M* detector sends data in UDP packets, which risk of data loss if the computer is not properly setup. `lima_launcher` is used in order to configure specific CPU affinities for the different system tasks like the hardware IRQs, network stack tasklets, daemons & *GPFS* client, as well as the Lima receiver & processing threads.

`supervisor` is configured to run `lima_launcher`, which is installed in the `lima_launcher` conda environment. As usual, *Lima* runs as `opid29`, so the HDF5 data files can be easily saved on NICE.

Both the initialization and clean-up of `set_cpu_affinity` & `LimaCCDs/SlsDetector` can take an important amount of time. `supervisor` has been configured to wait for at least *2 min for startup* and *3 min for stop*.

In addition to the `multivisor web interface <http://id29control.esrf.fr/multivisor>`, the `supervisorctl` command can be called by any user on `lid29p9jfrau1` to start, stop, monitor or follow the output of the *Lima/Tango* device server:

::

   blissadm@lid29p9jfrau1:~$ supervisorctl status
   LIMA:lima_jungfrau4m             STOPPED   Apr 13 07:41 PM

   blissadm@lid29p9jfrau1:~$ supervisorctl start LIMA:lima_jungfrau4m
   LIMA:lima_jungfrau4m: started

   blissadm@lid29p9jfrau1:~$ supervisorctl tail -f LIMA:lima_jungfrau4m
   ...
   [2022/04/14 13:36:02.398681] 7c58d48146f0 *Application*Lima.Server.LimaCCDs::LimaCCDs::apply_config-Always: Applied config : /users/opid29/lima_jungfrau4m.cfg : default


*Bliss* client
--------------

A *Bliss* session called `jungfrau` has been configured; it has been tested on the `id29control` computer through the command:

::

   (. blissenv && bliss -s jungfrau)

The *Lima/Tango* python detector object is accessed through the name `jungfrau4m`:

::

   JUNGFRAU [1]: jungfrau4m
        Out [1]: SlsDetector - PSI/Jungfrau-4M (SlsDetector) - Lima SlsDetectorJungfrau

                 Image:
                 width:    2069
                 height:   2164
                 depth:    2
                 bpp:      Bpp16
                 binning:  [1, 1]
                 flip:     [False, False]
                 rotation: 0
                 roi:      [0, 0, 2069, 2164]

                 Acquisition:
                 status: |'Ready'|
                 status_fault_error: |'No error'|
                 mode: 'SINGLE'
                 nb_frames: 1
                 expo_time: 1.0
                 trigger_mode: 'INTERNAL_TRIGGER'

                 ROI Counters: default
                 *** no ROIs defined ***

                 Roi Profile Counters: default
                 *** no ROIs defined ***

                 ROI Collection: default
                 *** no ROI defined ***

                 BPM Counters:
                 acq_time, intensity, x, y, fwhm_x, fwhm_y

                     Saving
                 --------------
                 File Format:   HDF5
                  └->  Suffix:  .h5
                 Current Mode:  ONE_FILE_PER_N_FRAMES
                 Available Modes:
                  - ONE_FILE_PER_FRAME
                  - ONE_FILE_PER_SCAN
                  - ONE_FILE_PER_N_FRAMES
                  - SPECIFY_MAX_FILE_SIZE

                 for ONE_FILE_PER_N_FRAMES mode
                 ------------------------------
                 frames_per_file: 1000

                 for SPECIFY_MAX_FILE_SIZE mode
                 ------------------------------
                 max file size (MB):  500
                  └-> frames per file: 59

                 Expert Settings
                 ---------------
                 config max_writing_tasks:  1
                 current max_writing_tasks: 1
                 lima managed_mode:         SOFTWARE


                 Mask
                 ----
                 use mask: False
                 mask image path: ** UNSET **

                 Flatfield
                 ---------
                 use flatfield: False
                 flatfield image path: ** UNSET **

                 Background Substraction
                 -----------------------
                 use background: False
                 background source: file (Take bg-image from file)
                 background image path: ** UNSET **

                 Expert Settings
                 ---------------
                 Lima Run-Level:
                    Mask            0
                    Flatfield:      1
                    Bg-Sub:         2
                    Roi Counters:   10
                    Roi Profiles:   10
                    Roi Collection: 10
                    BPM:            10


The corresponding measurement group is `jungfrau4m_mg`:

::

   JUNGFRAU [2]: jungfrau4m_mg.set_active()


`limatake`
----------

The *Bliss* command `limatake` uses the simplest scan framework configuration. It sets the detector in internal trigger and allows taking a sequence of images, which can be optionally saved. The acquired images can be monitored with *flint*, which is automatically started if `SCAN_DISPLAY.auto` is `True`:

::

   SCAN_DISPLAY.auto = True

The detector is expected to work around 10 usec exposure time. If used in internal trigger, the desired frame rate must be specified in terms of the `latency_time` parameter, which represents the interval between the end of one exposure and the start of the next one. Letting :math:`P` be the `point_period`, :math:`E` the `exposure_time`, :math:`L` the `latency_time` and :math:`R` the `frame_rate`, then

:math:`P = E + L = 1 / R \Rightarrow L = 1 / R - E`

In order to acquire 30,000 frames at 1 kHz in internal trigger the `limatake` command to use is:

::

   limatake(10e-6, 30000, jungfrau4m, latency_time=990e-6, save=True)

If external trigger is used, the `acq_trigger_mode` must be explicitly specified. The exposure time is still internally controlled. An example of 400 frames with 10 usec exposure:

::

   limatake(10e-6, 400, jungfrau4m, save=True, acq_trigger_mode='EXTERNAL_TRIGGER_MULTI')


Saving
------

The data from the tests is currently saved on `/data/id29/inhouse`:

::

   JUNGFRAU [7]: SCAN_SAVING
        Out [7]: Parameters (default) -

                   .base_path            = '/data/id29/inhouse/opid291/Jungfrau/tests/opid29/lima1'
                   .data_filename        = 'data'
                   .template             = '{session}/'
                   .images_path_relative = True
                   .images_path_template = 'scan{scan_number}'
                   .images_prefix        = '{img_acq_device}_'
                   .date_format          = '%Y%m%d'
                   .scan_number_format   = '%04d'
                   .session              = 'jungfrau'
                   .date                 = '20220413'
                   .user_name            = 'opid29'
                   .scan_name            = '{scan_name}'
                   .scan_number          = '{scan_number}'
                   .img_acq_device       = '<images_* only> acquisition device name'
                   .writer               = 'nexus'
                   .data_policy          = 'None'
                   .creation_date        = '2022-03-22 16:46:13'
                   .last_accessed        = '2022-04-13 18:57:39'
                 ------  ---------  -----------------------------------------------------------------------
                 exists  filename   /data/id29/inhouse/opid291/Jungfrau/tests/opid29/lima1/jungfrau/data.h5
                 exists  directory  /data/id29/inhouse/opid291/Jungfrau/tests/opid29/lima1/jungfrau
                 ------  ---------  -----------------------------------------------------------------------

.. note:: The *ESRF data policy* is not configured.

By default *Lima* saves in `HDF5` (no compression) format with 1000 frames per file:

::

   JUNGFRAU [8]: jungfrau4m.saving.file_format
        Out [8]: 'HDF5'

   JUNGFRAU [9]: jungfrau4m.saving.frames_per_file
        Out [9]: 1000

*Jungfrau* raw data (with no gain/pedestal correction) does not compress very well. It is recommended to use `HDF5` plain (uncompressed) saving format in order to minimize inter-CPU data transfer that can affect low-level UDP packet reception reliability.


Gain & Pedestal Correction
--------------------------

Principle
^^^^^^^^^

The *SlsDetector* plugin uses the `sls::FrameAssembler` to reconstruct the image from the UDP packets with the right detector module geometry, including gap pixels (internal `AsmWithGap` mode). The *Jungfrau* pixel raw data is 16-bit wide, composed by a 2-bit gain and a 14-bit ADC value. By default the 16-bit coded data is injected to *Lima* as pixel intensity, which is displayed and saved by the processing tasks. For low illumination regimes, where all the pixels are in *Gain0*, the gain-coded data is just a map of ADC values that need to be background-corrected. However, when the photo flux increases, more and more pixels will dynamically switch to *Gain1* or *Gain2*, breaking the linearity of the resulting 16-bit value.

The *SlsDetector* plugin can apply the so-called *gain & pedestal* correction as a part of the frame reconstruction task. The correction uses pedestal maps for the 3 gains (0: `DYNAMIC`, 1: `FORCE_SWITCH_G1`, 2: `FORCE_SWITCH_G2`), as well as the fine callibration maps for each gain. *Lima* is not well suited for floating point values, so the result of the correction is stored in a 32-bit integer value that corresponds to the *Gain0* scale. The linearized value, which is already background-corrected, can be used by  the standard *Lima* processing tasks like RoI counters, exploiting the single-photon resolution of the *Gain0* at the same time of the 23-bit dynamic range of the sensor electronics.

Pedestal measurement
^^^^^^^^^^^^^^^^^^^^
In order to use the *gain & pedestal* correction the pedestal maps with the background values for each gain must be acquired. The *Bliss* macro `jungfrau_pedestal_scans` acquires the pedestal maps for the three gains and uploads them into the *Lima* server; it receives the *Jungfrau* detector and `exposure_time`, `nb_frames`, `frame_rate` (plus some options):

::

   JUNGFRAU [10]: jungfrau_pedestal_scans(jungfrau4m, 10e-6, 1000, 1e3)
   Setting gain DYNAMIC
   acquisition chain
   └── jungfrau4m

   Scan 27 Fri Apr 15 11:03:52 2022 /data/id29/inhouse/opid291/Jungfrau/tests/opid29/lima1/jungfrau/data.h5 jungfrau user = opid29
   limatake 0.0000 1000
   Preparing jungfrau4m ...
   Running ...
   jungfrau4m acq #1000 save #1000
   Finished (took 0:00:15.826209)

   Setting gain FORCE_SWITCH_G1
   acquisition chain
   └── jungfrau4m

   Scan 28 Fri Apr 15 11:04:11 2022 /data/id29/inhouse/opid291/Jungfrau/tests/opid29/lima1/jungfrau/data.h5 jungfrau user = opid29
   limatake 0.0000 1000
   Preparing jungfrau4m ...
   Running ...
   jungfrau4m acq #1000 save #1000
   Finished (took 0:00:17.822132)

   Setting gain FORCE_SWITCH_G2
   acquisition chain
   └── jungfrau4m

   Scan 29 Fri Apr 15 11:04:35 2022 /data/id29/inhouse/opid291/Jungfrau/tests/opid29/lima1/jungfrau/data.h5 jungfrau user = opid29
   limatake 0.0000 1000
   Preparing jungfrau4m ...
   Running ...
   jungfrau4m acq #1000 save #1000
   Finished (took 0:00:15.621146)

   Setting gain DYNAMIC

.. note:: It is important to acquire the pedestal maps with the same timing (`exposure_time` and `frame_rate`) that will be used during the scans. In the previous example 10 usec and 1 kHz are used, respectively; 1000 frames are taken and averaged for each gain.

Image source
^^^^^^^^^^^^

The *SlsDetector* plugin `img_src` attribute selects the kind of data injected into *Lima*. By default it injects the raw data:

::

   JUNGFRAU [11]: jungfrau4m.camera.img_src
        Out [11]: 'RAW'

Once the pedestal maps have been acquired and uploaded with `jungfrau_pedestal_scans`, the corrected data can be injected into *Lima*:

::

   JUNGFRAU [12]: jungfrau4m.camera.img_src = 'GAIN_PED_CORR'

From this point on *Lima* will process, display and save corrected data in 32-bit signed format. The background noise average should be around 0, with a FWHM of about +/- 50 ADUs:

::

   JUNGFRAU [13]: limatake(10e-6, 1000, jungfrau4m, latency_time=990e-6, save=True)
   Scan 43 Fri Apr 15 11:55:55 2022 /data/id29/inhouse/opid291/Jungfrau/tests/opid29/lima1/jungfrau/data.h5 jungfrau user = opid29
   limatake 0.0000 1000
   Preparing jungfrau4m ...
   Running ...
   jungfrau4m acq #1000 save #1000
   Finished (took 0:00:15.921159)

        Out [13]: Scan(number=43, name=limatake, path=/data/id29/inhouse/opid291/Jungfrau/tests/opid29/lima1/jungfrau/data.h5)

   JUNGFRAU [14]: image = SCANS[-1].get_data('jungfrau4m:image').get_last_image().data
   JUNGFRAU [15]: valid_pixels = (image > -0x7f000000)
   JUNGFRAU [16]: valid_pixels.sum() == 2048 * 2048
        Out [16]: True

   JUNGFRAU [17]: hot_pixels = (image > 5000)
   JUNGFRAU [18]: hot_pixels.sum()
        Out [18]: 230

   JUNGFRAU [19]: image[valid_pixels & ~hot_pixels].mean()
        Out [19]: 2.4125454152692587

   JUNGFRAU [20]: image[valid_pixels & ~hot_pixels].std()
        Out [20]: 150.97486584993706

.. note:: The pedestal maps evolve with the temperature, so it is convenient to run `jungfrau_pedestal_scans` accordingly to update the *Lima* tables, in particular during the warm-up period of the detector.

.. note:: The *gain & pedestal* correction consumes an important amount of CPU resources. The effective acquisition speed is reduced by a factor ~6 when the detector is triggered at 1 kHz.

The `img_src` attribute can be freely switched between `RAW` and `GAIN_PED_CORR`. There will be a delay after each transition for the corresponding allocation of the high-level *Lima* buffers.

Calibration
^^^^^^^^^^^

By default the *SlsDetector* calibration is initialized with three homogenous (flat) maps with values based on the `nominal ADC value vs photon table <https://indico.cern.ch/event/686575/contributions/2999076/attachments/1673650/2686017/Redford_JUNGFRAU_good.pdf>`. The *ID29 Jungfrau-4M* server is configured, through the `det_asm_calib_file` property, to load the calibration at startup:

::

   id29/slsdetectorjungfrau/jungfrau4m->det_asm_calib_file: "/data/id29/inhouse/opid291/Jungfrau/Calibration/2022-06-17/gains.h5"

   
As a result, the *gain & pedestal* correction will use precise values, as can be seen the surroundings of the left-most inter-chip corner of the top-left module (0 values correspond to gap pixels):

::

   JUNGFRAU [30]: jungfrau4m._get_proxy('SlsDetectorJungfrau').gain_0_calib_map[254:260,254:260]
        Out [30]: array([[0.99049136, 0.99838808, 0.        , 0.        , 1.01873773,
                          1.01680335],
                         [1.00338619, 0.99031777, 0.        , 0.        , 1.0106512 ,
                          1.02222779],
                         [0.        , 0.        , 0.        , 0.        , 0.        ,
                          0.        ],
                         [0.        , 0.        , 0.        , 0.        , 0.        ,
                          0.        ],
                         [1.08856683, 1.07387058, 0.        , 0.        , 0.97100414,
                          0.97602869],
                         [1.0689695 , 1.08808761, 0.        , 0.        , 0.97805879,
                          0.98138088]])



`putCmd` & `getCmd`
-------------------

The *SlsDetector* plugin can send and receive low-level commands to/from the *slsDetectorPackage* layer through the `putCmd` & `getCmd` Tango commands, respectively:

::

   JUNGFRAU [20]: versions = jungfrau4m._get_proxy('SlsDetectorJungfrau').getCmd('versions').split('\n')
   JUNGFRAU [21]: for l in versions:
             ...:     print(l)
   versions
   Detector Type: Jungfrau
   Package Version: developer
   Client Version: 0x211125
   Firmware Version: 0x211008
   Detector Server Version: 0x220321
   Detector Server Version: #8 Fri Oct 29 09:04:20 CEST 2021
   Receiver Version: 0x211124


Other commands
--------------

Other commands using the *Bliss* scan framework can be used, like `loopscan` or `dscan`. However, the *Jungfrau* does *not* support the `INTERNAL_TRIGGER_MULTI` mode, so *Bliss* will configure *Lima* to take one-frame sequences each point. Although this is not the optimal configuration, it allows performing alignment scans thanks to *Lima RoI-counters*.


Diagnostics
===========

The output of the *Lima/Tango* device server shows a summary of the low-level packet reception stage of each `slsReceiver::Listener thread` (16 in total). As mentioned before, it is available available in `multivisor` and through the `supervisorctl` command. A successful 30,000 frame acquisition will display, through the `LimaCCDs` output, something like:

::

   - 13:39:28.009 INFO: Status: finished
   - 13:39:28.010 INFO: Summary of Port 32416
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.009 INFO: Status: finished
   - 13:39:28.010 INFO: Summary of Port 33426
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Status: finished
   - 13:39:28.010 INFO: Summary of Port 32412
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.009 INFO: Status: finished
   - 13:39:28.010 INFO: Summary of Port 32414
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Receiver Stopped
   - 13:39:28.010 INFO: Summary of Port 33424
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Receiver Stopped
   - 13:39:28.010 INFO: Summary of Port 33422
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Status: idle
   - 13:39:28.010 INFO: Status: idle
   - 13:39:28.010 INFO: Receiver Stopped
   - 13:39:28.010 INFO: Summary of Port 32410
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Summary of Port 33420
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Status: idle
   - 13:39:28.010 INFO: Receiver Stopped
   - 13:39:28.010 INFO: Status: idle
   - 13:39:28.010 INFO: Status: finished
   - 13:39:28.010 INFO: Summary of Port 32411
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Status: finished
   - 13:39:28.010 INFO: Summary of Port 32413
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Summary of Port 33421
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Summary of Port 33423
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Receiver Stopped
   - 13:39:28.010 INFO: Status: idle
   - 13:39:28.010 INFO: Receiver Stopped
   - 13:39:28.010 INFO: Status: finished
   - 13:39:28.010 INFO: Status: finished
   - 13:39:28.010 INFO: Status: idle
   - 13:39:28.010 INFO: Summary of Port 32415
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Summary of Port 32417
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Summary of Port 33425
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Summary of Port 33427
           Missing Packets         : 0
           Complete Frames         : 30000
           Last Frame Caught       : 30000
   - 13:39:28.010 INFO: Receiver Stopped
   - 13:39:28.010 INFO: Receiver Stopped
   - 13:39:28.010 INFO: Status: idle
   - 13:39:28.010 INFO: Status: idle
   [2022/04/14 13:39:28.011173] 7c53567cf180 *Camera*SlsDetector::Camera::AcqThread::threadFunction (/opt/bliss/src/sls_detectors/Lima/camera/slsdetector/src/SlsDetectorCamera.cpp:271)-Always: stats=<cb_period=<min=26, max=727978, ave=2912, std=12474, n=239992, hist={}>, new_finish=<min=0, max=0, ave=0, std=0, n=0, hist={}>, cb_exec=<min=0, max=149250, ave=34, std=334, n=240000, hist={}>, recv_exec=<min=25, max=727972, ave=2877, std=12475, n=239992, hist={}>>
   [2022/04/14 13:39:28.011217] 7c53567cf180     *Camera*SlsDetector::GlobalCPUAffinityMgr::recvFinished (/opt/bliss/src/sls_detectors/Lima/camera/slsdetector/src/SlsDetectorCPUAffinity.cpp:1757)-Always: Skipping processing

In a `VT100`-compatible terminal, the summary lines will be displayed in `green` if all the UDP packets were received, or in `red` if data was lost.


Known issues
============

`/data/id29/inhouse` mounted through NFS
----------------------------------------

The `/data/id29/inhouse` directory should be accessed from `lid29p9jfrau1` through GPFS. However, sometimes it is accidentally mounted through NFS. This notably slows down the effective data saving speed. The solution is to manually unmount the NFS volume:

::

   sudo umount /data/id29/inhouse

Next time it is used, it should be automatically accessed through GPFS.
   
