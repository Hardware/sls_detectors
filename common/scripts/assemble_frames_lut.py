#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import h5py as h5

parser = argparse.ArgumentParser(description='Assemble frames with LUT.')
parser.add_argument('--first_frame', type=int, default=0,
                    help='First frame to assemble')
parser.add_argument('--nb_frames', type=int, default=0,
                    help='Number of frames to assemble')
parser.add_argument('--gap_pixel_val', type=int, default=0,
                    help='Gap pixels value')
parser.add_argument('--data_path', default='/entry_0000/measurement/data',
                    help='HDF5 data path')
parser.add_argument('input_fname', help='Input file name')
parser.add_argument('lut_geom', help='HDF5 file with LUT')
parser.add_argument('output_fname', help='Output file name')

args = parser.parse_args()

with h5.File(args.lut_geom, 'r') as f:
    lut = f['/data'][:]

print(f'Detector shape={lut.shape}')

with h5.File(args.output_fname, 'w') as ofile:
    with h5.File(args.input_fname, 'r') as ifile:
        in_dataset = ifile[args.data_path]
        print(f'Input dataset shape={in_dataset.shape}')
        nb_frames = in_dataset.shape[0]
        if args.nb_frames:
            nb_frames = min(args.nb_frames, nb_frames)
        out_shape = [nb_frames] + list(lut.shape)
        print(f'Output dataset shape={out_shape}')
        out_dataset = np.zeros(out_shape, in_dataset.dtype)
        print(f'Assembling {nb_frames} frames ...')
        gap_pixels = (lut < 0)
        for i, o in zip(in_dataset[:nb_frames], out_dataset):
            o[...] = i.flatten()[lut]
            o[gap_pixels] = np.array(args.gap_pixel_val).astype(in_dataset.dtype)
        ofile.create_dataset(args.data_path, data=out_dataset)
