# Setup the SLS detector data acquisition environment

# Ensure global profile setter is loaded
if [ -z "${SLS_DETECTORS_DIR}" ]; then
    global_profile="/etc/profile.d/sls_detectors.sh"
    if [ -f "${global_profile}" ]; then
	. ${global_profile}
    else
	echo "Warning: SLS_DETECTORS_DIR empty and ${global_profile} not found"
    fi
    unset global_profile
fi

# If requested, include the conda environment
if [ -n "${SLS_DETECTOR_CONDA_ENV}" ]; then
    conda_type=$(type conda 2> /dev/null | head -n 1)
    if [ "${conda_type}" != "conda is a function" ]; then
        CONDA_BASE="/users/blissadm/conda/miniconda"
        . ${CONDA_BASE}/etc/profile.d/conda.sh
    fi
    if conda env list | grep -q ${SLS_DETECTOR_CONDA_ENV}; then
        conda activate ${SLS_DETECTOR_CONDA_ENV}
    fi
fi

LIMA_DIR=${SLS_DETECTORS_DIR}/Lima
export LIMA_DIR

COMMON=${SLS_DETECTORS_DIR}/common
PATH=${COMMON}/tango:${COMMON}/scripts:${PATH}
export PATH
unset COMMON

if [ -n "${SLS_DETECTOR_TYPE}" ]; then
    PATH=${SLS_DETECTORS_DIR}/${SLS_DETECTOR_TYPE}/scripts:${PATH}
    export PATH

    if [ "${SLS_DETECTOR_TYPE}" = "eiger" ]; then
	if [ -n "${SSH_AUTH_SOCK}" ]; then
	    ssh_key_dir="${SLS_DETECTORS_DIR}/eiger/ssh"

	    EIGER_SSH_CONFIG="${ssh_key_dir}/config"
	    export EIGER_SSH_CONFIG

	    function check_ssh_key
	    {
		[ -r ${ssh_key_dir}/id_dsa -a -r ${ssh_key_dir}/id_dsa.pub ]
	    }

	    function has_ssh_key_loaded
	    {
		check_ssh_key || return 1
		ssh-add -L | while read ktype kval kname; do
		    read gtype gval gname < ${ssh_key_dir}/id_dsa.pub
		    [ "${ktype}" = "${gtype}" -a "${kval}" = "${gval}" ] && return 0
		done
	    }

	    if ! check_ssh_key; then
		echo "Warning: Eiger SSH public and/or private keys not available"
		if ! [ -r ${ssh_key_dir}/id_dsa ]; then
		    echo "Copy the Eiger SSH private key with the command (as blissadm):"
		    echo "  scp lid01eiger1:${ssh_key_dir}/id_dsa ${ssh_key_dir}"
		fi
	    elif ! has_ssh_key_loaded; then
		if tmp_dir=$(mktemp -d /tmp/ssh-eiger-XXXXXXXX); then
		    if (umask 0077 && cp ${ssh_key_dir}/id_dsa ${tmp_dir}); then
			ssh-add ${tmp_dir}/id_dsa
		    else
			echo "Warning: could not copy Eiger SSH private key file"
		    fi
		    rm -rf ${tmp_dir}
		else
		    echo "Warning: could not create temporary Eiger SSH key directory"
		fi
		unset tmp_dir
	    fi

	    unset ssh_key_dir

	    function ssh
	    {
		SSH_OPTS="-o KexAlgorithms=+diffie-hellman-group1-sha1 -o PubkeyAcceptedKeyTypes=+ssh-dss"
		command ssh $SSH_OPTS "$@"
	    }

	    function scp
	    {
		SSH_OPTS="-o KexAlgorithms=+diffie-hellman-group1-sha1 -o PubkeyAcceptedKeyTypes=+ssh-dss"
		command scp $SSH_OPTS "$@"
	    }
	fi
    elif [ "${SLS_DETECTOR_TYPE}" = "jungfrau" ]; then
	# Jungfrau-specific definitions
	true
    else
	echo "Warning: ${SLS_DETECTOR_TYPE} detector not supported"
    fi

    if [ -z "${SLS_DETECTOR_LOG}" ]; then
	sls_detector_log=~blissadm/admin/log/${SLS_DETECTOR_TYPE}
	if [ -d ${sls_detector_log} ]; then
	    SLS_DETECTOR_LOG="${sls_detector_log}"
	    export SLS_DETECTOR_LOG
	else
	    echo "Warning: directory ${sls_detector_log} does not exist. Execute:"
	    echo "  sudo mkdir -p /var/log/bcu && \\"
	    echo "  sudo chown blissadm:bliss /var/log/bcu"
	    echo "and as blissadm:"
	    echo "  mkdir -p /var/log/bcu/${SLS_DETECTOR_TYPE} && \\"
	    echo "  ln -s /var/log/bcu/${SLS_DETECTOR_TYPE} ${sls_detector_log}"
	fi
	unset sls_detector_log
    fi

    base_dir=${SLS_DETECTORS_DIR}/config/${SLS_DETECTOR_TYPE}
    config_dir=${base_dir}/detector/${SLS_DETECTOR_NAME}
    SLS_DETECTOR_CONFIG_DIR=${config_dir}
    export SLS_DETECTOR_CONFIG_DIR

    if [ -z "${SLS_DETECTOR_CONFIG_NAME}" ]; then
	SLS_DETECTOR_CONFIG_NAME="slsdetector.config"
    fi

    if [ -z "${SLS_DETECTOR_SETUP}" ]; then
	SLS_DETECTOR_SETUP="${SLS_DETECTOR_BACKEND_SETUP}"
	export SLS_DETECTOR_SETUP
    fi

    setup_dir=${config_dir}/setup/${SLS_DETECTOR_SETUP}
    if [ -n "${SLS_DETECTOR_CONFIG_NAME}" ] && \
	   [ -f ${setup_dir}/sdk/${SLS_DETECTOR_CONFIG_NAME} ]; then
	SLS_DETECTOR_CONFIG=${setup_dir}/sdk/${SLS_DETECTOR_CONFIG_NAME}
	SLS_DETECTOR_MODULES=$(grep "^hostname" ${SLS_DETECTOR_CONFIG} | \
				   cut -d" " -f2 | tr '+' ' ')
	export SLS_DETECTOR_CONFIG SLS_DETECTOR_MODULES
    fi
    unset base_dir config_dir setup_dir

    if [ "${SLS_DETECTOR_TYPE}" = "eiger" ]; then
	EIGER_SETTINGS=$(grep ^settings ${SLS_DETECTOR_CONFIG} | \
			     awk '{print $2}')/standard
	export EIGER_SETTINGS

	if [ -n "${SLS_DETECTOR_MODULES}" ]; then
	    # Check that the local host keys are OK so batch mode will work
            local_keys=$(for m in ${SLS_DETECTOR_MODULES}; do
			     ssh-keygen -f ~/.ssh/known_hosts -F ${m} -l
			 done)
	    if [ -z "${local_keys}" ]; then
		echo "Warning: local SSH keys not defined!!"
		echo "  Batch mode will not work!"
		echo "  Source this script in a terminal, run the command:"
		echo "    for m in \${SLS_DETECTOR_MODULES}; do" \
		     " ssh -x root@\${m} echo OK; done"
		echo "  and accept the keys!"
	    fi
	    unset local_keys
	fi
    fi
fi
