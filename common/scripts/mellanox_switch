#!/usr/bin/env python

import sys
import os
import re
import weakref
import functools
import argparse
import json
import gevent
import gevent.lock
import gevent.fileobject

from contextlib import contextmanager, AbstractContextManager, ExitStack
from telnetlib import Telnet
from collections import namedtuple

def to_string(x):
    return x.decode() if isinstance(x, bytes) else str(x)

log_file_name = None
log_file = None
log_stream = sys.stderr

def log(*args):
    global log_file
    msg = ' '.join([to_string(x) for x in args]) + '\n'
    if log_file_name:
        if not log_file:
            log_exists = os.path.exists(log_file_name)
            log_file = open(log_file_name, 'at')
            if log_exists:
                 log_file.write('\n')
        log_file.write(msg)
        log_file.flush()
    log_stream.write(msg)
    log_stream.flush()


class ReentrantContextManager(AbstractContextManager):

    def __init__(self, obj):
        super().__init__()
        self.obj = obj
        self.count = 0

    def __enter__(self):
        super().__enter__()
        if self.count == 0:
            self.obj_exit = getattr(self.obj, '__exit__')
            self.ret_obj = self.obj.__enter__()
        self.count += 1
        return self.ret_obj

    def __exit__(self, *args, **kws):
        self.count -= 1
        if self.count == 0:
            self.obj_exit(*args, **kws)
        super().__exit__(*args, **kws)



def shared_resource(reentrant_context_manager=False):
    def decorator(orig_klass):
        if not hasattr(orig_klass, 'get_resource_hash'):
            raise RuntimeError(f'Class {orig_klass.__name__} must implement '
                               f'get_resource_hash')
        class Wrapper:
            Instances = {}
            def __new__(new_klass, *args, **kws):
                key = orig_klass.get_resource_hash(*args, **kws)
                if key not in new_klass.Instances:
                    log(f'Creating {orig_klass.__name__} object ...')
                    obj = orig_klass(*args, **kws)
                    if reentrant_context_manager:
                        obj = ReentrantContextManager(obj)
                    deleter = functools.partial(new_klass.Instances.pop, key)
                    def deleter(x, d=deleter):
                        log(f'Destroyed {orig_klass.__name__} object ...')
                        d()
                    new_klass.Instances[key] = weakref.ref(obj, deleter)
                else:
                    obj = new_klass.Instances[key]()
                return obj
        return Wrapper
    return decorator


class VT100:
    # https://www.csie.ntu.edu.tw/~r92094/c++/VT100.html

    EscSeq = lambda x: ('\x1b' + x).encode()

    Escape = {
        'SetAppl':   EscSeq(r'\[\?1h'),
        'AltKeyPad': EscSeq(r'='),
        'ModesOff':  EscSeq(r'\[m'),
        'Reverse':   EscSeq(r'\[7m'),
        'ClearREol': EscSeq(r'\[K'),
    }


def Optional(x):
    return b'(' + x + b')?'


TelnetPort = 23

@shared_resource(reentrant_context_manager=True)
class MellanoxSwitch:

    Login = b'admin'
    Password = b'admin'

    LoginPrompt = b'login:[ \\t]*'
    PromptFormat = b'%s > '
    LinesBegin = VT100.Escape['SetAppl'] + VT100.Escape['AltKeyPad']
    LinesPrompt = (Optional(VT100.Escape['Reverse']) +
                   b'lines ([0-9]+)-([0-9]+)' +
                   Optional(b'/([0-9]+) \(END\)') + b' ' +
                   Optional(VT100.Escape['ModesOff']))

    ClearLine = b'\r'.join([Optional(VT100.Escape['ClearREol'])] * 2)

    @classmethod
    def get_resource_hash(klass, config):
        return (config.get('SwitchName'),
                config.get('SwitchPort', TelnetPort))

    def __init__(self, config):
        self.config = config
        self.sw_name = config.get('SwitchName')
        self.sw_port = config.get('SwitchPort', TelnetPort)
        prompt = config.get('SwitchPromptPrefix', '').encode()
        self.prompt = self.PromptFormat % prompt
        self.ser2net = config.get('Ser2Net', (self.sw_name == 'localhost' or
                                              self.sw_port != TelnetPort))

    def __enter__(self):
        sw_name = self.sw_name
        sw_port = self.sw_port
        special_port = sw_port != TelnetPort
        port_str = ' [port %d]' % sw_port if special_port else ''
        log('Connecting to Mellanox Switch %s%s ...' % (sw_name, port_str))
        con = Telnet(sw_name, sw_port)
        con.write(b'\n')
        i, m, l = con.expect([self.LoginPrompt, self.prompt, self.LinesPrompt])
        if i == 0:
            con.write(b'%s\n' % self.Login)
            con.expect([b'Password:[ \\t]*'])
            con.write(b'%s\n' % self.Password)
            write = b''
        elif i == 1:
            write = b'\n'
        else:
            write = b'q\n'
        self.con = con
        self.wait_prompt(write)
        log('Connection established!')
        self.lock = gevent.lock.RLock()
        def heartbeat():
            while True:
                gevent.sleep(30)
                with self.lock:
                    log('Heartbeat!')
                    self.wait_prompt()
        self.heartbeat = gevent.spawn(heartbeat)
        return self

    def __exit__(self, *args, **kws):
        self.heartbeat.kill()
        with self.lock:
            con = self.wait_prompt()
            log('Quitting ...')
            con.write(b'quit\n')
            if self.ser2net:
                con.expect([self.LoginPrompt])
                con.close()
            else:
                con.read_all()
            log('Done!')

    def wait_prompt(self, write=b'\n'):
        if write:
            self.con.write(write)
        self.con.expect([self.prompt])
        return self.con

    def read_cmd_paged_output(self, cmd):
        with self.lock:
            con = self.wait_prompt()
            con.write(cmd)
            strip = lambda x: x.strip(b'\r\n').decode()
            l = con.read_until(b'\r\n')
            l, cmd = [strip(x) for x in (l, cmd)]
            if l != cmd:
                log(f'Warning: expected cmd echo "{cmd}", got "{l}"')
            lines = []
            while True:
                i, m, l = con.expect([self.prompt, self.LinesBegin,
                                      self.LinesPrompt, b'\r\n'])
                if i == 0:
                    break
                elif i == 1:
                    continue
                elif i == 2:
                    end = b'END' in l
                    con.write(b' ' if not end else b'\n')
                    con.expect([self.ClearLine])
                else:
                    lines.append(strip(l))
            for l in [l for l in lines if '\x1b' in l]:
                r = ''.join(['^[' if x == '\x1b' else x for x in l])
                log('Warning: Escape: %s' % r)

            return lines


class MellanoxSwitchPort(ExitStack):

    NbRetries = 3

    def __init__(self, config, port):
        super().__init__()
        self.config = config
        self.port = port

    def __enter__(self):
        cm = super().__enter__()
        self.sw = cm.enter_context(MellanoxSwitch(self.config))
        return self

    def read_information(self, nb_retries=NbRetries):
        log('Reading port %s information ...' % self.port)
        cmd = b'show interfaces ethernet %s\n' % self.port.encode()
        info = {}
        StageInfo = namedtuple('StageInfo', ['header', 'stage'])
        follower = dict(init=StageInfo(f'Eth{self.port}', 'status'),
                        status=StageInfo('Rx', 'rx'),
                        rx=StageInfo('Tx', 'tx'),
                        tx=StageInfo(None, None))
        rx_tx_re = re.compile('[ ]+([0-9]+)[ ]+(.+)')
        re_dict = dict(init=None,
                       status=re.compile('[ ]+([^:]+): (.+)'),
                       rx=rx_tx_re,
                       tx=rx_tx_re)

        for retry in range(nb_retries):
            try:
                stage = 'init'
                for l in self.sw.read_cmd_paged_output(cmd):
                    if not l.strip():
                        continue
                    if l == follower[stage].header:
                        stage = follower[stage].stage
                        continue
                    re_obj = re_dict[stage]
                    m = re_obj.match(l) if re_obj else None
                    if not m:
                        msg = f'Warning: Unexpected line in {stage}: {l}'
                        raise ValueError(msg)
                    d = info.setdefault(stage, {})
                    if stage == 'status':
                        d[m[1]] = m[2]
                    else:
                        d[m[2]] = int(m[1])
            except Exception as e:
                log(f'Exception: {e}')
                log('Retrying ...')
                continue
            else:
                break
        else:
            raise ValueError(f'Error parsing {cmd} output after {nb_retries}'
                             f'retries')
        return info


def print_port_info(config, ports, json_output=False, port_output_separator='',
                    output_file=sys.stdout, flush_output=False, **args):
    lines = []
    for name in ports:
        with MellanoxSwitchPort(config, name) as port:
            port_info = dict(name=name, info=port.read_information())
        if json_output:
            lines.append(json.dumps(port_info))
        else:
            lines.append("** %s" % name)
            for n, d in port_info['info'].items():
                lines.append("Group: %s" % n)
                for k, v in d.items():
                    lines.append(" %s: %s" % (k, v))
        if port_output_separator:
            lines.append(port_output_separator)
    print(*lines, sep='\n', file=output_file, flush=flush_output)


# Ser2Net example:
#  config = {
#      'SwitchName': 'localhost',
#      'SwitchPort': 28100,
#  }
#
# Telnet example:
#  config = {
#      'SwitchName': 'mswmgmt',
#  }


def main():

    parser = argparse.ArgumentParser(description='Mellanox switch tool')
    parser.add_argument('sw_host', help='Telnet host name')
    parser.add_argument('--sw_port', type=int, default=TelnetPort,
                        help='Telnet host port')
    parser.add_argument('--ports', help='Comma-separated port list')
    parser.add_argument('--read_ports_from_stdin', action='store_true',
                        help='Read ports from stdin')
    parser.add_argument('--json_output', action='store_true',
                        help='Write output in JSON format')
    parser.add_argument('--port_output_separator',
                        help='Separator to insert between port outputs')
    parser.add_argument('--output_file',
                        help='Separator to insert between port outputs')
    parser.add_argument('cmd', help='Command to execute: print_port_info')

    args = parser.parse_args()
    config = {'SwitchName': args.sw_host, 'SwitchPort': args.sw_port}

    args_dict = dict(args.__dict__, output_file=sys.stdout)

    with ExitStack() as stack:
        if args.output_file:
            out_file_cm = open(args.output_file, 'wt')
            args_dict['output_file'] = stack.enter_context(out_file_cm)

        sw = stack.enter_context(MellanoxSwitch(config))

        if args.cmd == 'print_port_info':
            args_dict.pop('ports')
            if args.ports:
                ports = args.ports.split(',')
                print_port_info(config, ports, **args_dict)
            elif args.read_ports_from_stdin:
                args_dict['flush_output'] = True
                for l in gevent.fileobject.FileObject(sys.stdin):
                    port = l.strip()
                    print_port_info(config, [port], **args_dict)
            else:
                raise ValueError(f'Please specify a port option')
        else:
            raise ValueError(f'Invalid command: {args.cmd}')


if __name__ == '__main__':
    main()
