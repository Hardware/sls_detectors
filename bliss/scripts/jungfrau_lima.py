import sys
import os
import numpy as np
import gevent
import gevent.event as event
import h5py as h5
import time
from contextlib import ExitStack

import bliss.common.plot as flintplot
from bliss.common.image_tools import array_to_file
from bliss.scanning.scan import ScanAbort
from bliss.controllers.lima.limatools import limatake
from blissdata.lima.image_utils import decode_devencoded_image

class Monitor:

    Instances = {}

    @classmethod
    def get_data_name(klass, lima_dev, attr_name):
        return f'{lima_dev.name}:{attr_name}'

    @classmethod
    def get_monitor(klass, lima_dev, attr_name, create=False, **kws):
        data_name = f'{lima_dev.name}:{attr_name}'
        if data_name in klass.Instances:
            return klass.Instances[data_name]
        elif not create:
            return None

        monitor = klass(lima_dev, attr_name, **kws)
        klass.Instances[data_name] = monitor
        return monitor

    @classmethod
    def get_data(klass, lima_dev, attr_name):
        if attr_name in ['last_image']:
            data_array = getattr(lima_dev.proxy, attr_name)
            return decode_devencoded_image(data_array)
        d = getattr(lima_dev.camera, attr_name)
        rx, ry, rw, rh = lima_dev.image.roi
        mw, mh = lima_dev.proxy.image_max_dim
        if ((rx != 0) or (ry != 0) or
            ((rw != 0) and (rw != mw)) or ((rh != 0) and (rh != mh))):
            d = d[ry:ry+rh,rx:rx+rw]
        return d

    @classmethod
    def get_histogram(klass, d, hist_range):
        dmin, dmax = d.min(), d.max()
        if hist_range is not None:
            dmin = max(dmin, hist_range[0])
            dmax = min(dmax, hist_range[1])
        if dmin == dmax:
            return [np.array(x) for x in ([0, 1], [0, 0])]
        r = int(dmax - dmin)
        y, b = np.histogram(d, r + 1, (dmin, dmax))
        x = np.array(b[:-1])
        return x, y

    def __init__(self, lima_dev, attr_name, active=True, refresh_time=1,
                 hist=False, hist_range=None, verbose=False, window_name=None,
                 set_colormap_args=None, default_dtype='uint16', mode='normal'):
        self.data_name = f'{lima_dev.name}:{attr_name}'
        self.lima_dev = lima_dev
        self.attr_name = attr_name

        try:
            d = self.get_data(lima_dev, attr_name)
        except Exception as e:
            d = np.zeros((1024, 1024), default_dtype)

        if window_name is None:
            window_name = self.data_name
        self.plot = flintplot.plot_image(d, name=window_name)
        self.plot.side_histogram_displayed = False
        self.plot.yaxis_direction = "down"
        if set_colormap_args:
            self.plot.set_colormap(**set_colormap_args)

        if hist:
            x, y = self.get_histogram(d, hist_range)
            self.hist = flintplot.plot(y, x=x, name=self.data_name + '_hist')
        else:
            self.hist = None
        self.hist_range = hist_range

        self.last_timestamp = None
        self.refresh_time = refresh_time
        self.verbose = verbose
        self.mode = mode
        self._end = None
        self._task = None

        self.active = active
        active_str = 'active' if active else 'not active'
        print(f'Monitoring {self.data_name} [{active_str}]')

    def __del__(self):
        self.active = False

    def __enter__(self):
        return self

    def __exit__(self, *args, **kws):
        self.active = False

    @property
    def active(self):
        return self._task is not None

    @active.setter
    def active(self, active):
        if active == self.active:
            return

        action = "(Re)starting" if active else "Stopping"
        print(f"{action} {self.data_name} monitor ...")
        if self.active:
            self._end.set()
            self._task.join()
            self._task = None
            self._end = None
        else:
            self._end = event.Event()
            self._task = gevent.spawn(self.monitor_task)

    def monitor_task(self):
        last_d = None
        acc_d = None
        n = 0
        while not self._end.ready():
            try:
                d = self.get_data(self.lima_dev, self.attr_name)
                if last_d is None or (d != last_d).any():
                    last_d = d
                    if acc_d is None:
                        acc_d = d
                    else:
                        acc_d += d
                    n += 1
                    if self.mode == 'accumulate':
                        display_d = acc_d
                    elif self.mode == 'average':
                        display_d = acc_d / n
                    else:
                        display_d = d
                    self.plot.set_data(display_d)
                    if self.hist:
                        x, y = self.get_histogram(display_d, self.hist_range)
                        self.hist.set_data(y, x=x)
                    if self.verbose:
                        print(f"Read {self.data_name}!")
                    self.last_timestamp = time.time()
            except Exception as e:
                if self.verbose:
                    print(f"Exception: {e}")
            finally:
                gevent.sleep(self.refresh_time)


def start_monitor(lima_dev, attr_name, **kws):
    return Monitor.get_monitor(lima_dev, attr_name, create=True, **kws)

def set_monitor_hist_range(lima_dev, attr_name, xmin, xmax):
    monitor = Monitor.get_monitor(lima_dev, attr_name)
    if monitor:
        monitor.hist_range = (xmin, xmax)

def reset_monitor_hist_range(lima_dev, attr_name):
    monitor = Monitor.get_monitor(lima_dev, attr_name)
    if monitor:
        monitor.hist_range = None

def set_monitor_active(lima_dev, attr_name, active):
    monitor = Monitor.get_monitor(lima_dev, attr_name)
    if monitor:
        monitor.active = active

def get_monitor_active(lima_dev, attr_name):
    monitor = Monitor.get_monitor(lima_dev, attr_name)
    return monitor and monitor.active

def jungfrau_init_img_proc(lima_dev):
    lima_dev.camera.img_proc_config = 'gain_ped,gain_adc_map'
    start_monitor(lima_dev, 'gain_map', True)
    start_monitor(lima_dev, 'adc_map', True, hist=True)
    for gain in range(3):
        start_monitor(lima_dev, f'ped_{gain}_calib_map', False)
    lima_dev.camera.gain_ped_map_type = 'MAP32'
    start_monitor(lima_dev, 'gain_ped_proc_map32', True, hist=True,
                  hist_range=(-100, 10e3))

def jungfrau_mask_bad_gain_pixels(lima_dev, exp_time=1e-3, level=None):
    if lima_dev.camera.gain_ped_map_type != 'MAP32':
        raise ValueError('gain_ped_map_type must be MAP32')
    lima_dev.processing.use_mask = False
    limatake(exp_time, 1)
    d = lima_dev.camera.gain_ped_proc_map32
    if level is None:
        level = 2**32 - 1 - 0x10
    if (np.sum((d >= level)) == 0):
        print('Nothing to do')
        return

    lima_dev.processing.use_mask = False
    limatake(exp_time, 1)
    d = lima_dev.camera.gain_ped_proc_map32
    print(f'Found {np.sum((d >= level))} hot pixels')
    mask = np.array((d < level), dtype='uint16')
    mask_file = '/tmp_14_days/ahoms/jungfrau/mask.edf'
    array_to_file(mask_file, 'I;16', mask)
    lima_dev.processing.use_mask = True
    lima_dev.processing.mask = mask_file


def seq_take(e, n, f, *limadevs, save=False, one_file=True, next_file_number=0,
             **args):
    l = 1.0 / f - e
    if one_file:
        args.update(saving_frame_per_file=n)
    if next_file_number is not None:
        args.update(saving_next_number=next_file_number)
    return limatake(e, n, *limadevs, latency_time=l, save=save, **args)


def tango_seq_take(e, n, f, *limadevs, save=False,
                   one_file=True, next_file_number=0,
                   acq_trigger_mode='INTERNAL_TRIGGER', **args):
    latency_time = 1.0 / f - e
    args.update(acq_expo_time=e, acq_nb_frames=n, latency_time=latency_time,
                acq_trigger_mode=acq_trigger_mode)
    if one_file:
        args.update(saving_frame_per_file=n)
    if next_file_number is not None:
        args.update(saving_next_number=next_file_number)
    saving_mode = 'AUTO_FRAME' if save else 'MANUAL'
    args.update(saving_mode=saving_mode)

    names, proxies = zip(*[(d.name, d.proxy) for d in limadevs])

    names_str = ','.join([n for n in names])
    title = f'Tango Frame Sequence for {names_str} ({n} frames)'

    print(f'Preparing {title} ...')
    for d in proxies:
        for n, v in args.items():
            setattr(d, n, v)
        d.prepareAcq()

    print(f'Starting {title} ...')
    with ExitStack() as stack:
        def running_cleanup(d):
            if d.acq_status == 'Running':
                d.stopAcq()

        for d in proxies:
            d.startAcq()
            stack.callback(running_cleanup, d)

        def is_running(proxies):
            return [(d.acq_status == 'Running') for d in proxies]

        def last_image(d):
            return d.last_image_saved if save else d.last_image_ready

        while any(is_running(proxies)):
            status_str = ', '.join([f'{n}: {last_image(d)}'
                                    for n, d in zip(names, proxies)])
            sys.stdout.write(f"{status_str} ...\r")
            sys.stdout.flush()
            gevent.sleep(10e-3)

        print(f'Finished {title}!')


def jungfrau_set_highz(lima_dev):
    mod = 0
    cmds = ['setbit 0x5d 14',
            'dac vref_prech 1000',
            'dac vref_ds 1100',
            'adcinvert 0xf0f']
    config_dev = lima_dev._get_proxy('SlsDetectorJungfrau')
    for c in cmds:
        config_dev.putCmd(f'{mod}:{c}')

def jungfrau_set_gain(lima_dev, gain):
    all_gains = ['DYNAMIC', 'FORCE_SWITCH_G1', 'FORCE_SWITCH_G2',
                 'FIXG1', 'FIXG2', 'FIXG0']
    if gain in ['', '?']:
        print(f"Available gains: {all_gains}")
        return

    print(f'Setting gain {gain}')
    lima_dev.camera.gain_mode = gain

def jungfrau_set_adc_speed(lima_dev, speed):
    all_speeds = ['full_speed', 'half_speed']
    if speed in ['', '?']:
        print(f"Available speeds: {all_speeds}")
        return

    print(f'Setting speed {speed}')
    config_dev = lima_dev._get_proxy('SlsDetectorJungfrau')
    config_dev.putCmd(f'speed {speed}')

def jungfrau_get_gain_corr_active(lima_dev):
    return lima_dev.camera.img_src == 'GAIN_PED_CORR'

def jungfrau_set_gain_corr_active(lima_dev, active):
    img_src = 'GAIN_PED_CORR' if active else 'RAW'
    print(f'Setting GainPedCorr active={active}: img_src={img_src}')
    lima_dev.camera.img_src = img_src

def jungfrau_set_sc_mode_active(lima_dev, active, nb_sc=16, start_sc=0):
    if active:
        if nb_sc < 1:
            raise RuntimeError(f'Invalid nb_sc={nb_sc}')
        elif nb_sc == 1 and start_sc == 15:
            raise RuntimeError(f'Setting SC mode with standard mode params')
    lima_dev.camera.storage_cell_mode_active = active
    if active:
        print(f'Setting SC mode: nb_sc={nb_sc}, start_sc={start_sc}')
        lima_dev.camera.nb_additional_storage_cells = nb_sc - 1
        lima_dev.camera.storage_cell_start = start_sc

def jungfrau_get_sc_mode_active(lima_dev):
    return (lima_dev.camera.storage_cell_mode_active,
            lima_dev.camera.nb_additional_storage_cells + 1,
            lima_dev.camera.storage_cell_start)

def jungfrau_prepare_pedestal_context(stack, lima_dev, stop_mask=True,
                                      reset_image_size=True,
                                      restore_dyn_gain=True,
                                      restore_skip_frame=True,
                                      restore_sc=False):

    if stop_mask:
        mask_dev = lima_dev._get_proxy('Mask')
        mask_state = mask_dev.State()
        state_type = type(mask_state)
        if mask_state == state_type.ON:
            mask_dev.Stop()
            stack.callback(mask_dev.Start)

    if reset_image_size:
        def image_restorer(bin=lima_dev.image.binning,
                           roi=lima_dev.image.roi):
            lima_dev.image.binning = bin
            lima_dev.image.roi = roi
        stack.callback(image_restorer)
        lima_dev.image.reset_roi()
        lima_dev.image.binning = (1, 1)

    if restore_dyn_gain:
        stack.callback(jungfrau_set_gain, lima_dev, 'DYNAMIC')

    if restore_skip_frame:
        def skip_frame_restorer(freq, idx):
            print(f'Restoring skip frame freq={freq}, idx={idx}')
            lima_dev.camera.skip_frame_freq = freq
            lima_dev.camera.skip_frame_idx = idx
        stack.callback(skip_frame_restorer, lima_dev.camera.skip_frame_freq,
                       lima_dev.camera.skip_frame_idx)

    def img_proc_config_restorer(config=lima_dev.camera.img_proc_config):
        lima_dev.camera.img_proc_config = config
    lima_dev.camera.img_proc_config = 'ave'
    stack.callback(img_proc_config_restorer)

    if jungfrau_get_gain_corr_active(lima_dev):
        jungfrau_set_gain_corr_active(lima_dev, False)
        stack.callback(jungfrau_set_gain_corr_active, lima_dev, True)

    attr_names = ['ave_map'] + [f'ped_{g}_calib_map' for g in range(3)]
    attr_names += ['last_image']
    monitor_attrs = [a for a in attr_names if get_monitor_active(lima_dev, a)]
    def monitor_attrs_restorer():
        for attr in monitor_attrs:
            set_monitor_active(lima_dev, attr, False)
    stack.callback(monitor_attrs_restorer)
    for attr in monitor_attrs:
        set_monitor_active(lima_dev, attr, True)

    if restore_sc:
        def sc_restorer(active, nb_sc, start_sc):
            jungfrau_set_sc_mode_active(lima_dev, active, nb_sc, start_sc)
        active, nb_sc, start_sc = jungfrau_get_sc_mode_active(lima_dev)
        stack.callback(sc_restorer, active, nb_sc, start_sc)


def jungfrau_get_gain_calib_map_from_ave(lima_dev, n, sc=15):
    lima_dev.camera.ave_curr_storage_cell = sc
    if lima_dev.camera.ave_nb_frames != n:
        print('Warning: expected ave_nb_frames=%s. got %s' %
              (n, lima_dev.camera.ave_nb_frames))
    return lima_dev.camera.ave_map


def jungfrau_set_gain_calib_map(lima_dev, g, c, sc=15):
    lima_dev.camera.gain_ped_calib_curr_storage_cell = sc
    map_name = f'ped_{g}_calib_map'
    setattr(lima_dev.camera, map_name, c)


def jungfrau_pedestal_scans(lima_dev, e, n, f, one_file=False, save=True,
                            gains=range(3),
                            restore_dyn_gain=True, update_calib=True,
                            pedestal_base_path=None, pedestal_filename=None,
                            pedestal_file_idx=0, save_pedestal=False,
                            stop_mask=True, use_tango=False,
                            gain_names=['DYNAMIC',
                                        'FORCE_SWITCH_G1',
                                        'FORCE_SWITCH_G2']):
    if jungfrau_get_sc_mode_active(lima_dev)[0]:
        good_funct = "jungfrau_pedestal_scans_sc"
        raise RuntimeError(f"Jungfrau in SC mode! Use {good_funct} instead")

    with ExitStack() as stack:
        restore_skip_frame = False
        if lima_dev.camera.skip_frame_freq:
            restore_skip_frame = True
            print('Disabling skip frame')
            lima_dev.camera.skip_frame_freq = 0

        jungfrau_prepare_pedestal_context(stack, lima_dev, stop_mask=stop_mask,
                                          restore_dyn_gain=restore_dyn_gain,
                                          restore_skip_frame=restore_skip_frame)
        gevent.sleep(1)

        take_fn = tango_seq_take if use_tango else seq_take
        take_args = dict(save=save, one_file=one_file,
                         next_file_number=pedestal_file_idx)

        valid_path = (pedestal_base_path and pedestal_filename)
        if valid_path:
            fname, fext = os.path.splitext(pedestal_filename)
        elif save_pedestal:
            raise ValueError("Must specify both pedestal_base_path and "
                             "pedestal_filename")
        change_saving = save and valid_path
        if change_saving:
            take_args.update(saving_directory=pedestal_base_path,
                             saving_format='HDF5BS')

        h5d = None
        if save_pedestal:
            dshape = [3] + list(lima_dev.image.fullsize[::-1])
            h5fname = os.path.join(pedestal_base_path, pedestal_filename)
            print(f'Saving pedestals into {h5fname}')
            h5f = stack.enter_context(h5.File(h5fname, 'w'))
            h5d = h5f.create_dataset('/data', dshape, dtype='f8')

        def process_calib(g, n):
            if not (h5d or update_calib):
                return
            c = jungfrau_get_gain_calib_map_from_ave(lima_dev, n)
            if h5d:
                h5d[g] = c
            if update_calib:
                jungfrau_set_gain_calib_map(lima_dev, g, c)
            
        for g in gains:
            jungfrau_set_gain(lima_dev, gain_names[g])
            if change_saving:
                take_args.update(saving_prefix=f'{fname}_gain{g}_')
            take_fn(e, n, f, lima_dev, **take_args)
            process_calib(g, n)

        gevent.sleep(1)



def jungfrau_pedestal_scans_exposures(lima_dev, min_exp, max_exp, nb_exp, n, f,
                                      gains=range(3)):
    min_log, max_log = [log10(x) for x in (min_exp, max_exp)]
    expo_times = [pow(10, min_log + (max_log - min_log) / nb_exp * i)
                  for i in range(nb_exp + 1)]
    for e in expo_times:
        print(f'Taking pedestals for exposure: {e}')
        for r in range(3):
            try:
                jungfrau_pedestal_scans(lima_dev, e, n, f, gains=gains)
                break
            except:
                continue
        else:
            raise RuntimeError("Too many failures")


def jungfrau_pedestal_scans_sc(lima_dev, e, n0=200, n12=100, f=1/160e-3,
                               one_file=False, save=True,
                               gains=range(3),
                               restore_dyn_gain=True, update_calib=True,
                               pedestal_base_path=None, pedestal_filename=None,
                               pedestal_file_idx=0, save_pedestal=False,
                               stop_mask=True, use_tango=False,
                               gain_names=['DYNAMIC',
                                           'FORCE_SWITCH_G1',
                                           'FORCE_SWITCH_G2'],
                               use_second_sc=False):
    active, nb_sc, start_sc = jungfrau_get_sc_mode_active(lima_dev)
    if not active:
        good_funct = "jungfrau_pedestal_scans"
        raise RuntimeError(f"Jungfrau not in SC mode! Use {good_funct} instead")

    with ExitStack() as stack:
        jungfrau_prepare_pedestal_context(stack, lima_dev, stop_mask=stop_mask,
                                          restore_dyn_gain=restore_dyn_gain,
                                          restore_skip_frame=True,
                                          restore_sc=True)

        h5d = None
        if save_pedestal:
            if not (pedestal_base_path and pedestal_filename):
                raise ValueError("Must specify both pedestal_base_path and "
                                 "pedestal_filename")
            dshape = [16, 3] + list(lima_dev.image.fullsize[::-1])
            h5fname = os.path.join(pedestal_base_path, pedestal_filename)
            print(f'Saving pedestals into {h5fname}')
            h5f = stack.enter_context(h5.File(h5fname, 'w'))
            h5d = h5f.create_dataset('/data', dshape, dtype='f8')

        def process_calib(g, n, sc):
            if not (h5d or update_calib):
                return
            c = jungfrau_get_gain_calib_map_from_ave(lima_dev, n, sc)
            if h5d:
                h5d[sc, g] = c
            if update_calib:
                jungfrau_set_gain_calib_map(lima_dev, g, c, sc)
            
        gevent.sleep(1)
        for g in gains:
            jungfrau_set_gain(lima_dev, gain_names[g])
            if g == 0:
                n = n0
                print('Disabling skip frame')
                lima_dev.camera.skip_frame_freq = 0

                nb_sc, start_sc = 16, 0
                print(f'Measuring G{g}: {nb_sc} SC pedestals')
                jungfrau_set_sc_mode_active(lima_dev, True, nb_sc, start_sc)
                seq_take(e, n * nb_sc, f, lima_dev, one_file=one_file, save=save)
                for sc in range(nb_sc):
                    process_calib(g, n, sc)
            else:
                n = n12
                print('Skipping one SC frame')
                nb_sc = 2
                lima_dev.camera.skip_frame_freq = nb_sc - 1
                for sc in range(16):
                    skip_frame_idx = 0 if use_second_sc else 1
                    start_sc =  (sc - 1) % 16 if use_second_sc else sc
                    print(f'Measuring G{g}: SC #{sc} pedestal '
                          f'[skip_frame_idx={skip_frame_idx}]')
                    lima_dev.camera.skip_frame_idx = skip_frame_idx
                    jungfrau_set_sc_mode_active(lima_dev, True, nb_sc, start_sc)
                    seq_take(e, n, f, lima_dev, one_file=one_file, save=save)
                    process_calib(g, n, sc)

        gevent.sleep(1)
