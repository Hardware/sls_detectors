#!/usr/bin/env python

# don't need to monkey-patch socket because telnetlib runs in one greenlet

import os
import sys
import re
import time
import errno
import socket
import functools
import weakref
from getopt import getopt
from telnetlib import Telnet
from configparser import ConfigParser

import gevent
from gevent.queue import Queue
from gevent.subprocess import Popen, PIPE, STDOUT, run as run_proc, TimeoutExpired
from gevent.fileobject import FileObject
from gevent.socket import socket as Socket

run_text_proc = functools.partial(run_proc, text=True)
run_text_proc_capture = functools.partial(run_text_proc, capture_output=True)

log_file_name = None
log_file = None

start_time = time.ctime()

def to_string(x):
    return x.decode() if type(x) == bytes else str(x)

def log(*args):
    global log_file
    msg = ' '.join([to_string(x) for x in args]) + '\n'
    if log_file_name:
        if not log_file:
            log_exists = os.path.exists(log_file_name)
            log_file = open(log_file_name, 'at')
            if log_exists:
                 log_file.write('\n')
        log_file.write(msg)
        log_file.flush()
    sys.stdout.write(msg)
    sys.stdout.flush()

def get_eiger_executables_dir():
    this_script = sys.argv[0]
    return os.path.join(os.path.dirname(this_script), os.pardir, 'executables')

def get_domain_name():
    return socket.getfqdn().strip(socket.gethostname())

get_inet_ip = functools.partial(socket.inet_pton, socket.AF_INET)

def get_route_table():
    route = run_text_proc_capture(['/sbin/route', '-n'])
    ip_re_str = '\\.'.join(['[0-9]{1,3}'] * 4)
    route_ip_group = ['dest', 'gw', 'mask']
    route_str_group = ['flags', 'metric', 'ref', 'use', 'iface']
    route_re_list = [('(?P<%s>{0})' % k) for k in route_ip_group]
    route_re_list += [('(?P<%s>{1})' % k) for k in route_str_group]
    route_re_format = '{2}'.join(route_re_list)
    route_re_str = route_re_format.format(ip_re_str, '[^ \t]+', '[ \t]+')
    route_re = re.compile(route_re_str)
    route_table = []
    for l in route.stdout.split('\n'):
        m = route_re.match(l.strip())
        if m:
            d = dict([(k, m.group(k)) for k in route_str_group])
            d.update([(k, get_inet_ip(m.group(k))) for k in route_ip_group])
            route_table.append(d)
    return route_table

def get_masked_ip(addr, mask):
    masked_ip = [(x & y) for x, y in zip(addr, mask)]
    return bytes(masked_ip)


class Connection(object):

    DefDomain = get_domain_name()

    def __init__(self, name, config):
        self.name = name
        self.config = config
        self.addr = socket.gethostbyname(name)

    def init(self):
        pass

    def restart(self):
        pass

    def check_link(self):
        if self.get_link_active():
            return

        log('[%s] Restarting Ethernet connection ...' % self.name)
        self.restart()
        sleep_time = 10
        log('[%s] Waiting for connection (%s sec) ...' %
            (self.name, sleep_time))
        gevent.sleep(sleep_time)

    def get_link_active(self):
        return True

    def cleanup(self):
        pass


class DirectEthernetConnection(Connection):

    def __init__(self, name, config):
        super(DirectEthernetConnection, self).__init__(name, config)

        ip = get_inet_ip(self.addr)
        default_gw = lambda d: d['dest'] == get_inet_ip('0.0.0.0')
        good_subnet = lambda d: get_masked_ip(ip, d['mask']) == d['dest']
        ifaces = [d['iface'] for d in get_route_table()
                  if not default_gw(d) and good_subnet(d)]
        if not ifaces:
            raise RuntimeError('Cannot find direct link to %s' % name)
        self.iface = ifaces[0]

    def restart(self):
        log('[%s] Disabling %s ...' % (self.name, self.iface))
        os.system('sudo ifconfig %s down' % self.iface)
        gevent.sleep(4)
        log('[%s] Enabling %s ...' % (self.name, self.iface))
        os.system('sudo ifconfig %s up' % self.iface)

    def get_link_active(self):
        ethtool = run_text_proc_capture(['sudo', 'ethtool', self.iface])
        for l in ethtool.stdout.split('\n'):
            l = l.strip()
            field = 'Link detected' + ': '
            if l.startswith(field):
                return l[len(field):] == 'yes'
        return False


def groupable(klass):
    klass.Registered = {}
    klass.GroupData = {}
    klass.add_item = lambda self, n: self.Registered.setdefault(self, n)
    klass.get_nb_items = lambda self: len(self.Registered)
    return klass

def grouped(method):
    def grouped_method(self):
        waiting, queue = self.GroupData.setdefault(method, ([], Queue()))
        n = len(waiting)
        waiting.append(self)
        nb_items = self.get_nb_items()
        if len(waiting) == nb_items:
            res = method(self)
            for i in range(nb_items):
                queue.put(res)
        res = queue.get()
        waiting.remove(self)
        return res
    return grouped_method

@groupable
class ExtremeSwitchConnection(Connection):

    Login = b'admin'
    Password = b''

    PromptFormat = b'%s.[0-9]+ # '
    StatusSep = b'={85}'
    StatusLine = b' +'.join([b'([^ ]+)'] * 12)

    @staticmethod
    def get_ports_from_config_string(ports_str):
        ports = []
        for r in ports_str.split(','):
            s_e = r.split('-')
            start, end = s_e if len(s_e) > 1 else [s_e[0]] * 2
            ports.extend(range(int(start), int(end) + 1))
        return ports

    class SwitchLogin:
        def __init__(self, con):
            self.con = con

        def __enter__(self):
            sw_name = self.con.sw_name
            log('Connecting to Extreme Switch %s ...' % sw_name)
            sw = Telnet(sw_name)
            sw.expect([b'login:[ \\t]*'])
            sw.write(b'%s\n' % self.con.Login)
            sw.expect([b'password:[ \\t]*'])
            sw.write(b'%s\n' % self.con.Password)
            self.sw = sw
            return sw

        def __exit__(self, *args):
            sw = self.sw
            log('Quitting ...')
            sw.expect([self.con.prompt])
            sw.write(b'quit\n')
            sw.expect([b'Do you wish to save your config.+\\(y/N\\) '])
            sw.write(b'\n')
            log('Done!')

    def __init__(self, name, config):
        super(ExtremeSwitchConnection, self).__init__(name, config)
        self.sw_name = config.get('SwitchName')
        prompt = config.get('SwitchPromptPrefix').encode()
        self.prompt = self.PromptFormat % prompt
        self.ports = self.get_ports_from_config_string(config.get('Ports'))
        no_auto_neg = get_config_boolean_option(config, 'DisableAutoNeg')
        self.restart_disable_autoneg = no_auto_neg
        self.add_item(self.name)

    def login(self):
        return self.SwitchLogin(self)

    def get_ports_string(self):
        return ','.join([str(p) for p in self.ports])

    def get_extreme_ports_status(self):
        with self.login() as sw:
            nb_items, nb_ports = self.get_nb_items(), len(self.ports)
            if nb_items != nb_ports:
                raise RuntimeError('Items (%d) / Ports (%d) mismatch' % 
                                   (nb_items, nb_ports))
            ports = ','.join(map(str, self.ports))
            log('Getting status of ports %s ...' % ports)
            sw.expect([self.prompt])
            sw.write(b'show ports %s information\n' % ports.encode())
            sw.expect([self.StatusSep])
            status = {}
            nb_ports = 0
            while True:
                i, m, l = sw.expect([self.StatusLine, self.StatusSep])
                if i == 1:
                    break
                active = (m.group(3) == b'active')
                if status.setdefault('active', active) != active:
                    raise RuntimeError('Link status not the same for all ports')
                nb_ports += 1
            if nb_ports != nb_items:
                raise RuntimeError('Items (%d) / Received ports (%d) mismatch' %
                                   (nb_items, nb_ports))
            return status['active']

    @grouped
    def get_link_active(self):
        return self.get_extreme_ports_status()

    def restart_extreme_ports(self):
        with self.login() as sw:
            ports_str = self.get_ports_string()
            log('Restarting ports %s ...' % ports_str)
            sw.expect([self.prompt])
            sw.write(b'disable ports %s\n' % ports_str.encode())
            sw.expect([self.prompt])
            sw.write(b'enable ports %s\n' % ports_str.encode())

    def set_auto_neg(self, active):
        with self.login() as sw:
            ports_str = self.get_ports_string()
            action = 'Enabling' if active else 'Disabling'
            log('%s Auto-Negotiation on ports %s ...' % (action, ports_str))
            sw.expect([self.prompt])
            auto_neg_str = b'on' if active else b'off'
            sw.write(b'configure ports %s auto %s speed 1000 duplex full\n' %
                     (ports_str.encode(), auto_neg_str))

    @grouped
    def init(self):
        if self.restart_disable_autoneg:
            self.set_auto_neg(True)
        self.restart_extreme_ports()
        
    @grouped
    def restart(self):
        if self.restart_disable_autoneg:
            self.set_auto_neg(False)
        self.restart_extreme_ports()

    @grouped
    def cleanup(self):
        if self.restart_disable_autoneg:
            self.set_auto_neg(True)


ConnectionTypes = {'Direct': DirectEthernetConnection,
                   'ExtremeSwitch': ExtremeSwitchConnection}

class NetCat:

    Listeners = {}

    class Listener:

        MTU = 1500

        class Refresh(Exception):
            pass

        def __init__(self, loc_port):
            self.loc_port = loc_port
            log('[COMMON]', 'Creating NetCat.Listener on port %d' % self.loc_port)
            self.socket = Socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.socket.bind(('', loc_port))
            self.queue_map = {}
            self.task = gevent.spawn(self.run)

        def __del__(self):
            log('[COMMON]', 'Destroying NetCatListener on port %d' % self.loc_port)

        def run(self):
            while self.queue_map:
                try:
                    s, anc, flags, (host, port) = self.socket.recvmsg(self.MTU)
                    if (host, port) in self.queue_map:
                        self.queue_map[(host, port)].put(s)
                except self.Refresh:
                    pass

        def add_remote_port(self, host, port):
            self.queue_map[(host, port)] = Queue()

        def del_remote_port(self, host, port):
            del self.queue_map[(host, port)]
            self.task.kill(self.Refresh, block=False)

        def send_mesg(self, host, port, s):
            self.socket.sendto(s, (host, port))

        def recv_mesg(self, host, port):
            return self.queue_map[(host, port)].get()

    @classmethod
    def get_listener(klass, port):
        if port in klass.Listeners:
            l = klass.Listeners[port]()
            if not l:
                raise RuntimeError('Invalid Listener reference in port %d', port)
            return l
        l = klass.Listener(port)
        deleter = functools.partial(klass.Listeners.pop, port)
        klass.Listeners[port] = weakref.ref(l, deleter)
        return l

    def __init__(self, loc_port, rem_host, rem_port, text=True):
        self.rem_host = socket.gethostbyname(rem_host)
        self.rem_port = rem_port
        self.text = text
        self.listener = self.get_listener(loc_port)
        self.listener.add_remote_port(self.rem_host, self.rem_port)
        self.read_buffer = b''

    def terminate(self):
        self.listener.del_remote_port(self.rem_host, self.rem_port)
        self.listener = None

    def write(self, s):
        if self.text:
            s = s.encode()
        self.listener.send_mesg(self.rem_host, self.rem_port, s)

    def read(self, size=None):
        ret = b''
        while not ret or (size and len(ret) < size):
            if self.read_buffer:
                avail = len(self.read_buffer)
                missing = size - len(ret) if size else 0
                xfer = min(avail, missing) if size else avail
                ret += self.read_buffer[:xfer]
                self.read_buffer = self.read_buffer[xfer:]
            else:
                s = self.listener.recv_mesg(self.rem_host, self.rem_port)
                self.read_buffer = s
        if self.text:
            ret = ret.decode()
        return ret


class EigerHalfModule:

    LOC_PORT = 3000
    REM_PORT = 3000

    MAIN_BIT = 'MAIN_BIT'
    LEFT_BIT = 'LEFT_BIT'
    RIGHT_BIT = 'RIGHT_BIT'
    KERNEL_LOCAL = 'KERNEL_LOCAL'

    TFTPDest = {MAIN_BIT: '/fw0', LEFT_BIT: '/febl', RIGHT_BIT: '/febr',
                KERNEL_LOCAL: '/kernel'}

    LocalFlashModeDir = get_eiger_executables_dir()
    RemoteFlashModeDir = 'executables'
    FlashModeFiles = ['boot_recovery', 'z_mem', 'z_mem_write']

    FLASH = 'flash'
    PING = 'ping'
    SSH = 'ssh'

    SSH_OPTS = ['KexAlgorithms=+diffie-hellman-group1-sha1',
                'PubkeyAcceptedKeyTypes=+ssh-dss']

    def __init__(self, name, connect):
        self.nc = None
        self.xterm = None
        self.in_flash_mode = None
        self.con_out_task = None
        self.con_pipe = None
        self.con_pipe_name = None
        self.log_file_name = None
        self.log_file = None

        self.name = name
        self.connect = connect
        self.con_pipe_name = '/tmp/eiger_flash_con_pipe.%s' % self.name
        if os.path.exists(self.con_pipe_name):
            os.unlink(self.con_pipe_name)
        os.mkfifo(self.con_pipe_name)

        if log_file_name:
            base, ext = log_file_name, ''
            if '.' in log_file_name:
                token = log_file_name.split('.')
                base, ext = list(map('.'.join, [token[:-1], ['', token[-1]]]))
            self.log_file_name = '%s_%s%s' % (base, self.name, ext)

    def __del__(self):
        if self.nc:
            self.nc.terminate()

        self.close_xterm()

        if self.con_pipe_name and os.path.exists(self.con_pipe_name):
            os.unlink(self.con_pipe_name)

    def upload_fw(self, fw_type, fw_name, wait=True):
        self.start_upload_file(fw_name, self.TFTPDest[fw_type], fw_type)
        if wait:
            self.wait_firmware_flash()

    def start_upload_file(self, fw_name, dest, fw_type=None):
        fw_data = self.get_fw_data(fw_name)

        self.open_con_term()

        size = fw_data['size']
        fw_type = fw_type if fw_type else 'Unknown'
        self.log('Uploading %s %s to %s (%d bytes)' % (fw_type, fw_name, 
                                                       dest, size))
        cmds = ['binary', 'put %s %s' % (fw_name, dest), '']
        to_send = '\n'.join(cmds)
        tftp = run_text_proc_capture(['tftp', self.name], input=to_send)
        prompt = 'tftp> '
        line = tftp.stdout.split(prompt)[2]
        xfer_report = re.compile('^Sent (?P<size>[0-9]+) bytes in '
                                 '(?P<sec>[0-9.]+) seconds$')
        m = xfer_report.match(line)
        if m:
            xferred = int(m.group('size'))
            if xferred != size:
                raise RuntimeError('TFTP size mismatch: %s (expected %s)' %
                                   (xferred, size))
            self.log('Transferred %s bit file %s (took %s sec)' % 
                     (fw_type, fw_name, m.group('sec')))

        self.flash_start_ts = time.time()

    def wait_firmware_flash(self):
        if not self.con_out_task:
            return

        self.log('Waiting for firmware flash to finish ...')
        self.con_out_task.join()
        self.con_out_task = None

        elapsed = self.flash_end_ts - self.flash_start_ts
        self.log('Firmware flash finished OK (took %.1f sec)' % elapsed)

    def ssh_host(self):
        return 'root@%s' % self.name

    def ssh_cmd(self, cmd=None, stdin=None, **kws):
        ssh_opts = ''.join(['-o %s ' % o for o in self.SSH_OPTS])
        ssh_opts += '-xa'
        args = 'ssh %s %s' % (ssh_opts, self.ssh_host())
        if cmd is not None:
            args += ' ' + cmd
        elif stdin is None:
            raise ValueError('Eiger ssh: Must specify one cmd,stdin')
        return run_proc(args.split(), stdin=stdin, capture_output=True, **kws)

    def open_con_term(self):
        self.check_flash_mode()
        self.start_xterm()
        if self.con_out_task:
            return

        self.con_out_task = gevent.spawn(self.con_out_task_fn)

    def check_flash_mode(self, silent=False):
        if self.get_flash_mode():
            return True

        def process_error(err):
            if not silent:
                raise RuntimeError(self.get_bad_mode_msg([self.name, err]))
            return err

        self.log('Not in firmware flash mode ... ping\'ing ...')
        self.connect.init()
        sleep_time = 10
        self.log('Waiting for connection to init (%s sec) ...' % sleep_time)
        gevent.sleep(sleep_time)

        ping = run_text_proc_capture(['ping', '-c1', self.name])
        if ping.returncode != 0:
            return process_error(self.PING)

        self.log('ping OK ... Check ssh ...')
        timeout = 10
        try:
            self.ssh_cmd('hostname', timeout=timeout)
        except TimeoutExpired:
            return process_error(self.SSH)

        self.start_flash_mode()

        self.in_flash_mode = None
        if not self.get_flash_mode():
            return process_error(self.FLASH)

        self.log("Entered into flash-mode OK!")
        return True

    def get_flash_mode(self):
        if self.in_flash_mode is not None:
            return self.in_flash_mode

        self.connect.check_link()

        self.start_nc()
            
        timeout = 1
        prompt = '%s > ' % self.name
        
        l = gevent.with_timeout(timeout, self.nc.read, len(prompt),
                                timeout_value='')
        self.in_flash_mode = bool(l and prompt in l)
        if not self.in_flash_mode:
            self.stop_nc()

        return self.in_flash_mode

    @classmethod
    def get_bad_mode_msg(klass, host_code_list):
        err = {}
        for h, c in host_code_list:
            hl = err.get(c, [])
            hl.append(h)
            err[c] = hl
        msg = []
        for e in klass.PING, klass.SSH:
            if e in err:
                hl = err.pop(e)
                multi = len(hl) > 1
                p = 's' if multi else ''
                d = '' if multi else 'es'
                hstr = ','.join(hl)
                msg += ['Host%s %s do%s not answer to %s!' % (p, hstr, d, e),
                        'Please restart the detector and wait for LED #4 to '
                        'become green']
        if klass.FLASH in err:
            hl = err.pop(klass.FLASH)
            multi = len(hl) > 1
            p = 's' if multi else ''
            b = 'are' if multi else 'is'
            hstr = ','.join(hl)
            msg += ['Host%s %s %s not in firmware flash mode!' % (p, hstr, b),
                    'Please insert a clip into the rear panel hole until '
                    'all LEDs are red,',
                    '  and then wait until LED #4 blinks gren/red']
        if err:
            hl = [h for hl in err.values() for h in hl]
            multi = len(hl) > 1
            p = 's' if multi else ''
            hstr = ','.join(hl)
            msg += ['Host%s %s: Unknown error(s): %s' % (p, hstr, err.keys())]
        return '\n'.join(msg)

    def start_flash_mode(self):
        self.check_flash_files()

        start_flash = self.FlashModeFiles[0]
        self.log('Starting flash-mode (%s) ...' % start_flash)
        cmd = 'cd %s && ./%s' % (self.RemoteFlashModeDir, start_flash)
        timeout = 5
        try:
            self.ssh_cmd(cmd, timeout=timeout)
        except:
            pass

        sleep_time = 20
        self.log('Waiting for flash-mode (%s sec) ...' % sleep_time)
        gevent.sleep(sleep_time)

        self.log('Restarting Ethernet connection ...')
        self.connect.restart()

        sleep_time = 20
        self.log('Waiting for connection (%s sec) ...' % sleep_time)
        gevent.sleep(sleep_time)

    def check_flash_files(self):
        def filescmd(dir_name, cmd, post_cmd=''):
            flist = ' '.join(self.FlashModeFiles)
            return 'cd %s && %s %s %s' % (dir_name, cmd, flist, post_cmd)
        def md5cmd(dir_name):
            return filescmd(dir_name, 'md5sum')

        self.log('Checking flash-mode setup files ...')
        cmd = md5cmd(self.LocalFlashModeDir)
        local_md5 = run_text_proc_capture(cmd, shell=True)
        local_md5lines = [l.split() for l in local_md5.stdout.split('\n')]
        cmd = md5cmd(self.RemoteFlashModeDir)
        rem_md5 = self.ssh_cmd(cmd, text=True)
        rem_md5lines = [l.split() for l in rem_md5.stdout.split('\n')]

        if rem_md5lines == local_md5lines:
            return

        self.log("Remote and local files differ!")
        for t, l in ('local', local_md5lines), ('remote', rem_md5lines):
            if l:
                for md5, n in l:
                    self.log('%s: %s  %s' % (t.capitalize(), md5, n))
            else:
                self.log('%s: No file found!' % t)

        self.log('Copying flash-mode setup files ...')
        rem_dir = '%s:%s' % (self.ssh_host(), self.RemoteFlashModeDir)
        cmd = filescmd(self.LocalFlashModeDir, 'scp', rem_dir)
        run_text_proc_capture(cmd)
        self.ssh_cmd('sync')

    def start_nc(self):
        if self.nc:
            return

        port_host_port = self.LOC_PORT, self.name, self.REM_PORT
        self.log('Running NetCat on port %d from host %s port %d' % port_host_port)
        self.nc = NetCat(*port_host_port, text=True)
        gevent.sleep(1)
        self.nc.write('\n')

    def stop_nc(self):
        self.nc.terminate()
        self.nc = None

    def con_out_task_fn(self):
        term = '\n\r\0'
        end = False
        while not end:
            l = ''
            while term not in l:
                l += self.nc.read(1)
            l = l.strip(term)
            end = l == 'Success'

            if self.con_pipe:
                try:
                    self.con_pipe.write('%s\n' % l)
                    self.con_pipe.flush()
                except:
                    self.close_xterm()

            if self.log_file:
                self.log_file.write('%s\n' % l)
                self.log_file.flush()

        self.flash_end_ts = time.time()

    def start_xterm(self):
        if self.xterm:
            return

        xterm_cmd = ['xterm', '-title', 'Eiger %s console' % self.name,
                     '-e', 'cat', self.con_pipe_name]
        self.log('Executing: %s' % ' '.join(xterm_cmd))
        self.xterm = Popen(xterm_cmd)

        con_pipe_os = open(self.con_pipe_name, 'wt')
        self.con_pipe = FileObject(con_pipe_os, 'wt')
        msg = '*** Output from %s console ***\n' % self.name
        self.con_pipe.write(msg)
        self.con_pipe.flush()

        if self.log_file_name:
            log_exists = os.path.exists(self.log_file_name)
            log_file_os = open(self.log_file_name, 'at')
            self.log_file = FileObject(log_file_os, 'wt')
            if log_exists:
                self.log_file.write('\n')
            self.log_file.write('Eiger flash - %s\n' % start_time)
            self.log_file.write(msg)
            self.log_file.flush()

    def close_xterm(self):
        if self.con_pipe:
            self.con_pipe = None

        if self.xterm:
            self.xterm.terminate()
            self.xterm = None

    def get_fw_data(self, fw_name):
        size = os.stat(fw_name).st_size
        return dict(size=size)

    def log(self, msg):
        log('[%s] %s' % (self.name, msg))

    def cleanup_flash_mode(self):
        self.connect.cleanup()


class Eiger:

    def __init__(self, host_list, flash_config):
        self.tasks = []
        if 'Connection' in flash_config:
            con_config = flash_config['Connection']
            con_type = con_config.get('Type', 'Direct')
        else:
            con_config = None
            con_type = 'Direct'
        con_klass = ConnectionTypes[con_type]
        self.hosts = [EigerHalfModule(host, con_klass(host, con_config))
                      for host in host_list]

    def __del__(self):
        self.wait_tasks()

    def check_flash_mode(self):
        tasks = [(eiger.name, gevent.spawn(eiger.check_flash_mode, silent=True))
                 for eiger in self.hosts]
        bad = [(n, t.get()) for n, t in tasks if t.get() != True]
        if bad:
            raise RuntimeError(EigerHalfModule.get_bad_mode_msg(bad))

    def upload_fw(self, fw_type, fw_name, wait=True):
        self.wait_tasks()
        for eiger in self.hosts:
            task = gevent.spawn(eiger.upload_fw, fw_type, fw_name)
            self.tasks.append(task)
        if wait:
            self.wait_tasks()

    def wait_tasks(self):
        for task in gevent.wait(self.tasks):
            self.tasks.remove(task)

    def cleanup_flash_mode(self):
        self.tasks = [gevent.spawn(eiger.cleanup_flash_mode)
                      for eiger in self.hosts]
        self.wait_tasks()


def flash_eiger_list(flash_config, fw_files, host_list):
    try:
        eiger = Eiger(host_list, flash_config)
        eiger.check_flash_mode()
        try:
            for fw_type, fw_name in fw_files:
                eiger.upload_fw(fw_type, fw_name)
        finally:
            eiger.cleanup_flash_mode()
    except Exception as e:
        sys.exc_info()
        log(str(e))


def heart_beat(msg):
    while True:
        log(msg)
        gevent.sleep(1)


def usage():
    prgname = os.path.basename(sys.argv[0])
    print("")
    print("Usage: %s [options] host [host ...]" % prgname)
    print("")
    print("  Options:")
    print("    -c  <flash config file>")
    print("    -m  <download.bit file>")
    print("    -l  <feb_l.bit file>")
    print("    -r  <feb_r.bit file>")
    print("    -k  <kernel_local file>")
    print("    -o  <output log file>")
    exit(1)


def get_config_boolean_option(config_section, opt_name, default=False):
    if opt_name not in config_section:
        return default
    return config_section.get(opt_name).lower() in ['yes', 'on', '1']

def main():
    global log_file_name
    globals().update([(n, getattr(EigerHalfModule, n))
                      for n in ['MAIN_BIT', 'LEFT_BIT', 'RIGHT_BIT', 
                                'KERNEL_LOCAL']])

    flash_config_name = None
    fw_types = [MAIN_BIT, LEFT_BIT, RIGHT_BIT, KERNEL_LOCAL]
    fw_files = dict([(x, None) for x in fw_types])
    beat = False

    fw_opts = {MAIN_BIT: '-m', LEFT_BIT: '-l', RIGHT_BIT: '-r', 
               KERNEL_LOCAL: '-k'}

    try:
        opts, args = getopt(sys.argv[1:], 'c:m:l:r:k:o:b')
        for opt, val in opts:
            if opt == '-c':
                flash_config_name = val
            for fw_type, fw_opt in fw_opts.items():
                if opt == fw_opt:
                    fw_files[fw_type] = val
            if opt == '-o':
                log_file_name = val
            if opt == '-b':
                beat = True

        fw_files = [(t, fw_files[t]) for t in fw_types if fw_files[t]]
        if not fw_files:
            print("Error: one or more firware file(s) must be specified")
            raise ValueError

        host_list = args
        if not host_list:
            raise ValueError
    except:
        usage()

    try:
        flash_config = ConfigParser()
        flash_config.read(flash_config_name)
        if 'Connection' in flash_config:
            con_config = flash_config['Connection']
            con_type = con_config.get('Type', 'Direct')
            if con_type not in ConnectionTypes:
                raise ValueError('cannot find connection type')
    except Exception as e:
        print("Error reading flash config file %s: %s" % flash_config_name, e)
        exit(1)

    log('Eiger flash - %s' % start_time)

    file_names = list(list(zip(*fw_files))[1])
    md5sum = run_text_proc(['md5sum'] + file_names, stdout=PIPE, stderr=STDOUT)
    for l in md5sum.stdout.split('\n'):
        log(l)
    if md5sum.returncode != 0:
        log('Error in md5sum %s' % ' '.join(file_names))
        exit(1)
    log('')

    if beat:
        gevent.spawn(heart_beat, "Beat")

    t = gevent.spawn(flash_eiger_list, flash_config, fw_files, host_list)
    while not t.ready():
        gevent.wait([t], 1)

    input('Press any key to quit ...')

if __name__ == '__main__':
    main()
