#!/usr/bin/env python

import PyTango
from PyTango import DevState
from PyTango.server import Device, DeviceMeta, device_property
from PyTango.server import attribute, command, get_worker

PyTango.requires_pytango('8.1.9', software_name='Jungfrau')

import gevent
from gevent.subprocess import Popen
from gevent.event import Event

import six
import time
import numpy
import signal
import math

@six.add_metaclass(DeviceMeta)
class Jungfrau(Device):

    DefaultDataDir = device_property(dtype=str, default_value='/tmp',
                                     doc='Default data directory path')

    CalcResultDim = device_property(dtype=int, default_value=2,
                                    doc='Dimension of calc. result vector')

    def init_device(self):
        Device.init_device(self)
        self.debug_stream("In init_device() of controller")
        self.data_dir = self.DefaultDataDir
        self.data_prefix = ''
        self.expo_time = 1.0
        self.single_expo_time = 0.1
        self.single_dead_time = 0.05
        self.calc_result = numpy.zeros((self.CalcResultDim,), numpy.float)
        self.acq_task = None
        self.stop_event = Event()
        self.child_event = Event()
        self.register_signal(signal.SIGCHLD)

    def signal_handler(self, intno):
        get_worker().execute(self.child_event.set)

    def dev_state(self):
        acq_running = get_worker().execute(self.is_acq_running)
        state = DevState.RUNNING if acq_running else DevState.ON
        self.set_state(state)
        return state

    def dev_status(self):
        self.dev_state()
        return self.get_state().name

    @attribute(dtype=str, label='Data directory path')
    def DataFileDir(self):
        return self.data_dir

    @DataFileDir.write
    def DataFileDir(self, data_dir):
        self.data_dir = data_dir

    @attribute(dtype=str, label='Data file name prefix')
    def DataFilePrefix(self):
        return self.data_prefix

    @DataFilePrefix.write
    def DataFilePrefix(self, data_prefix):
        self.data_prefix = data_prefix

    @attribute(dtype=float, label='Acq. sequence duration', unit='s')
    def ExpoTime(self):
        return self.expo_time

    @ExpoTime.write
    def ExpoTime(self, expo_time):
        self.expo_time = expo_time

    @attribute(dtype=float, label='Single exposure time', unit='s')
    def SingleExpoTime(self):
        return self.single_expo_time

    @SingleExpoTime.write
    def SingleExpoTime(self, single_expo_time):
        self.single_expo_time = single_expo_time

    @attribute(dtype=float, label='Single exposure time', unit='s')
    def SingleDeadTime(self):
        return self.single_dead_time

    @SingleDeadTime.write
    def SingleDeadTime(self, single_dead_time):
        self.single_dead_time = single_dead_time

    def real_single_time(self):
        return self.single_expo_time + self.single_dead_time

    def nb_frames(self):
        return math.ceil(self.expo_time / self.real_single_time())
      
    def is_acq_running(self):
        return bool(self.acq_task and not self.acq_task.ready())

    @command
    def Prepare(self):
        """Prepare an acquisition"""
        if self.is_acq_running():
            raise RuntimeError("Previous acq. did not finish yet")
        self.calc_result[:] = 0
        self.stop_event.clear()

    @command
    def Start(self):
        """Starts an acquisition"""
        self.acq_task = gevent.spawn(self.exec_acq)
    
    def exec_acq(self):
        # execute an external command (acquisition start)
        cmd_fmt = """
import time
for i in xrange(%d):
    time.sleep(%s)
"""
        cmd = cmd_fmt % (self.nb_frames(), self.real_single_time())
        self.exec_cmd(['python', '-c', cmd])

        # wait an arbitrary time (till the data is written)
        time_to_end = 3
        while time_to_end and not self.stop_event.is_set():
            self.exec_cmd('sleep 1', shell=True)
            time_to_end -= 1

        # calculate the result
        for i in xrange(self.CalcResultDim):
            self.calc_result[i] = (i + 1) * self.nb_frames()

    def exec_cmd(self, *args, **kws):
        cmd_str = args[0] if type(args[0]) == str else ' '.join(args[0])
        print("Executing\n%s" % cmd_str)
        t0 = time.time()
        self.child_event.clear()
        p = Popen(*args, **kws)
        self.child_event.wait()
        print("Done (%s)" % (time.time() - t0))

    @command
    def Stop(self):
        """Stops the current acquisition"""
        self.stop_event.set()

    @attribute(dtype=float, dformat=PyTango.AttrDataFormat.SPECTRUM,
               max_dim_x=1000, label='Calculation result')
    def CalcResult(self):
        return self.calc_result

def main(argv=None):
    from PyTango import GreenMode
    from PyTango.server import run
    run([Jungfrau], green_mode=GreenMode.Gevent)

if __name__ == '__main__':
    main()
