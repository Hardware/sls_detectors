#!/usr/bin/env python

import os
import math
from multiprocessing import RawArray
from multiprocessing.pool import Pool
import numpy as np
import h5py as h5

calib_dir = '/data/id29/inhouse/opid291/Jungfrau/Calibration/current'
calib_file = 'Jungfrau_4M_ID29.h5'
calib_dataset = '/entry_0000/pyFAI/Jungfrau_4M/pixel_corners'

pixel_size = 75e-6

chip_shape = [256] * 2
inter_chip_gap = [2] * 2
mod_chips = [2, 4]
mod_shape = [n * p + (n - 1) * g
             for n, p, g in zip(mod_chips, chip_shape, inter_chip_gap)]
print(f'Module shape={mod_shape}')

inter_mod_gap = [36, 8]
det_mods = [4, 2]
det_shape = [n * p + (n - 1) * g
             for n, p, g in zip(det_mods, mod_shape, inter_mod_gap)]
print(f'Detector shape={det_shape}')

with h5.File(os.path.join(calib_dir, calib_file), 'r') as f:
    calib_data = f[calib_dataset][:]

calib_det_shape = list(calib_data.shape[:2])
assert(calib_det_shape == det_shape)
# theoretical Y,X coordinates of top-left corner of each pixel (pixel units)
theo_pos = np.indices(det_shape).swapaxes(0, 2).swapaxes(0, 1)
# real Y,X coordinates of top-left corner of each pixel (pixel units)
real_pos = calib_data[:,:,0,1:] / pixel_size
# error in Y, X coordinates (pixel units)
pixel_error = real_pos - theo_pos

out_dir = '/tmp_14_days/ahoms/jungfrau/calib'
contrib_fname = os.path.join(out_dir, 'pixel_neighbors_contrib.h5')
geom_csr_fname = os.path.join(out_dir, 'geom_csr.h5')

pixel_neighbors_distance = 3
pixel_region_size = [2 * pixel_neighbors_distance + 1] * 2
pixel_neighbors = np.indices(pixel_region_size).swapaxes(0, 2).swapaxes(0, 1)
pixel_neighbors_flat = (pixel_neighbors.reshape((-1, 2)) -
                        pixel_neighbors_distance)
nb_neighbors = np.product(pixel_region_size)
print(f'pixel_region_size={pixel_region_size}, nb_neighbors={nb_neighbors}')

neighbor_contrib_shape = det_shape + pixel_region_size
neighbor_contrib_pixels = int(np.product(neighbor_contrib_shape))
neighbor_contrib_array = RawArray('f', neighbor_contrib_pixels)
neighbor_contrib_raw = np.frombuffer(neighbor_contrib_array, 'float32')
neighbor_contrib = neighbor_contrib_raw.reshape(neighbor_contrib_shape)
neighbor_contrib_flat = neighbor_contrib.reshape(det_shape + [nb_neighbors])

def pixel_contrib(e):
    return np.product(1 - np.abs(e)) if (np.abs(e) < 1).all() else 0

for e in [(0.5, 0.5), (-0.5, 0.5), (0.2, 0.2), (0.2, 0.5), (0.5, 0)]:
    print(f'pixel_contrib({e})={pixel_contrib(np.array(e))}')

def pixel_idx(c):
    return c[0] * det_shape[1] + c[1]

def calc_contrib_row(row):
    row_pixels = det_shape[1]
    max_nb_pixel_contrib = 8 * row_pixels
    indptr = np.zeros((row_pixels + 1,), 'int32')
    indices = np.zeros((max_nb_pixel_contrib,), 'int32')
    data = np.zeros((max_nb_pixel_contrib,), 'float32')
    curr_idx = 0
    pos_contrib = zip(theo_pos[row], neighbor_contrib_flat[row])
    for i, (p, contrib) in enumerate(pos_contrib):
        indptr[i] = curr_idx
        for j, n in enumerate(pixel_neighbors_flat):
            r = p + n
            if ((r >= 0) & (r < det_shape)).all():
                x = pixel_contrib(pixel_error[r[0],r[1]] + n)
                contrib[j] = x
                if x > 0:
                    indices[curr_idx] = pixel_idx(r)
                    data[curr_idx] = x
                    curr_idx += 1
    indptr[row_pixels] = curr_idx
    return indptr, indices[:curr_idx], data[:curr_idx]

print(f'neighbor_contrib={neighbor_contrib.shape}')
det_pixels = np.product(neighbor_contrib.shape[:2])
print(f'det_pixels={det_pixels}')

max_nb_pixel_contrib = 8 * det_pixels
indptr_arr = np.zeros((det_pixels + 1,), 'int32')
indices_arr = np.zeros((max_nb_pixel_contrib,), 'int32')
data_arr = np.zeros((max_nb_pixel_contrib,), 'float32')

with Pool() as pool:
    all_res = pool.imap(calc_contrib_row, range(det_shape[0]))
    for row, (indptr, indices, contrib) in enumerate(all_res):
        first_row_idx = pixel_idx((row, 0))
        last_row_idx = pixel_idx((row + 1, 0))
        curr_idx = indptr_arr[first_row_idx]
        indptr_arr[first_row_idx:last_row_idx + 1] = indptr + curr_idx
        first_idx = indptr_arr[first_row_idx]
        last_idx = indptr_arr[last_row_idx]
        indices_arr[first_idx:last_idx] = indices
        data_arr[first_idx:last_idx] = contrib
        print(f'row={row}, nb_contrib={indptr[-1]}, tot_contrib={last_idx}')
    nb_contrib = indptr_arr[det_pixels]
    print(f'nb_contrib={nb_contrib}')

print(f'Saving to {contrib_fname}...')
with h5.File(contrib_fname, 'w') as f:
    f.create_dataset('/data', data=neighbor_contrib, compression='gzip')

print(f'Saving to {geom_csr_fname}...')
with h5.File(geom_csr_fname, 'w') as f:
    f.create_dataset('/data', data=data_arr[:nb_contrib])
    f.create_dataset('/indices', data=indices_arr[:nb_contrib])
    f.create_dataset('/indptr', data=indptr_arr)
