#!/usr/bin/env python3

import sys
import os
import argparse
import socket
import time
import re
import glob
import subprocess
import json

import numpy as np
import h5py as h5

user_name = "blissadm"
host_conda_envs = {
    "lid00lima23": "slsdetector_dev",
}

# Target directory
target_dir="/users/blissadm/local/id09/jungfrau/calibration"

# Jungfrau packet parameters
jfrau_pixel_depth = np.dtype("uint16").itemsize
jfrau_packet_header_len = 64 # (4 + 48) left-padded to be 128-bit aligned
jfrau_packet_header_pixels = jfrau_packet_header_len // jfrau_pixel_depth
jfrau_packet_data_len = 8 * 1024
jfrau_packet_data_pixels = jfrau_packet_data_len // jfrau_pixel_depth

# Jungfrau geometry
chip_size = 256
mod_chips = [4, 2]
chip_gap = 2
mod_size_wg = [n * chip_size + (n - 1) * chip_gap for n in mod_chips]
print(f"Module size (with gap): {mod_size_wg}")

# Detector gain calibration
gain_map_dir = "gain_maps/2024.04"
gain_map_link = "gain_maps"
gain_calib_date = "2023-10-0?"
gain_calib_sc_date = "2024-02-0?"
jfrau_mod_sensors = [593, 601]
jfrau_mod_names = [f"jfrau1mid09b{i}" for i in range(len(jfrau_mod_sensors))]
jfrau_nb_sc = 16

gain_wg_std = "gains_wg_std.h5"
gain_wg_sc = "gains_wg_sc.h5"

# Bchip replacement
jfrau_bchip_replace = {
}

# Lima Tango server info
tango_host = "l-cb184-1:20000"
lima_tango_server_name = "id09_jungfrau1m"

# pyFAI detector definition
det_def_uncorrected="Jungfrau_1M_ID09_uncorrected.h5"
det_def="Jungfrau_1M_ID09.h5"

# Auxiliary files: ng=NoGap, wg=WithGap
pixel_map_wg = "pixel_map_wg.h5"
csr_wg = "csr_wg.h5"
bin_centers_wg = "bin_centers_wg.h5"
r_center_wg = "r_center_wg.h5"
geom_lut = "psi_geom_lut.h5"
mask = "mask.h5"
mask_wg = "mask_wg.h5"
mask_wide_basic = "mask_wide_basic.h5"
mask_wide_extra = "mask_wide_extra.h5"

# parse command line arguments
app_desc = "Jungfrau Calibration Generator"
parser = argparse.ArgumentParser(description=app_desc)
parser.add_argument("--cal_tag",
                    help="Calibration tag (subdirectory): YY-MM-DD")

args = parser.parse_args()

def log_stage(msg, *args, **kws):
    print("**** " + msg)

prog_name = os.path.basename(sys.argv[0])

log_stage("Checking that we are at the good place ...")
host_name = socket.gethostname()
valid_hosts = host_conda_envs.keys()
if os.getlogin() != user_name or host_name not in valid_hosts:
    raise RuntimeError(f"{prog_name} must run as {user_name}@{valid_hosts}")
elif os.environ.get("CONDA_DEFAULT_ENV") != host_conda_envs[host_name]:
    raise RuntimeError(f"{prog_name} must run inside "
                       f"{host_conda_envs[host_name]} conda environment")

SLS_DETECTORS_DIR = os.environ.get("SLS_DETECTORS_DIR")
if not (SLS_DETECTORS_DIR and os.path.exists(SLS_DETECTORS_DIR)):
    raise RuntimeError(f"Bad SLS_DETECTORS_DIR: {SLS_DETECTORS_DIR}")

# Shell helpers
def run_sh(cmd, **args):
    shell = isinstance(cmd, str) or cmd[:2] != ["bash", "-c"]
    if shell and not isinstance(cmd, str):
        cmd = " ".join(cmd)
    p = subprocess.run(cmd, shell=shell, check=False, text=True, **args)
    if p.returncode != 0:
        if args.get("capture_output", False) and p.stderr:
            print(f"Error: {p.stderr}")
        raise subprocess.CalledProcessError(p.returncode, cmd,
                                            output=p.stdout, stderr=p.stderr)
    return p

for p in os.environ["PATH"].split(":"):
    conda_activate = os.path.join(p, "../bin/activate")
    if os.path.exists(conda_activate):
        break
else:
    raise RuntimeError("Cannot find conda activate script")

def run_conda_sh(env, cmd, **args):
    # Explicitly execute bash (instead of sh)
    sh_cmd = f". {conda_activate} {env} && {cmd}"
    return run_sh(["bash", "-c", sh_cmd], **args)

log_stage("Checking for needed scripts ...")
fai_scripts_dir = os.path.join(SLS_DETECTORS_DIR, "lima2/processings/common/fai/scripts")
jfrau_scripts_dir = os.path.join(SLS_DETECTORS_DIR, "jungfrau/scripts")
required_entries = [fai_scripts_dir, jfrau_scripts_dir]
if not all([os.path.exists(d) for d in required_entries]):
    raise RuntimeError(f"Missing: {required_entries}")
print(f"Found: {required_entries}")

os.chdir(target_dir)

cal_tag = args.cal_tag or time.strftime("%Y-%m-%d")
if not os.path.exists(cal_tag):
    run_sh(f"mkdir {cal_tag}")

os.chdir(cal_tag)

if not os.path.exists(os.path.basename(gain_map_link)):
    run_sh(f"ln -s ../{gain_map_dir} {gain_map_link}")

def backup_entry(name, backup):
    if os.path.exists(name):
        run_sh(f"rm -f {backup} && mv {name} {backup}")

def update_link(name, target):
    run_sh(f"rm -f {name} && ln -s {target} {name}")

def backup_update_link(name, backup, target):
    backup_entry(name, backup)
    update_link(name, target)

def get_lima_sdk_config(server_name, server_bin="LimaCCDs",
                        jfrau_cls="SlsDetectorJungfrau", tango_host=tango_host):
    log_stage("Reading Lima config file from Tango DB ...")

    cmd = f"""
import tango
import json
db = tango.Database()
lima_serv = "{server_bin}/{server_name}"
jfrau_cls = "{jfrau_cls}".lower()
dev_cls_list = db.get_device_class_list(lima_serv).value_string
for dn, dc in zip(dev_cls_list[0::2], dev_cls_list[1::2]):
    if dc.lower() == jfrau_cls:
        break
else:
    raise RuntimeError("Could not find {jfrau_cls} dev in {{lima_serv}}")
config_file = db.get_device_property(dn, "config_fname")["config_fname"][0]
with open(config_file, "rt") as f:
    for l in f:
        print(l)
"""
    p = run_conda_sh("lima_launcher", f"TANGO_HOST={tango_host} python",
                     input=cmd, capture_output=True)
    return p.stdout.split("\n")

lima_sdk_config = get_lima_sdk_config(lima_tango_server_name)

log_stage("Getting detector basic geometry ...")
det_size_ng = None
jfrau_udp_ifaces = None
for l in lima_sdk_config:
    tok = l.split()
    if len(tok) == 3 and tok[0] == "detsize":
        det_size_ng = [int(x) for x in tok[1:3]]
    elif len(tok) == 2 and tok[0] == "numinterfaces":
        jfrau_udp_ifaces = int(tok[1])
if not (det_size_ng and jfrau_udp_ifaces):
    raise RuntimeError("Could not find detsize/numinterfaces in SDK config")
print(f"Detector size w/o gaps: {det_size_ng}")
print(f"Nb. UDP interfaces: {jfrau_udp_ifaces}")

kilo_pixels = np.product(det_size_ng) // 1024
mega_pixels = kilo_pixels // 1024
det_geom_name = f"{mega_pixels}M" if mega_pixels else f"{kilo_pixels}k"
print(f"Found config for Jungfrau-{det_geom_name}")

log_stage("Querying detector AsmWG size ...")
det_geom_type = f"jungfraux{jfrau_udp_ifaces}"
det_size_str = ",".join([str(x) for x in det_size_ng])
cmd = f"sls_geometry_assembler {det_geom_type} {det_size_str} 1 pixel_idx Raw uint32 '' AsmWG uint32 '' -1"
p = run_sh(cmd, capture_output=True)

def decode_geom(geom, out):
    header = f"{geom.capitalize()} geometry: "
    target_geom = [x for x in out.split("\n") if x.startswith(header)][0]
    target_geom_re = re.compile(f"^{header}XY<(?P<x>[0-9]+),(?P<y>[0-9]+)>")
    m = target_geom_re.match(target_geom)
    if not m:
        raise RuntimeError(f"Cannot decode detector {geom} geometry: {target_geom}")
    return [int(m[g]) for g in "xy"]

geometries = {g: decode_geom(g, p.stdout) for g in ("source", "target")}
print("Assembler geometries:")
for g, s in geometries.items():
    print(f"  {g.capitalize()}: {s}")
det_size_wg = geometries["target"]
print(f"Detector size (with gap): {det_size_wg}")

det_chips = [i // chip_size for i in det_size_ng]
print(det_chips, mod_chips)
det_mods = [d // m for d, m in zip(det_chips, mod_chips)]
print(det_mods)
def calc_mod_gap(d, m, n):
    return (d - m * n) // (n - 1) if n > 1 else 0
mod_gap = [calc_mod_gap(d, m, n) for d, m, n in zip(det_size_wg, mod_size_wg, det_mods)]
print(f"Detector modules: {det_mods}")
print(f"Inter-module gap: {mod_gap}")

log_stage("Creating the raw-in-assembled map file ...")
script = os.path.join(fai_scripts_dir, "psi/gen_pixel_map.py")
cmd = f"python {script} {det_geom_name} Raw AsmWG {pixel_map_wg} 2"
run_sh(cmd)

log_stage("Creating the geometry LUT file ...")
with h5.File(pixel_map_wg, "r") as f:
    raw_pixel_idx = np.array(f["/data"])
raw_packet, pixel_idx = np.divmod(raw_pixel_idx, jfrau_packet_data_pixels)
header_offset = (raw_packet + 1) * jfrau_packet_header_pixels
valid_pixels = (raw_pixel_idx >= 0)
raw_pixel_idx[valid_pixels] += header_offset[valid_pixels]
with h5.File(geom_lut, "w") as f:
    f.create_dataset("/data", data=raw_pixel_idx)

log_stage("Finding Jungfrau module index in SDK config ...")
for l in lima_sdk_config:
    tok = l.split()
    if len(tok) == 2 and tok[0] == "hostname":
        hosts = tok[1].split("+")[:-1]
        # Replace bad module(s)
        for new_bchip, orig_bchip in jfrau_bchip_replace.items():
            hosts[hosts.index(new_bchip)] = orig_bchip
        assert len(hosts) == len(jfrau_mod_names)
        sdk_mod_idx = [hosts.index(m) for m in jfrau_mod_names]
        break
else:
    raise RuntimeError("Did not find hostname key in Lima SDK config")

print("Jungfrau module index in SDK config: ",
      {n: i for n, i in zip(jfrau_mod_sensors, sdk_mod_idx)})

log_stage("Creating gain calibration file ...")
def mod_calib_file(mod):
    fname = f"gainMaps_M{mod}_{gain_calib_date}.bin"
    return glob.glob(f"{gain_map_link}/Module{mod}/{fname}")[0]

def mod_calib_file_sc(mod, sc):
    fname = f"gainMaps_M{mod}_sc{sc}_{gain_calib_sc_date}.bin"
    return glob.glob(f"{gain_map_link}/Module{mod}/{fname}")[0]

mod_calib_files = " ".join([mod_calib_file(m)
                            for m in jfrau_mod_sensors])
script = os.path.join(jfrau_scripts_dir, "gen_calibration.py")
mod_idx_str = ",".join([str(i) for i in sdk_mod_idx])
cmd = (f"python {script} --sdk_mod_idx_list {mod_idx_str} "
       f"--asm_wg_det_calib {gain_wg_std} {det_geom_name} {mod_calib_files}")
run_sh(cmd)

mod_calib_file_scs = " ".join([mod_calib_file_sc(m, sc)
                            for sc in range(jfrau_nb_sc)
                            for m in jfrau_mod_sensors])
script = os.path.join(jfrau_scripts_dir, "gen_calibration.py")
mod_idx_str = ",".join([str(i) for i in sdk_mod_idx])
cmd = (f"python {script} --sdk_mod_idx_list {mod_idx_str} "
       f"--asm_wg_det_calib {gain_wg_sc} {det_geom_name} {mod_calib_file_scs}")
run_sh(cmd)

log_stage("Creating (uncorrected) detector geometry ...")
script = os.path.join(fai_scripts_dir, "psi/gen_detector.py")
cmd = f"python {script} --pixel_map {pixel_map_wg} {det_def_uncorrected}"
run_conda_sh("pyfai", cmd)

log_stage("Creating default mask file ...")
script = os.path.join(fai_scripts_dir, "gen_mask.py")
gen_mask_params = [f"--pixel_map {pixel_map_wg}",
                   f"--wide_gap_mask {mask_wide_basic}",
                   f"{mask_wg}"]
cmd = " ".join([f"python {script}"] + gen_mask_params)
run_sh(cmd)

extra_mod_gap = 2
log_stage("Enlarging inter-module gap ...")
with h5.File(mask_wide_basic, "r") as f:
    m = np.array(f["/data"], "bool")
for i in range(1, det_mods[0]):
    x0 = mod_size_wg[0] * i + mod_gap[0] * (i - 1) - extra_mod_gap
    x1 = x0 + mod_gap[0] + 2 * extra_mod_gap
    m[:,x0:x1] = True
for i in range(1, det_mods[1]):
    y0 = mod_size_wg[1] * i + mod_gap[1] * (i - 1) - extra_mod_gap
    y1 = y0 + mod_gap[1] + 2 * extra_mod_gap
    m[y0:y1,:] = True
with h5.File(mask_wide_extra, "w") as f:
    f.create_dataset("/data", data=m)

