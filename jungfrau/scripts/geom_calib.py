import os
import math
import numpy as np
import h5py as h5

calib_dir = '/data/id29/inhouse/opid291/Jungfrau/Calibration/current'
calib_file = 'Jungfrau_4M_ID29.h5'
calib_dataset = '/entry_0000/pyFAI/Jungfrau_4M/pixel_corners'

err_dir = '.'
err_file = 'geom_err.h5'

pixel_size = 75e-6

chip_shape = [256] * 2
inter_chip_gap = [2] * 2
mod_chips = [2, 4]
mod_shape = [n * p + (n - 1) * g
             for n, p, g in zip(mod_chips, chip_shape, inter_chip_gap)]
print(f'Module shape={mod_shape}')

inter_mod_gap = [36, 8]
det_mods = [4, 2]
det_shape = [n * p + (n - 1) * g
             for n, p, g in zip(det_mods, mod_shape, inter_mod_gap)]
print(f'Detector shape={det_shape}')

nb_mods = det_mods[0] * det_mods[1]
mod_idx = [(i // det_mods[1], i % det_mods[1]) for i in range(nb_mods)]
        
def calc_mod_corner(x, c):
    top_left = [n * (p + g)
                for n, p, g in zip(x, mod_shape, inter_mod_gap)]
    corner = c // 2, c % 2
    return [p + c * (s - 1) for p, c, s in zip(top_left, corner, mod_shape)]

def get_mod_corners(x):
    return [calc_mod_corner(x, c) for c in range(4)]

mod_corners = [[get_mod_corners((y, x)) for x in range(det_mods[1])]
               for y in range(det_mods[0]) ]
    
for i, (y, x) in enumerate(mod_idx):
    print(f'Module {i} corners: {mod_corners[y][x]}')
    top_left = mod_corners[y][x][0]
    geom_data = list(zip(top_left, chip_shape, inter_chip_gap))
    for cy in range(mod_chips[0]):
        for cx in range(mod_chips[1]):
            chip_idx = (cy, cx)
            first = [tl + c * (s + g)
                     for c, (tl, s, g) in zip(chip_idx, geom_data)]
            last = [f + s - 1 for f, s in zip(first, chip_shape)]
            print(f'  Chip {chip_idx}: {first}-{last}')

with h5.File(os.path.join(calib_dir, calib_file), 'r') as f:
    calib_data = f[calib_dataset][:]

calib_det_shape = list(calib_data.shape[:2])
assert(calib_det_shape == det_shape)
theo_pos = np.indices(det_shape) * pixel_size

err_shape = [2] + det_shape
err_data = np.zeros(err_shape, 'float64')
for i in range(2):
    err_data[i] = calib_data[:,:,0,i+1] - theo_pos[i]

def real_mod_corners(mod_corners, pixel_units=True):
    def real_pos(y, x):
        s = pixel_size if pixel_units else 1
        return [calib_data[y,x,0,i+1] / s for i in range(2)]
    return [real_pos(*x) for x in mod_corners]

def real_mod_corner_errors(mod_corners):
    def err(c, r):
        return [b - a for a, b in zip(c, r)]
    return [err(c, r)
            for c, r in zip(mod_corners, real_mod_corners(mod_corners))]

def get_mod_disp_rot(mod_corners):
    err = real_mod_corner_errors(mod_corners)
    disp = err[0]
    dy, dx = [b - a for a, b in zip(*err[:2])]
    rot = math.asin(dy / mod_shape[1])
    return disp, rot

for i, (y, x) in enumerate(mod_idx):
    err = real_mod_corner_errors(mod_corners[y][x])
    disp, rot = get_mod_disp_rot(mod_corners[y][x])
    print(f'Real module {i} corners:')
    print(f'    errors:', [[f'{x:.3f}' for x in c] for c in err])
    print(f'    disp:', [f'{x:.3f}' for x in disp])
    print(f'    rot: {rot * 1e3:.2f} mrad')

with h5.File(os.path.join(err_dir, err_file), 'w') as f:
    f.create_dataset('/data/pos', data=err_data)
    f.create_dataset('/data/pixel', data=err_data / pixel_size)
