import sys
import os
import numpy as np
import h5py as h5
import fabio

h5_in_fname = sys.argv[1]

unity = True
if len(sys.argv) > 3:
    baseline = float(sys.argv[2])
    factor = float(sys.argv[3])
    unity = (baseline == 0) and (factor == 1)

fname_prefix = os.path.basename(h5_in_fname)[:-3]

fmt = 'cbf'

out_dir = f'{fmt}'

os.system(f'mkdir -p {out_dir}')

with h5.File(h5_in_fname) as h5_in_file:
    idata = h5_in_file['data']

    mod = getattr(fabio, f'{fmt}image')
    klass = getattr(mod, f'{fmt}image')

    # single frame files
    if len(idata.shape) == 2:
        idata = [idata[:]]

    for i, frame in enumerate(idata):
        if unity:
            photon = frame
            suffix = ''
        else:
            photon = (frame - baseline) / factor
            suffix = '_photon'
    
        if fmt == 'cbf':
            photon = np.array(photon, dtype='int32')
            photon[photon > 2**30] = -1
    
        image = klass(data=photon)
        out_fname = os.path.join(out_dir,
                                 f'{fname_prefix}{suffix}_{i:04}.{fmt}')
        image.write(out_fname)
