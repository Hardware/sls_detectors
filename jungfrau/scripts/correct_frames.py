import sys
import numpy as np
import h5py as h5
import functools
from multiprocessing import Pool, shared_memory
from multiprocessing.managers import SharedMemoryManager
import fabio

data_fname = sys.argv[1]
mask_fname = sys.argv[2]
corr_method = sys.argv[3]

disp_suffix = '_nodisp'
suffix = disp_suffix if disp_suffix in data_fname else ''

ped_fname_list = [f'pedestal_20190324_0846.JF07T32V01_asm_2500_10{suffix}.h5',
                  f'pedestal_20190324_0846.JF07T32V01_asm_3000_10{suffix}.h5',
                  f'pedestal_20190324_0846.JF07T32V01_asm_3300_10{suffix}.h5']

gain_fname = f'gains_201810_asm_0_4{suffix}.h5'

ped_ave_fname = f'pedestal_ave{suffix}.h5'

corr_data_fname_prefix = data_fname[:-3] + '_corr'

esrf_baseline = 5000
energy_kev = 9.06

frame_slices = 1

pixel_gain_values = (0, 1, 3)
nb_gains = len(pixel_gain_values)

def swapaxes(d):
    return np.swapaxes(np.swapaxes(d, 1, 2), 0, 2)

def write_data(data, fname):
    if data.shape[0] == 1:
        data = data[0]
    with h5.File(fname, 'w') as h5_out_file:
        h5_out_file.create_dataset('data', data=data)

def ave_pedestals(fname_list):
    nb_ped = len(fname_list)
    ave = None
    for i, fname in enumerate(fname_list):
        with h5.File(fname, 'r') as h5_in_file:
            idata = h5_in_file['data'][:]
            if ave is None:
                ave = np.zeros((nb_ped, idata.shape[1], idata.shape[2]),
                               idata.dtype)
            ave[i] = np.average(idata & 0x3fff, 0)
    return ave

with h5.File(mask_fname, 'r') as h5_in_file:
    mask = h5_in_file['data'][:]
valid = mask == 1

with h5.File(gain_fname, 'r') as h5_in_file:
    base_gain = h5_in_file['data'][:nb_gains]
gain = swapaxes(base_gain)

ave_gain = np.array([np.average(gain[:,:,i][valid])
                     for i in range(nb_gains)])
g0_g1 = ave_gain[0] / ave_gain[1]
g1_g2 = ave_gain[1] / ave_gain[2]
print(f'ave_gain={ave_gain}, g0/g1={g0_g1:.6f}, g1/g2={g1_g2:.6f}')

if corr_method == 'psi2esrf':
    gain = np.zeros(mask.shape, 'float64') + ave_gain[0]
    ped = np.zeros(mask.shape, 'uint32') + esrf_baseline
else:
    ped_ave = ave_pedestals(ped_fname_list)
    if ped_ave_fname:
        write_data(ped_ave, ped_ave_fname)
    ped = swapaxes(ped_ave)

    ref_gain = np.array([ave_gain[0]] * nb_gains)
    esrf_gain = ref_gain / gain
    esrf_ped = esrf_baseline - esrf_gain * ped

with h5.File(data_fname, 'r') as h5_in_file:
    data = h5_in_file['data'][:]

gain_ped_list = [esrf_gain, esrf_ped] if corr_method == 'esrf' else [gain, ped]

def correct_frame(frame, corr_frame, lines=None):
    f, l = lines if lines is not None else (0, frame.shape[0])
    data_list = [corr_frame, frame, mask] + gain_ped_list
    c, d, m, g, p = [x[f:l] for x in data_list]
    if corr_method == 'psi2esrf':
        c[...] = d * g + p
    else:
        pg = d >> 14
        pv = np.array(d & 0x3fff, dtype='int16')
        for i, gv in enumerate(pixel_gain_values):
            gm = (pg == gv)
            f, o = g[:,:,i], p[:,:,i]
            if corr_method == 'psi':
                c[gm] = (pv[gm] - o[gm]) / f[gm]
            else:
                c[gm] = pv[gm] * f[gm] + o[gm]
    masked = (m != 1)
    c[masked] = m[masked]

def get_slices(size, nb_slices):
    slice_size = size // nb_slices
    done = 0
    while done < size:
        block_size = min(slice_size, size - done)
        yield done, block_size
        done += block_size

with SharedMemoryManager() as smm:
    is_psi = (corr_method == 'psi')
    corr_dtype = 'float64' if is_psi else 'uint32'

    frames, height, width = data.shape
    corr_size = frames * height * width * np.dtype(corr_dtype).itemsize
    shm = smm.SharedMemory(corr_size)

    corr_data_fname_base = f'{corr_data_fname_prefix}_{corr_method}'
    corr_data_fname = corr_data_fname_base + '.h5'
    ave_corr_data_fname = corr_data_fname_base + '_ave.h5'
    ave_corr_data_edf_fname = corr_data_fname_base + '_ave.edf'

    def correct(frame_lines):
        frame, lines = frame_lines
        child_shm = shared_memory.SharedMemory(shm.name)
        corr_data = np.ndarray(data.shape, dtype=corr_dtype,
                               buffer=child_shm.buf)
        correct_frame(data[frame], corr_data[frame], lines)

    with Pool() as p:
        in_data = [(frame, (first_line, first_line + step))
                   for frame in range(frames)
                   for first_line, step in get_slices(height, frame_slices)]
        for x in p.imap_unordered(correct, in_data):
            pass

    corr_data = np.ndarray(data.shape, dtype=corr_dtype, buffer=shm.buf)
    print(f'Writing corrected data on {corr_data_fname}')
    write_data(corr_data, corr_data_fname)

    ave_corr_data = np.average(corr_data, 0)
    print(f'Writing averaged corrected data on {ave_corr_data_fname}')
    write_data(ave_corr_data, ave_corr_data_fname)
    print(f'Writing averaged corrected data on {ave_corr_data_edf_fname}')
    edf = fabio.edfimage.edfimage(data=ave_corr_data)
    edf.write(ave_corr_data_edf_fname)
