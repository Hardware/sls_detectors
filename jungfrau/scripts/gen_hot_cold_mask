#!/usr/bin/env python

import os
import sys
import argparse

import numpy as np
import h5py as h5

parser = argparse.ArgumentParser(description='Generate hot/cold pixel masks.')
parser.add_argument('--base_mask', default='mask_wide.h5',
                    help='Base mask file')
parser.add_argument('--cold_thres', type=float, default=-10000.0,
                    help='Cold threshold')
parser.add_argument('--hot_thres', type=float, default=10000.0,
                    help='Hot threshold')
parser.add_argument('--min_occurrence', type=float, default=30,
                    help='Minimun number of frames with event (in %)')
parser.add_argument('--frames_per_file', type=int, default=0,
                    help='Number of frames per file to process')
parser.add_argument('--output_mask', help='Output mask file name')
parser.add_argument('--print_found', action='store_true',
                    help='Print coordinates of found pixels')
parser.add_argument('input_data', nargs='+', help='Input file name')

args = parser.parse_args()

mask = None
if args.base_mask:
    with h5.File(args.base_mask, 'r') as f:
        mask = np.array(f['/data'])

events = None
keys = ('cold', 'hot')

thres = {k: getattr(args, f'{k}_thres') for k in keys}

read_frames = 0

for fn in args.input_data:
    print(f"Reading file {fn} ...")
    with h5.File(fn, 'r') as f:
        path = '/entry_0000/measurement/data'
        nb_frames = len(f[path])
        if args.frames_per_file:
            nb_frames = min(nb_frames, args.frames_per_file)
        for d in f[path][:nb_frames]:
            if events is None:
                events = {k: np.zeros(d.shape, 'uint64') for k in keys}
            for k, t in thres.items():
                x = (d < t) if k == 'cold' else (d > t)
                if mask is not None:
                    x &= ~mask
                events[k] += x
            read_frames += 1

print(f"Done! {read_frames} frames read")

event_thres = read_frames * args.min_occurrence / 100
out_mask = {k: e >= event_thres for k, e in events.items()}
event_found = {k: m.sum() for k, m in out_mask.items()}
for k, m in out_mask.items():
    print(f'Found {k} pixels: {m.sum()}')
    if args.print_found:
        f = np.indices(m.shape).swapaxes(0, 1).swapaxes(1, 2)[m]
        for i, (y, x) in enumerate(f):
            print(f'{i:03d}: ({x}, {y})')

out_mask['all'] = out_mask['cold'] | out_mask['hot']
if mask is not None:
    out_mask['all'] |= mask

if args.output_mask:
    with h5.File(args.output_mask, 'w') as f:
        f.create_dataset('/data', data=out_mask['all'])
