#!/usr/bin/env python

import sys
import os
import argparse
import numpy as np
import h5py as h5
from functools import reduce

parser = argparse.ArgumentParser(description='Generate detector calibration.')
parser.add_argument('--sdk_mod_idx_list',
                    help='Comma-separated module idx list in SDK config file')
parser.add_argument('--sls_conda_env',
                    help='Conda environment with sls_geometry_assembler')
parser.add_argument('--raw_det_calib', default='gains_raw.h5',
                    help='output Raw calibration file name')
parser.add_argument('--asm_wg_det_calib', default='gains_wg.h5',
                    help='output AsmWG calibration file name')
parser.add_argument('det_geom', help='Detector geometry: 500k|1M|4M|16M')
parser.add_argument('mod_calib', nargs='+',
                    help='module calibration files')

args = parser.parse_args()

mod_shape = (512, 1024)

det_modules = {
    '500k': (1, 1),
    '1M': (1, 2),
    '4M': (2, 4),
    '16M': (4, 8),
}

nb_gains = 3

nb_calibs = len(args.mod_calib)
det_mods_x, det_mods_y = det_modules[args.det_geom]
nb_mods = det_mods_x * det_mods_y
nb_sc, aux = divmod(nb_calibs, nb_mods)
if aux != 0 or nb_sc not in [1, 16]:
    raise ValueError(f'Number of calibs mismatch: expetected {nb_mods}, '
                     f'got {nb_calibs} calibrations')

mod_calib_shape = [nb_gains] + list(mod_shape)
raw_calib_shape = [nb_sc, nb_gains, nb_mods] + list(mod_shape)
file_calib_shape = [nb_sc * nb_gains, -1, mod_shape[1]]
cal_dtype = np.dtype('float64')

if args.sdk_mod_idx_list:
    mod_idx_map = [int(s) for s in args.sdk_mod_idx_list.split(',')]
    if len(mod_idx_map) != nb_mods:
        raise ValueError(f'Number of modules mismatch: expetected {nb_mods}, '
                         f'got {len(mod_idx_map)} indexes')
else:
    mod_idx_map = range(nb_mods)

# Raw format: vertical module concatenation
raw_calib = None
sc_mod_list = [[sc, n] for sc in range(nb_sc) for n in mod_idx_map]
for (sc, n), fname in zip(sc_mod_list, args.mod_calib):
    with open(fname, 'rb') as f:
        l = reduce(np.multiply, mod_calib_shape, cal_dtype.itemsize)
        d = f.read(l)
    mod_calib = np.frombuffer(d, cal_dtype).reshape(mod_calib_shape)
    if raw_calib is None:
        raw_calib = np.zeros(raw_calib_shape)
    raw_calib[sc,:,n] = mod_calib[:nb_gains]

print(f'Calib. average={[c.mean() for c in raw_calib]}')

print(f'Writing detector Raw calib to {args.raw_det_calib} ...')
with h5.File(args.raw_det_calib, 'w') as f:
    calib = raw_calib.reshape(file_calib_shape)
    f.create_dataset('/data', data=calib)

print(f'Assembling [{nb_sc} SCs] detector calib into {args.asm_wg_det_calib} ...')
opts = ' '
if args.sls_conda_env:
    opts += f'--sls_conda_env {args.sls_conda_env} '
opts += f'--nb_frames {nb_sc * nb_gains} '
opts += f'--pixel_map_fname pixel_map_wg_aux.h5 '
opts += f'--chip_map_fname chip_map_wg_aux.h5 '
opts += f'--gap_pixel_val 0 '
opts += f'--out_h5_fname {args.asm_wg_det_calib} '
cmd_args = f'{args.det_geom} {args.raw_det_calib} data'
this_script_dir = os.path.dirname(sys.argv[0])
asm_frames = os.path.join(this_script_dir, 'assemble_frames.py')
cmd = f'python {asm_frames} {opts} {cmd_args}'
print(f'Executing: {cmd} ...')
if os.system(cmd) != 0:
    raise RuntimeError('Error assembling gain')
