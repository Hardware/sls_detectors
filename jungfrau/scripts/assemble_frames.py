#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import argparse
from tempfile import mkstemp
import contextlib
from subprocess import run
import re
import numpy as np
import h5py as h5

parser = argparse.ArgumentParser(description='Assemble frames.')
parser.add_argument('--sls_conda_env',
                    help='Conda environment with sls_geometry_assembler')
parser.add_argument('--first_frame', type=int, default=0,
                    help='First frame to assemble')
parser.add_argument('--nb_frames', type=int, default=1,
                    help='Number of frames to assemble')
parser.add_argument('--gap_pixel_val', type=int, default=-1,
                    help='Gap pixels value')
parser.add_argument('--no_disp', action='store_true',
                    help='Do not displace 4M modules in J16M')
parser.add_argument('--geom_name', help='Process a "ref2.geom" file')
parser.add_argument('--verbose', action='store_true',
                    help='Be verbose')
parser.add_argument('--chip_map_fname', help='Generate chip_map')
parser.add_argument('--pixel_map_fname', help='Generate pixel_map')
parser.add_argument('--out_h5_fname', help='Output file name')
parser.add_argument('det_geom', help='Detector geometry: 500k|1M|4M|16M')
parser.add_argument('h5_fname', help='Input file name')
parser.add_argument('h5_data_path', help='Input data path')

args = parser.parse_args()

det_type = 'jungfraux2'

mod_geom = (1024, 512)
det_modules = {
    '500k': (1, 1),
    '1M': (1, 2),
    '4M': (2, 4),
    '16M': (4, 8),
}

det_size = [n * s for n, s in zip(det_modules[args.det_geom], mod_geom)]

default_frames = 0, 1

quadrant_disp = 34 if args.det_geom == '16M' and not args.no_disp else 0
disp_suffix = '_nodisp' if args.no_disp else ''

frame_suffix = ''
if (args.first_frame, args.nb_frames) != default_frames:
    frame_suffix = f'_{args.first_frame}_{args.nb_frames}'

if not args.out_h5_fname:
    suffix = f'{frame_suffix}{disp_suffix}'
    fname, ext = os.path.splitext(args.h5_fname)
    args.out_h5_fname = os.path.basename(fname) + f'_asm{suffix}.h5'

frame_block_size = 16

@contextlib.contextmanager
def safe_mkstemp(*args, **kws):
    ifile, ifname = mkstemp(*args, **kws)
    try:
        yield ifile, ifname
    finally:
        os.remove(ifname)

def size_2_shape(x):
    return x[1], x[0]

def image_2d_stack_shape(nb_frames, shape):
    return (nb_frames * shape[0], shape[1])

def image_3d_stack_shape(nb_frames, shape):
    return (nb_frames, shape[0], shape[1])

def exec_geometry_assembler(det_size, nb_frames = 1, gen_type = '',
                            src_fname = '', tgt_fname = '', dtype = 'uint16',
                            gap_pixel_val = -1):
    det_size_str = f'{det_size[0]},{det_size[1]}'
    src_fmt, src_dtype = 'Raw', dtype
    tgt_fmt, tgt_dtype = 'AsmWG', dtype
    prog_args = (f'{det_type} {det_size_str} {nb_frames} "{gen_type}" '
                 f'{src_fmt} {src_dtype} "{src_fname}" '
                 f'{tgt_fmt} {tgt_dtype} "{tgt_fname}" {gap_pixel_val}')
    prog = 'sls_geometry_assembler'
    cmd = ''
    if args.sls_conda_env:
        bashrc = '${HOME}/.bashrc'
        cmd += f'[ -f {bashrc} ] && . {bashrc}; ' \
               f'conda activate {args.sls_conda_env} && '
    cmd += f'{prog} {prog_args}'
    if args.verbose:
        print(f'Executing: {cmd}')
    p = run(['bash', '-c', cmd], capture_output=True, text=True)
    if args.verbose and p.stderr:
        print(p.stderr)
    if p.returncode != 0:
        raise RuntimeError(f'{prog} failed: ret={p.returncode}')
    return p.stdout.split('\n')

xy_re = re.compile('XY<(?P<x>[0-9]+),(?P<y>[0-9]+)>')
def decode_xy(s):
    m = xy_re.match(s)
    if not m:
        raise RuntimeError(f'Invalid output: {s}: {s}')
    return tuple(map(int, (m.group('x'), m.group('y'))))
    
dims_mod_corners = None

def query_geometry_assembler(det_size):
    global dims_mod_corners
    if dims_mod_corners is not None:
        return dims_mod_corners

    out_lines = exec_geometry_assembler(det_size)
    def decode_dim_line(l, kind):
        header = f'{kind} geometry: '
        if not l.startswith(header):
            raise RuntimeError(f'Invalid {kind} geometry line header: {l}')
        return decode_xy(l[len(header):])
    dims = [decode_dim_line(l, k)
            for l, k in zip(out_lines[:2], ['Source', 'Target'])]
    header = 'Target modules corners: '
    l = out_lines[2]
    if not l.startswith(header):
        raise RuntimeError(f'Invalid module corners line header: {l}')
    mod_corners_line = l[len(header):]
    all_xy = [decode_xy(p) for x in mod_corners_line.split()
                           for p in x.split('x')]
    mod_corners = list(zip(all_xy[0::2], all_xy[1::2]))
    dims_mod_corners = dims, mod_corners
    return dims_mod_corners

def assemble_raw_wg_frames(gen_type, raw_data, dtype):
    print('Raw data with inter-chip gaps')
    nb_frames = raw_data.shape[0]
    dims, mod_corners = query_geometry_assembler(det_size)
    raw_ng_mod_size, asm_wg_det_size = dims
    data = np.zeros(image_3d_stack_shape(nb_frames,
                                         size_2_shape(asm_wg_det_size)),
                    raw_data.dtype)
    raw_wg_mod_size = mod_corners[0][1]
    w, h = raw_wg_mod_size
    for m, ((x, y), s) in enumerate(mod_corners):
        y0 = m * h
        data[:, y:y+h, x:x+w] = raw_data[:, y0:y0+h]
    return data

basic_pixel_data = None

def assemble_raw_ng_frames_from_idx(raw_data):
    pd = basic_pixel_data
    data_shape = raw_data.shape[0], pd.shape[0], pd.shape[1]
    data = np.zeros(data_shape, raw_data.dtype)
    gap_val = np.int32(-1)
    masked = (pd == gap_val)
    valid = (pd != gap_val)
    for r, d in zip(raw_data, data):
        d[masked] = args.gap_pixel_val
        d[valid] = r.flatten()[pd[valid]]
    return data

def assemble_raw_ng_frames(gen_type, raw_data, dtype):
    global basic_pixel_data
    is_xform = (gen_type == 'pixel_val')
    if is_xform:
        if basic_pixel_data is not None:
            print('Assembling raw data with no inter-chip gaps from pixel_idx')
            return assemble_raw_ng_frames_from_idx(raw_data)
        print('Assembling raw data with no inter-chip gaps')
        nb_frames = raw_data.shape[0]
        dtype = raw_data.dtype
    else:
        print(f'Generating {gen_type} data with no inter-chip gaps')
        nb_frames = 1
        dtype = dtype
    dims, mod_corners = query_geometry_assembler(det_size)
    raw_ng_mod_size, asm_wg_det_size = dims
    with safe_mkstemp() as mks_data:
        ofile_fd, oname = mks_data
        os.close(ofile_fd)
        with safe_mkstemp() as mks_data:
            ifile_fd, iname = mks_data
            with open(ifile_fd, 'wb') as ifile:
                if is_xform:
                    ifile.write(raw_data.tobytes())
                else:
                    iname = ''
            exec_geometry_assembler(det_size, nb_frames, gen_type,
                                    iname, oname, dtype)
        out_pixels = asm_wg_det_size[0] * asm_wg_det_size[1]
        exp_osize = nb_frames * out_pixels * np.dtype(dtype).itemsize
        osize = os.stat(oname).st_size
        if osize != exp_osize:
            raise RuntimeError(f'Invalid size: {osize}, expected {exp_osize}')
        data = np.fromfile(oname, dtype)
    data = data.reshape(image_3d_stack_shape(nb_frames,
                                             size_2_shape(asm_wg_det_size)))
    if gen_type == 'pixel_idx':
        basic_pixel_data = data[0]
    return data

def assemble_frames(gen_type, raw_data, dtype):
    is_xform = (gen_type == 'pixel_val')
    raw_shape = raw_data.shape[1:] if is_xform else None
    dims, mod_corners = query_geometry_assembler(det_size)
    raw_ng_det_size, asm_wg_det_size = dims
    nb_mod = len(mod_corners)
    raw_wg_mod_size = mod_corners[0][1]
    raw_wg_det_shape = image_2d_stack_shape(nb_mod,
                                            size_2_shape(raw_wg_mod_size))
    raw_ng_det_shape = size_2_shape(raw_ng_det_size)
    # check if raw_data includes inter-chip gaps or not
    if raw_shape == raw_wg_det_shape:
        data = assemble_raw_wg_frames(gen_type, raw_data, dtype)
    elif not is_xform or raw_shape == raw_ng_det_shape:
        data = assemble_raw_ng_frames(gen_type, raw_data, dtype)
    else:
        raise RuntimeError(f'Invalid size: {raw_shape}, expected '
                           f'{raw_wg_det_shape} or {raw_ng_det_shape}')
    return data

def get_module_disp(mod, disp):
    left = (mod % 4 < 2)
    top = (mod < 16)
    h_disp = disp * (1 if top else -1)
    v_disp = disp * (-1 if left else 1)
    return h_disp, v_disp

def get_corr_module_corners(dims, mod_corners, disp):
    corners = []
    for m, (o, s) in enumerate(mod_corners):
        c = [(a + b + disp) for a, b in zip(o, get_module_disp(m, disp))]
        corners.append(c)
    return corners
    
def read_geom_file(geom_fname):
    geom_re = re.compile('m(?P<mod>[0-9]+)/(?P<par>[^ \t=]+)[ \t]*='
                         '[ \t]*(?P<val>.+)')
    module_geom = {}
    with open(geom_fname) as ifile:
        for l in ifile:
            m = geom_re.match(l)
            if not m:
                continue
            mod = int(m.group('mod'))
            mod_data = module_geom.setdefault(mod, {})
            val = m.group('val')
            if ' ' in val:
                val = val.split()
                if val[0].endswith('x') and val[1].endswith('y'):
                    val = [v[:-1] for v in val]
            else:
                val = [val]
            val = [float(v) for v in val]
            if len(val) == 1:
                val = val[0]
            mod_data[m.group('par')] = val
    return module_geom


def print_geometry_diff(geom_fname):
    module_geom = read_geom_file(geom_fname)    
    dims, mod_corners = query_geometry_assembler(det_size)
    corr_mod_corners = get_corr_module_corners(dims, mod_corners,
                                               quadrant_disp)
    x0, y0 = [x / 2 for x in dims[1]]
    for (m, v), o in zip(module_geom.items(), corr_mod_corners):
        o_x, o_y = o
        c_x = int(v["corner_x"] + x0)
        c_y = int(v["corner_y"] + y0)
        d_x = c_x - o_x
        d_y = c_y - o_y
        print(f'{m:2}: {c_x:10}, {c_y:10}, {o_x:10}, {o_y:10} '
              f'{d_x:10}, {d_y:10}')
    
def assemble_jungfrau_16M_frames(gen_type, raw_data, dtype = 'int16',
                                 disp = quadrant_disp):
    basic_data = assemble_frames(gen_type, raw_data, dtype)
    nb_frames = basic_data.shape[0]
    corr = lambda x: x + 2 * disp
    data_shape = [nb_frames] + list(map(corr, basic_data.shape[1:]))
    data = np.zeros(data_shape, basic_data.dtype)
    data[:] = args.gap_pixel_val
    dims, mod_corners = query_geometry_assembler(det_size)
    corr_mod_corners = get_corr_module_corners(dims, mod_corners, disp)
    for corner, corr_corner in zip(mod_corners, corr_mod_corners):
        (x0, y0), (w, h) = corner
        mod_data = basic_data[:, y0:y0+h, x0:x0+w]
        x, y = corr_corner
        data[:, y:y+h, x:x+w] = mod_data
    return data

def write_data(data, fname):
    if data.shape[0] == 1:
        data = data[0]
    with h5.File(fname, 'w') as h5_out_file:
        h5_out_file.create_dataset('data', data=data)

def write_geometry_chip_map(fname):
    data = assemble_jungfrau_16M_frames('chip_idx', None)
    write_data(data, fname)

def get_slices(total_frames, first_frame, nb_frames, frame_block_size):
    done = 0
    while done < nb_frames:
        block_size = min(frame_block_size, nb_frames - done)
        yield first_frame + done, block_size
        done += block_size

def read_assemble_write_data(h5_fname, first_frame, nb_frames, frame_block_size,
                             out_fname):
    path = args.h5_data_path.split('/')
    with h5.File(h5_fname, 'r') as h5_in_file:
        idata = None
        obj = h5_in_file
        while path:
            obj = obj[path.pop(0)]
        idata = obj
        if len(idata.shape) == 2:
            if first_frame != 0 or nb_frames != 1:
                raise RuntimeError('Invalid frame request on single-frame data')
            idata = idata[:].reshape(image_3d_stack_shape(1, idata.shape))

        with h5.File(out_fname, 'w') as h5_out_file:
            dset = None
            frame_slices = get_slices(idata.shape[0], first_frame, nb_frames,
                                      frame_block_size)
            for block_start, block_size in frame_slices:
                block_end = block_start + block_size
                print(f'Processing frames [{block_start}:{block_end}]')
                raw_data = idata[block_start:block_end]
                data = assemble_jungfrau_16M_frames('pixel_val', raw_data)

                if nb_frames == 1:
                    dset = h5_out_file.create_dataset('data', data=data[0])
                    break

                if dset is None:
                    dset_shape = nb_frames, data.shape[1], data.shape[2]
                    dset = h5_out_file.create_dataset('data', dset_shape,
                                                      data.dtype)
                    dset_first = 0

                dset_end = dset_first + block_size
                dset[dset_first:dset_end] = data
                dset_first += block_size
                
pixel_data = assemble_jungfrau_16M_frames('pixel_idx', None, 'int32')
if args.pixel_map_fname:
    write_data(pixel_data, args.pixel_map_fname)
    
if args.geom_name:
    print_geometry_diff(geom_name)

if args.chip_map_fname:
    write_geometry_chip_map(args.chip_map_fname)

read_assemble_write_data(args.h5_fname, args.first_frame, args.nb_frames,
                         frame_block_size, args.out_h5_fname)
