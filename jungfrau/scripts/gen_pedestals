#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
import argparse
import numpy as np
import h5py as h5
import hdf5plugin

nb_gains = 3

def ave_pedestals(fname_list):
    ave = None
    for i, fname in enumerate(fname_list):
        print(f'Reading {fname} ...')
        with h5.File(fname, 'r') as h5_in_file:
            idata = h5_in_file['/entry_0000/measurement/data']
            if ave is None:
                ave = np.zeros((nb_gains, idata.shape[1], idata.shape[2]),
                               'float64')
            ave[i] = np.average(idata, 0)
    return ave


def main():
    parser = argparse.ArgumentParser(description=('Jungfrau Lima1 to Raw '
                                                  'converter.'))
    parser.add_argument('--out_fname',
                        help='Output pedestal file name')
    for i in range(nb_gains):
        parser.add_argument(f'in_gain{i}',
                            help=f'HDF5 Gain-{i} input file name')

    args = parser.parse_args()

    ped_fname_list = [getattr(args, f'in_gain{i}') for i in range(nb_gains)]
    if not args.out_fname:
        raise RuntimeError(f'Pedestal output file name is mandatory')

    pedestals = ave_pedestals(ped_fname_list)

    print(f'Writing pedestals to {args.out_fname} ...')
    with h5.File(args.out_fname, 'w') as h5_out_file:
        h5_out_file.create_dataset('/data', data=pedestals)


if __name__ == '__main__':
    main()
