#!/usr/bin/env python3

import sys
import os
import argparse
import socket
import time
import re
import glob
import subprocess
import json

import numpy as np
import h5py as h5

user_name = "opid29"
host_conda_envs = {
    "lid29p9jfrau1": "jungfrau_lima2",
    "lid29pwr9": "lima2"
}

# Target directory
target_dir="/data/id29/inhouse/opid291/Jungfrau/Calibration"

# Jungfrau packet parameters
jfrau_pixel_depth = np.dtype("uint16").itemsize
jfrau_packet_header_len = 64 # (4 + 48) left-padded to be 128-bit aligned
jfrau_packet_header_pixels = jfrau_packet_header_len // jfrau_pixel_depth
jfrau_packet_data_len = 8 * 1024
jfrau_packet_data_pixels = jfrau_packet_data_len // jfrau_pixel_depth

# Jungfrau geometry
chip_size = 256
mod_chips = [4, 2]
chip_gap = 2
mod_size_wg = [n * chip_size + (n - 1) * chip_gap for n in mod_chips]
print(f"Module size (with gap): {mod_size_wg}")

# Detector gain calibration
gain_map_dir = "gainMaps_JF4MID29"
jfrau_mod_sensors = [243, 334, 379, 337, 341, 368, 376, 342]
jfrau_mod_names = [f"jfrau4mid29b{i}" for i in range(len(jfrau_mod_sensors))]

# Bchip replacement
jfrau_bchip_replace = {
    "jfraubchip307": "jfrau4mid29b3",
}

# Lima2 Tango server info
tango_host = "id29:20000"
lima2_tango_server_bin = "lima2_psi_ctrl"
lima2_tango_server_name = "jungfrau4m_rr_0"

# pyFAI detector definition
det_def_uncorrected="Jungfrau_4M_ID29_uncorrected.h5"
det_def="Jungfrau_4M_ID29.h5"

# Auxiliary files: ng=NoGap, wg=WithGap
pixel_map_wg = "pixel_map_wg.h5"
csr_wg = "csr_wg.h5"
bin_centers_wg = "bin_centers_wg.h5"
r_center_wg = "r_center_wg.h5"
geom_lut = "psi_geom_lut.h5"
calib_params_fmt = "calib_params_{0}.txt"
calib_params = "calib_params.txt"
beamstop_geometry = "beamstop_geometry.json"
mask = "mask.h5"
mask_wg = "mask_wg.h5"
mask_wide_basic = "mask_wide_basic.h5"
mask_wide_extra = "mask_wide_extra.h5"
mask_hot_cold = "mask_hot_cold.h5"
mask_hot_cold_fmt = "mask_hot_cold_{0}.h5"
mask_hot_cold_prev = "mask_hot_cold_previous.h5"
mask_no_beamstop = "mask_no_beamstop.h5"
mask_beamstop_fmt = "mask_beamstop_{0}.h5"
mask_beamstop_prev = "mask_beamstop_previous.h5"
mask_beamstop = "mask_beamstop.h5"
mask_hot_cold_beamstop_fmt = "mask_hot_cold_beamstop_{0}.h5"
mask_hot_cold_beamstop_prev = "mask_hot_cold_beamstop_previous.h5"
mask_hot_cold_beamstop = "mask_hot_cold_beamstop.h5"
peakfinder_params = "peakfinder_params.json"

# parse command line arguments
app_desc = "ID29 Jungfrau-4M Calibration Generator"
parser = argparse.ArgumentParser(description=app_desc)
parser.add_argument("--cal_tag",
                    help="Calibration tag (subdirectory): YY-MM-DD")
parser.add_argument("--pyfai_poni",
                    help="pyFAI calibration PONI file")
parser.add_argument("--sample_distance",
                    help="Sample-to-detector distance (in mm)")
parser.add_argument("--beam_center",
                    help="Beam center pixel position: X,Y")
parser.add_argument("--nr_bins", type=int, default=800,
                    help="pyFAI number radial bins")
parser.add_argument("--reset_hot_cold", action="store_true",
                    help="Start with an empty hot-cold pixel mask")
parser.add_argument("--dark_data_dir", default=os.curdir,
                    help="Dir. with dark data files to update hot-cold mask ")
parser.add_argument("--dark_data_files",
                    help="Dark filename pattern [ex: lysosyme-25-dense-*.h5]")
parser.add_argument("--cold_thres",
                    help="Cold threshold when updating the hot-cold pixel mask")
parser.add_argument("--hot_thres",
                    help="Hot threshold when updating the hot-cold pixel mask")
parser.add_argument("--min_occurrence",
                    help="Minimun number of frames with hot-cold event (in %)")
parser.add_argument('--frames_per_file',
                    help='Number of frames per dark file to process')

args = parser.parse_args()

if args.pyfai_poni:
    if args.sample_distance or args.beam_center:
        raise ValueError("pyfai_poni & sample_distance/beam_center options "
                         "are exclusive")
elif not (args.sample_distance and args.beam_center):
    raise ValueError("Must provide one of pyfai_poni & "
                     "sample_distance/beam_center options")

def log_stage(msg, *args, **kws):
    print("**** " + msg)

prog_name = os.path.basename(sys.argv[0])

log_stage("Checking that we are at the good place ...")
host_name = socket.gethostname()
valid_hosts = host_conda_envs.keys()
if os.getlogin() != user_name or host_name not in valid_hosts:
    raise RuntimeError(f"{prog_name} must run as {user_name}@{valid_hosts}")
elif os.environ.get("CONDA_DEFAULT_ENV") != host_conda_envs[host_name]:
    raise RuntimeError(f"{prog_name} must run inside "
                       f"{host_conda_envs[host_name]} conda environment")

SLS_DETECTORS_DIR = os.environ.get("SLS_DETECTORS_DIR")
if not (SLS_DETECTORS_DIR and os.path.exists(SLS_DETECTORS_DIR)):
    raise RuntimeError(f"Bad SLS_DETECTORS_DIR: {SLS_DETECTORS_DIR}")

# Shell helpers
def run_sh(cmd, **args):
    shell = isinstance(cmd, str) or cmd[:2] != ["bash", "-c"]
    if shell and not isinstance(cmd, str):
        cmd = " ".join(cmd)
    p = subprocess.run(cmd, shell=shell, check=False, text=True, **args)
    if p.returncode != 0:
        if args.get("capture_output", False) and p.stderr:
            print(f"Error: {p.stderr}")
        raise subprocess.CalledProcessError(p.returncode, cmd,
                                            output=p.stdout, stderr=p.stderr)
    return p

for p in os.environ["PATH"].split(":"):
    conda_activate = os.path.join(p, "../bin/activate")
    if os.path.exists(conda_activate):
        break
else:
    raise RuntimeError("Cannot find conda activate script")

def run_conda_sh(env, cmd, **args):
    # Explicitly execute bash (instead of sh)
    sh_cmd = f". {conda_activate} {env} && {cmd}"
    return run_sh(["bash", "-c", sh_cmd], **args)

log_stage("Checking for needed scripts ...")
fai_scripts_dir = os.path.join(SLS_DETECTORS_DIR, "lima2/processings/common/fai/scripts")
jfrau_scripts_dir = os.path.join(SLS_DETECTORS_DIR, "jungfrau/scripts")
required_entries = [fai_scripts_dir, jfrau_scripts_dir]
if not all([os.path.exists(d) for d in required_entries]):
    raise RuntimeError(f"Missing: {required_entries}")
print(f"Found: {required_entries}")

os.chdir(target_dir)

cal_tag = args.cal_tag or time.strftime("%Y-%m-%d")
if not os.path.exists(cal_tag):
    run_sh(f"mkdir {cal_tag}")
    prev_cal = "UNKNOWN"
    try:
        prev_cal = os.readlink("current")
        print(f"Creating calibration {cal_tag} from previous {prev_cal}...")
        prev_mask = os.readlink(f"{prev_cal}/{mask_hot_cold}")
        cp = "cp --preserve=timestamps"
        cmds = [f"cd {cal_tag}",
                f"ln -s ../{prev_cal}/{gain_map_dir}",
                f"ln -s ../{prev_cal}/{det_def}",
                f"{cp} ../{prev_cal}/{prev_mask} .",
                f"ln -s {prev_mask} {mask_hot_cold}",
                f"ln -s {mask_hot_cold} {mask_no_beamstop}",
                f"{cp} ../{prev_cal}/{beamstop_geometry} ."]
        prev_pf_params = f"{prev_cal}/{peakfinder_params}"
        if os.path.exists(prev_pf_params):
            cmds += [f"{cp} ../{prev_pf_params} ."]
        run_sh(" && ".join(cmds))
    except:
        print(f"Error copying from {prev_cal}: removing {cal_tag}")
        run_sh(f"rm -rf {cal_tag}")
        raise

os.chdir(cal_tag)

def backup_entry(name, backup):
    if os.path.exists(name):
        run_sh(f"rm -f {backup} && mv {name} {backup}")

def update_link(name, target):
    run_sh(f"rm -f {name} && ln -s {target} {name}")

def backup_update_link(name, backup, target):
    backup_entry(name, backup)
    update_link(name, target)

def set_hot_cold_mask(new_mask):
    print(f"Setting hot_cold_mask to {new_mask} ...")
    backup_update_link(mask_hot_cold, mask_hot_cold_prev, new_mask)

if args.reset_hot_cold:
    set_hot_cold_mask(mask_wide_extra)

log_stage("Reading Lima2 config file from Tango DB ...")

def get_lima2_sdk_config(server_name, server_bin=lima2_tango_server_bin,
                         tango_host=tango_host):
    cmd = f"""
import tango
import json
db = tango.Database()
lima2_serv = "{server_bin}/{server_name}"
sn, sc, dn, dc = db.get_device_class_list(lima2_serv).value_string
dp = db.get_device_property(dn, "plugin_params")["plugin_params"][0]
plugin_params = json.loads(dp)
with open(plugin_params["config_file"], "rt") as f:
  lima2_config = json.load(f)
for l in lima2_config["sdk_config_params"]:
    print(l)
"""
    p = run_conda_sh("lima_launcher", f"TANGO_HOST={tango_host} python",
                     input=cmd, capture_output=True)
    return p.stdout.split("\n")

lima2_sdk_config = get_lima2_sdk_config(lima2_tango_server_name)

log_stage("Getting detector basic geometry ...")
det_size_ng = None
jfrau_udp_ifaces = None
for l in lima2_sdk_config:
    tok = l.split()
    if len(tok) == 3 and tok[0] == "detsize":
        det_size_ng = [int(x) for x in tok[1:3]]
    elif len(tok) == 2 and tok[0] == "numinterfaces":
        jfrau_udp_ifaces = int(tok[1])
if not (det_size_ng and jfrau_udp_ifaces):
    raise RuntimeError("Could not find detsize/numinterfaces in SDK config")
print(f"Detector size w/o gaps: {det_size_ng}")
print(f"Nb. UDP interfaces: {jfrau_udp_ifaces}")

log_stage("Querying detector AsmWG size ...")
det_geom_type = f"jungfraux{jfrau_udp_ifaces}"
det_size_str = ",".join([str(x) for x in det_size_ng])
cmd = f"sls_geometry_assembler {det_geom_type} {det_size_str} 1 pixel_idx Raw uint32 '' AsmWG uint32 '' -1"
p = run_sh(cmd, capture_output=True)

def decode_geom(geom, out):
    header = f"{geom.capitalize()} geometry: "
    target_geom = [x for x in out.split("\n") if x.startswith(header)][0]
    target_geom_re = re.compile(f"^{header}XY<(?P<x>[0-9]+),(?P<y>[0-9]+)>")
    m = target_geom_re.match(target_geom)
    if not m:
        raise RuntimeError(f"Cannot decode detector {geom} geometry: {target_geom}")
    return [int(m[g]) for g in "xy"]

geometries = {g: decode_geom(g, p.stdout) for g in ("source", "target")}
print("Assembler geometries:")
for g, s in geometries.items():
    print(f"  {g.capitalize()}: {s}")
det_size_wg = geometries["target"]
print(f"Detector size (with gap): {det_size_wg}")

det_chips = [i // chip_size for i in det_size_ng]
det_mods = [d // m for d, m in zip(det_chips, mod_chips)]
mod_gap = [(d - m * n) // (n - 1) for d, m, n in zip(det_size_wg, mod_size_wg, det_mods)]
print(f"Detector modules: {det_mods}")
print(f"Inter-modudle gap: {mod_gap}")

log_stage("Creating the raw-in-assembled map file ...")
script = os.path.join(fai_scripts_dir, "psi/gen_pixel_map.py")
cmd = f"python {script} 4M Raw AsmWG {pixel_map_wg} 2"
run_sh(cmd)

log_stage("Creating the geometry LUT file ...")
with h5.File(pixel_map_wg, "r") as f:
    raw_pixel_idx = np.array(f["/data"])
raw_packet, pixel_idx = np.divmod(raw_pixel_idx, jfrau_packet_data_pixels)
header_offset = (raw_packet + 1) * jfrau_packet_header_pixels
valid_pixels = (raw_pixel_idx >= 0)
raw_pixel_idx[valid_pixels] += header_offset[valid_pixels]
with h5.File(geom_lut, "w") as f:
    f.create_dataset("/data", data=raw_pixel_idx)

log_stage("Finding Jungfrau module index in SDK config ...")
for l in lima2_sdk_config:
    tok = l.split()
    if len(tok) == 2 and tok[0] == "hostname":
        hosts = tok[1].split("+")[:-1]
        # Replace bad module(s)
        for new_bchip, orig_bchip in jfrau_bchip_replace.items():
            hosts[hosts.index(new_bchip)] = orig_bchip
        assert len(hosts) == len(jfrau_mod_names)
        sdk_mod_idx = [hosts.index(m) for m in jfrau_mod_names]
        break
else:
    raise RuntimeError("Did not find hostname key in Lima2 SDK config")

print("Jungfrau module index in SDK config: ",
      {n: i for n, i in zip(jfrau_mod_sensors, sdk_mod_idx)})

log_stage("Creating gain calibration file ...")
def mod_calib_file(mod):
    return glob.glob(f"gainMaps_JF4MID29/gainMaps_M{mod}_*.bin")[0]

mod_calib_files = " ".join([mod_calib_file(m) for m in jfrau_mod_sensors])
script = os.path.join(jfrau_scripts_dir, "gen_calibration.py")
mod_idx_str = ",".join([str(i) for i in sdk_mod_idx])
cmd = f"python {script} --sdk_mod_idx_list {mod_idx_str} 4M {mod_calib_files}"
run_sh(cmd)

log_stage("Creating (uncorrected) detector geometry ...")
script = os.path.join(fai_scripts_dir, "psi/gen_detector.py")
cmd = f"python {script} --pixel_map {pixel_map_wg} {det_def_uncorrected}"
run_conda_sh("pyfai", cmd)

log_stage("Checking detector position calibration ...")
if args.pyfai_poni:
    # Copy poni file to target dir
    poni_dir = os.path.dirname(os.path.abspath(args.pyfai_poni))
    if poni_dir != os.path.abspath("."):
        run_sh(f"cp --preserve=timestamps {args.pyfai_poni} .")
    # Create the pyFAI CSR files using pyFAI params
    gen_csr_params = [f"--pyfai_poni {args.pyfai_poni}"]

    cmd = f"""
import json
import pyFAI
ai = pyFAI.load("{args.pyfai_poni}")
fit2d = ai.getFit2D()
det = fit2d["detector"]
det_fields = ["name"]
if isinstance(det, pyFAI.detectors._common.NexusDetector):
  det_fields += ["filename"]
else:
  det_fields += ["max_shape", "pixel1", "pixel2"]
fit2d["detector"] = {{i: getattr(det, i) for i in det_fields}}
print(json.dumps(fit2d))
"""
    p = run_conda_sh("pyfai", f"python", input=cmd, capture_output=True)
    print(f"Output: {p.stdout}")
    calib_data = json.loads(p.stdout)
    print(f"calib_data={calib_data}")
    sample_distance = calib_data["directDist"]
    beam_center = [calib_data[f"center{i}"] for i in "XY"]
else:
    sample_distance = float(args.sample_distance)
    beam_center = [float(x) for x in args.beam_center.split(",")]

    # Create the pyFAI CSR files using pyFAI params
    gen_csr_params = [f"--det_config {det_def}",
                      f"--sample_distance {args.sample_distance}",
                      f"--beam_position {args.beam_center}"]

log_stage("Saving beam calibration parameters ...")
calib_data = {
    "sample_distance": sample_distance,
    "beam_center": beam_center,
}
print(f"Calibration parameters: {calib_data}")
last_calib_params = calib_params_fmt.format(cal_tag)
with open(last_calib_params, "wt") as f:
    f.write(json.dumps(calib_data, indent=2) + "\n")
update_link(calib_params, last_calib_params)

log_stage("Creating the pyFAI CSR files ...")
script = os.path.join(fai_scripts_dir, "gen_csr.py")
gen_csr_params += [str(args.nr_bins), csr_wg, bin_centers_wg, r_center_wg]
cmd = " ".join([f"python {script}"] + gen_csr_params)
run_conda_sh("pyfai", cmd)

log_stage("Creating default mask file ...")
script = os.path.join(fai_scripts_dir, "gen_mask.py")
gen_mask_params = [f"--pixel_map {pixel_map_wg}",
                   f"--wide_gap_mask {mask_wide_basic}",
                   f"{mask_wg}"]
cmd = " ".join([f"python {script}"] + gen_mask_params)
run_sh(cmd)

extra_mod_gap = 2
log_stage("Enlarging inter-module gap ...")
with h5.File(mask_wide_basic, "r") as f:
    m = np.array(f["/data"], "bool")
for i in range(1, det_mods[0]):
    x0 = mod_size_wg[0] * i + mod_gap[0] * (i - 1) - extra_mod_gap
    x1 = x0 + mod_gap[0] + 2 * extra_mod_gap
    m[:,x0:x1] = True
for i in range(1, det_mods[1]):
    y0 = mod_size_wg[1] * i + mod_gap[1] * (i - 1) - extra_mod_gap
    y1 = y0 + mod_gap[1] + 2 * extra_mod_gap
    m[y0:y1,:] = True
with h5.File(mask_wide_extra, "w") as f:
    f.create_dataset("/data", data=m)

log_stage("Creating beam-stop mask file ...")
print(f"Reading {beamstop_geometry} ...")
with open(beamstop_geometry, "rt") as f:
    bs_geom = json.load(f)
    print(f"Geometry: {bs_geom}")
    bs_size = bs_geom["size"]
    bs_holder = bs_geom["holder"]

last_mask_beamstop = mask_beamstop_fmt.format(cal_tag)
script = os.path.join(fai_scripts_dir, "gen_beam_stop_mask.py")
beam_pos = ",".join([str(i) for i in beam_center])
gen_bs_mask_params = [f"--base_mask {mask_wide_extra}",
                      f"--beam_position {beam_pos}",
                      f"--beam_stop_size {bs_size}",
                      f"--beam_stop_holder '{bs_holder}'",
                      f"{last_mask_beamstop}"]
cmd = " ".join([f"python {script}"] + gen_bs_mask_params)
run_conda_sh("pyfai", cmd)
backup_update_link(mask_beamstop, mask_beamstop_prev, last_mask_beamstop)

dark_data_files = []
if args.dark_data_files:
    dark_data_pattern = os.path.join(args.dark_data_dir, args.dark_data_files)
    dark_data_files = sorted(glob.glob(dark_data_pattern))
    
if dark_data_files:
    log_stage("Updating hot/cold pixel mask ...")
    script = os.path.join(jfrau_scripts_dir, "gen_hot_cold_mask")
    new_mask = mask_hot_cold_fmt.format(cal_tag)
    script_args = ["--base_mask", mask_hot_cold,
                   "--output_mask", new_mask,
                   "--print_found"]
    for par in ["cold_thres", "hot_thres", "min_occurrence", "frames_per_file"]:
        val = getattr(args, par)
        if val is not None:
            script_args += [f"--{par}", val]
    print(f"Reading dark files: {dark_data_files} ...")
    run_sh([script] + script_args + dark_data_files)
    set_hot_cold_mask(new_mask)

log_stage("Merging hot/cold & beam-stop masks ...")
with h5.File(mask_hot_cold, "r") as f:
    m1 = np.array(f["/data"], "bool")
with h5.File(mask_beamstop, "r") as f:
    m2 = np.array(f["/data"], "bool")
last_mask_hot_cold_beamstop = mask_hot_cold_beamstop_fmt.format(cal_tag)
with h5.File(last_mask_hot_cold_beamstop, "w") as f:
    f.create_dataset("/data", data=(m1 | m2))
backup_update_link(mask_hot_cold_beamstop, mask_hot_cold_beamstop_prev,
                   last_mask_hot_cold_beamstop)
update_link(mask, mask_hot_cold_beamstop)

log_stage("Creating links used by SMX pipeline ...")
for x in ["gains", "csr", "bin_centers", "r_center"]:
    update_link(x + ".h5", x + "_wg.h5")

# Update the current link - manual
cmds = [f"cd {target_dir}",
        f"rm -f previous",
        f"mv current previous",
        f"ln -s {cal_tag} current"]
cmd_str = " && ".join(cmds)
print("In order to test the new calibration perform:")
print(f"  ({cmd_str})")
